import datetime
import eboxconfig
from enum import Enum

class ScheduleEvent(Enum):
    start_sampling = 1
    stop_sampling = 2


class EboxSchedule(object):
    def __init__(self, ebox_config):

        # for debugging
        self.verbose = False
        
        self.ebox_config = ebox_config

        self.position_index = 1
        self.cartridge_schedule_index = 0

        #self.locked_until_cartridge_change = False
        #self.cartridge_was_changed = False

        self.sampling_active = False

        self.previous_samdat = self.ebox_config.sampling_start
        self.next_samdat = self._forward_cycle(self.previous_samdat,
                                               self.ebox_config.sampling_cycle_time_on,
                                               self.ebox_config.sampling_cycle_time_off)

        self.next_event_time = self.next_samdat
        self.next_event = ScheduleEvent.stop_sampling

        #self.current_cartridge_dates = []
        self.calculated_cartridge_dates = []
        self.calculated_next_cartridge_dates = []

        self._forward_to_time(datetime.datetime.now())

    @property
    def next_cartridge_schedule_index(self):
        schedule = self.ebox_config.filter_cartridge_schedule
        next_index = self.cartridge_schedule_index + 1
        if len(schedule) <= next_index:
            next_index = 0
        return next_index

    @property
    def locked_until_cartridge_change(self):
        return self.ebox_config.locked_until_cartridge_change
    @locked_until_cartridge_change.setter
    def locked_until_cartridge_change(self, value):
        self.ebox_config.locked_until_cartridge_change = value

    @property
    def cartridge_was_changed(self):
        return self.ebox_config.cartridge_was_changed
    @cartridge_was_changed.setter
    def cartridge_was_changed(self, value):
        self.ebox_config.cartridge_was_changed = value

    def change_cartridge(self):
        if self.locked_until_cartridge_change:
            self.locked_until_cartridge_change = False
            if self.sampling_active:
                self.set_next_event_to_now()
        else:
            self.cartridge_was_changed = True

    def to_dictionary(self):
        properties = ['cartridge_schedule_index', 'locked_until_cartridge_change',
                      'cartridge_was_changed', 'next_event', 'next_event_time',
                      'next_samdat', 'position_index',
                      'previous_samdat', 'sampling_active', 'verbose',
                      'calculated_cartridge_dates', 'calculated_next_cartridge_dates'
                      ]
        dict_ = {}
        for prop in properties:
            value = getattr(self, prop)
            dict_[prop] = value
        dict_['cartridge_schedule'] = self.ebox_config.filter_cartridge_schedule
        dict_['sampling_time_on'] = self.ebox_config.sampling_cycle_time_on
        dict_['sampling_time_off'] = self.ebox_config.sampling_cycle_time_off
        return dict_

    def forward_to_next_event(self):
        middle_transition = self.previous_samdat + self.ebox_config.sampling_cycle_time_on
        if self.next_event_time == self.next_samdat:
            self.previous_samdat = self.next_samdat
            self.next_samdat = self._forward_cycle(self.previous_samdat,
                                                   self.ebox_config.sampling_cycle_time_on,
                                                   self.ebox_config.sampling_cycle_time_off)
            self.next_event_time = self.previous_samdat + self.ebox_config.sampling_cycle_time_on
            self.next_event = ScheduleEvent.stop_sampling
        elif self.next_event_time == middle_transition:
            self.next_event_time = self.next_samdat
            self.next_event = ScheduleEvent.start_sampling
            self._increment_position(self.ebox_config.prevent_sampling_until_cartridge_change)
        elif self.previous_samdat < self.next_event_time and self.next_event_time < middle_transition:
            self.next_event_time = middle_transition
            self.next_event = ScheduleEvent.stop_sampling
        elif middle_transition < self.next_event_time and self.next_event_time < self.next_samdat:
            self.next_event_time = self.next_samdat
            self.next_event = ScheduleEvent.start_sampling
        

    def set_next_event_to_now(self):
        self.next_event_time = datetime.datetime.now()
        if self.sampling_active:
            self.next_event = ScheduleEvent.start_sampling
        else:
            self.next_event = ScheduleEvent.stop_sampling
        

    def _forward_cycle(self, start, time_on, time_off):
        """Move forward a sampling cycle (time on + time off) and return next samdat."""
        next_ = start + time_on
        next_ += time_off
        return next_

    def _increment_position(self, check_for_cartridge_change = False):
        """Move forward to the next position."""
        # don't want to type the whole variable so assign it to a shorter name
        schedule = self.ebox_config.filter_cartridge_schedule
        # move to the next position
        self.position_index += 1
        # check if the position is over the number of positions on the current cartridge
        if schedule[self.cartridge_schedule_index] < self.position_index:
            # debug info to show cartridge changed
            if (True == self.verbose): print('-')
            # reset position for next cartridge and then select next cartridge
            self.position_index = 1
            self.cartridge_schedule_index += 1
            # check if went past the last cartridge in the cartridge schedule
            if len(schedule) <= self.cartridge_schedule_index:
                # reset to first cartridge
                self.cartridge_schedule_index = 0
                # debug info to show 'box' changed
                if (True == self.verbose): print('--')
            if check_for_cartridge_change:
                self.locked_until_cartridge_change = True
                if self.cartridge_was_changed:
                    self.locked_until_cartridge_change = False
                    self.cartridge_was_changed = False
            # we changed cartridges so need to reset the cartridge dates
            self.calculated_cartridge_dates = self.calculated_next_cartridge_dates
            self.calculated_next_cartridge_dates = self._get_next_cartridge_dates(schedule)

    def _get_next_cartridge_dates(self, schedule):
        last_samdat = self.calculated_cartridge_dates[-1]
        next_samdat = self._forward_cycle(last_samdat,
                                          self.ebox_config.sampling_cycle_time_on,
                                          self.ebox_config.sampling_cycle_time_off)
        self.calculated_next_cartridge_dates = []
        next_cartridge_index = self.cartridge_schedule_index + 1
        #schedule = self.ebox_config.filter_cartridge_schedule
        if len(schedule) <= next_cartridge_index:
            next_cartridge_index = 0
        cassette_count = schedule[next_cartridge_index]
        dates = self._get_cartridge_dates(next_samdat, cassette_count)
        return dates


    def _get_cartridge_dates(self, first_samdat, cassette_count):
        dates = []
        samdat = first_samdat
        for index in range(cassette_count):
            dates.append(samdat)
            samdat = self._forward_cycle(samdat,
                                         self.ebox_config.sampling_cycle_time_on,
                                         self.ebox_config.sampling_cycle_time_off)
        return dates
        

    def _forward_to_time(self, time):
        """Go forward from the config start date to the specified time."""
        # reset to starting values
        self.calculated_cartridge_dates = []
        self.calculated_next_cartridge_dates = []
        self.position_index = 1
        self.cartridge_schedule_index = 0
        self.previous_samdat = self.ebox_config.sampling_start
        time_on = self.ebox_config.sampling_cycle_time_on
        time_off = self.ebox_config.sampling_cycle_time_off
        schedule = self.ebox_config.filter_cartridge_schedule
        self.next_samdat = self._forward_cycle(self.previous_samdat, time_on, time_off)

        # skip forward through the offset cycles
        offset = self.ebox_config.sampling_cycle_offset_count
        while(0 < offset):
            offset -= 1
            self.previous_samdat = self.next_samdat
            self.next_samdat = self._forward_cycle(self.previous_samdat, time_on, time_off)

        # set up the array of dates for the current and the next cartridges
        self.calculated_cartridge_dates = self._get_cartridge_dates(self.previous_samdat,
                                                                 schedule[0])
        self.calculated_next_cartridge_dates = self._get_next_cartridge_dates(schedule)
        
        # cycle forward until caught up with the given time
        while(self.next_samdat < time):
            self._increment_position()

            # for debugging:
            if (True == self.verbose):
                print('    Samdat: ', end='')
                print(self.next_samdat, end='')
                print('  Day: ', end='')
                print(self.position_index)

            self.previous_samdat = self.next_samdat
            self.next_samdat = self._forward_cycle(self.previous_samdat, time_on, time_off)

        # check to see if we are in an active sampling period
        self.next_event_time = self.previous_samdat + time_on
        if (time < self.next_event_time):
            print('    Sampling started at ', end='')
            print(self.previous_samdat, end='')
            print(' on position %d and ends at ' % self.position_index, end='')
            print(self.next_event_time)
            self.sampling_active = True
            self.next_event = ScheduleEvent.stop_sampling
        # otherwise wait for next samdat
        else:
            print('    Next sampling starts ', end='')
            print(self.next_samdat, end='')
            print(' (last position: %d, last sampling start: ' % self.position_index, end='')
            print(self.previous_samdat, end='')
            print(')')
            self.next_event_time = self.next_samdat
            self.next_event = ScheduleEvent.start_sampling
            self.sampling_active = False
            # the position indexes need to be pointing at the next samdat instead of the previous one
            self._increment_position()
            
        
