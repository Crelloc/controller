#!/bin/bash
# -*-ksh-*-
### BEGIN INIT INFO
# Provides:          Crocker-BBB-controller
# Required-Start:    $all
# Required-Stop:     
# Default-Start:     2
# Default-Stop:
# Short-Description: Starts the Controller required scripts
### END INIT INFO

PATH="/sbin:/bin"

. /lib/lsb/init-functions

case "$1" in
  start) 
    sleep 5
    run-parts --regex="\.(sh|py)\$"  --verbose --arg=start /usr/local/controller/init.d/
    ;;
  stop)
    exit 0
    ;;
  *)
    echo "Usage: $0 {start|stop}" >&2
    exit 1
    ;;
esac

