import os
import subprocess


class GPIO(object):
    pumps = {1: 0x01,
             2: 0x02,
             3: 0x04,
             4: 0x08,
             5: 0x10,
             6: 0x20, # we shouldn't have more than 5 modules connected in the field.
             7: 0x40, #  but there are two extra in case we want more.
             8: 0x80, #  so for testing purposes I am adding extra entries
             9: 0x80, #  which will probably be removed later
             10: 0x80,
             11: 0x80,
             12: 0x80,
             13: 0x80,
             14: 0x80,
             15: 0x80
             }

    def _mcp230xx_settings():
        i2c_bus = 2
        i2c_address = 0x20
        return (i2c_bus, i2c_address)

    def init_gpio_platform(gpio_number):
        gpio_name = 'gpio%d' % gpio_number
        base_path = '/sys/class/gpio/'
        gpio_path = base_path + gpio_name
        # check if gpio has already been set up
        if not os.path.isdir(gpio_path):
            # if not, set it up
            export_path = base_path + 'export'
            with open(export_path, 'w') as export_file:
                export_file.write('%d' % gpio_number)
        # make sure the direction of the gpio port is set
        direction_path = gpio_path + '/direction'
        with open(direction_path, 'w') as direction_file:
            direction_file.write('out')
        # make sure the value is set at zero
        value_path = gpio_path + '/value'
        with open(value_path, 'w') as value_file:
            value_file.write('0')

    def init_gpio_mcp230xx(pump_number):
        (i2c_bus, i2c_address) = GPIO._mcp230xx_settings()
        value = GPIO._i2c_read_register(i2c_bus, i2c_address, 0x00)
        if value is None:
            return
        gpio_bit = GPIO.pumps[pump_number]
        # pins come on as input by default (bit = 1 is input, 0 is output)
        if value & gpio_bit:
            value = value ^ gpio_bit
            GPIO._i2c_write_register(i2c_bus, i2c_address, 0x00, value)
        GPIO.turn_off_pump(pump_number)
        
    def init_pumps():
        print('init pump GPIOs')
        #for pump_number in GPIO.pumps:
        #    gpio_number = GPIO.pumps[pump_number]
        #    GPIO.init_gpio_platform(gpio_number)
        
        for pump_number in range(1,6):
            GPIO.init_gpio_mcp230xx(pump_number)
        

    def turn_on_pump(pump_number):
        print('turn on pump %d' % pump_number)
        if not pump_number in GPIO.pumps:
            print('pump %d not found in pump list.' % pump_number)
        GPIO._set_gpio_mcp230xx(pump_number, True)

    def turn_off_pump(pump_number):
        print('turn off pump %d' % pump_number)
        if not pump_number in GPIO.pumps:
            print('pump %d not found in pump list.' % pump_number)
        GPIO._set_gpio_mcp230xx(pump_number, False)

    def get_pump_state(pump_number):
        if not pump_number in GPIO.pumps:
            print('pump %d not found in pump list.' % pump_number)
        value = GPIO._get_gpio_mcp230xx(pump_number)
        if True == value:
            return 'on'
        else:
            return 'off'

    def _set_gpio_platform(gpio_number, value):
        gpio_name = 'gpio%d' % gpio_number
        base_path = '/sys/class/gpio/'
        gpio_path = base_path + gpio_name
        value_path = gpio_path + '/value'
        with open(value_path, 'w') as value_file:
            if value:
                value_file.write('1')
            else:
                value_file.write('0')

    def _get_gpio_platform(gpio_number):
        gpio_name = 'gpio%d' % gpio_number
        base_path = '/sys/class/gpio/'
        gpio_path = base_path + gpio_name
        value_path = gpio_path + '/value'
        with open(value_path, 'r') as value_file:
            value = value_file.read().strip()
            #print('value: "%s"' % value)
            if value == '1':
                return True
            else:
                return False

    def _set_gpio_mcp230xx(gpio_number, value):
        (i2c_bus, i2c_address) = GPIO._mcp230xx_settings()
        register_address = 0x09
        register_value = GPIO._i2c_read_register(i2c_bus, i2c_address, register_address)
        if register_value is None:
            return
        gpio_bit = GPIO.pumps[gpio_number]
        if (register_value & gpio_bit) != value:
            if value: # turn on (bit = 1)
                register_value = register_value | gpio_bit
            else: # turn off (bit = 0)
                register_value = register_value ^ gpio_bit
            GPIO._i2c_write_register(i2c_bus, i2c_address, register_address, register_value)

    def _get_gpio_mcp230xx(gpio_number):
        (i2c_bus, i2c_address) = GPIO._mcp230xx_settings()
        register_address = 0x09
        register_value = GPIO._i2c_read_register(i2c_bus, i2c_address, register_address)
        if register_value is None:
            return None
        gpio_bit = GPIO.pumps[gpio_number]
        if register_value & gpio_bit:
            return True
        else:
            return False

    def _i2c_read_register(i2c_bus, i2c_address, register):
        value = None
        try:
            cmd = 'i2cget -y %d 0x%x 0x%x' % (i2c_bus, i2c_address, register)
            #print(cmd)
            raw_output = subprocess.getoutput(cmd)
            #print(raw_output)
            value = int(raw_output, 0)
        except:
            print("An error happened trying to talk to i2c device 0x%x on bus %d" % (i2c_address, i2c_bus))
        return value

    def _i2c_write_register(i2c_bus, i2c_address, register, value):
        try:
            cmd = 'i2cset -y %d 0x%x 0x%x 0x%x' % (i2c_bus, i2c_address, register, value)
            #print(cmd)
            raw_output = subprocess.getoutput(cmd)
            #print(raw_output)
        except:
            print("An error happened trying to talk to i2c device 0x%x on bus %d" % (i2c_address, i2c_bus))
