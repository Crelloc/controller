import tkinter as tk
import datetime
import sys

from gui_frame import PageFrame
import gui_common as common

class DateTimePage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'D1'

        title_area = common.TitleArea(self, "Date and Time")
        title_area.pack(fill=tk.X)

        
        date_frame = tk.Frame(self, bg=common.Colors.BG)
        date_label = tk.Label(date_frame, text="Date:", font=common.Fonts.LABEL)
        date_label.pack(side=tk.LEFT)
        self.date_var = tk.StringVar()
        date_field = tk.Label(date_frame, textvariable=self.date_var, font=common.Fonts.LABEL)
        date_field.pack(side=tk.LEFT)
        edit_date_button = tk.Button(date_frame, text="Edit", font=common.Fonts.BUTTON,
                                     command=lambda: self.controller.show_frame(EditDatePage, DateTimePage),
                                     image=common.Icons.edit, compound=tk.NONE)
        edit_date_button.pack(side=tk.LEFT, padx=5)
        date_frame.pack(pady=(40,10))

        time_frame = tk.Frame(self, bg=common.Colors.BG)
        time_label = tk.Label(time_frame, text="Time:", font=common.Fonts.LABEL)
        time_label.pack(side=tk.LEFT)
        self.time_var = tk.StringVar()
        time_field = tk.Label(time_frame, textvariable=self.time_var, font=common.Fonts.LABEL)
        time_field.pack(side=tk.LEFT)
        edit_time_button = tk.Button(time_frame, text="Edit", font=common.Fonts.BUTTON,
                                     command=lambda: self.controller.show_frame(EditTimePage, DateTimePage),
                                     image=common.Icons.edit, compound=tk.NONE)
        edit_time_button.pack(side=tk.LEFT, padx=5)
        time_frame.pack(pady=10)

        self.update()

    def update(self):
        now = datetime.datetime.now()
        self.date_var.set(now.strftime("%x"))
        self.time_var.set("{0:%X}".format(now))

        


class EditDatePage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'D2'

        title_area = common.TitleArea(self, "Edit Date")
        title_area.pack(fill=tk.X)

        self.date_var = tk.StringVar()
        self.modifier = [0,0,0]
        self.selected_index = 0
        self.modifier_var = tk.StringVar()
        self.original_date_var = tk.StringVar()
        #tk.Label(self, textvariable=self.modifier_var, font=common.Fonts.LABEL).pack()
        #tk.Label(self, textvariable=self.original_date_var, font=common.Fonts.LABEL).pack()
        self.valid_values = {0: (1,12), 1:(1,31), 2:(2014,9999)}
        self.error_var = tk.StringVar()
        tk.Label(self, textvariable=self.error_var, font=common.Fonts.ERROR, fg="Red").pack(pady=(15, 2))

        self.textbox = tk.Entry(self, textvariable=self.date_var, font=common.Fonts.ENTRY, state="readonly", width=10)
        self.textbox.pack()
        buttonframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(buttonframe, text="<", command=self.move_left, image=common.Icons.back_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(buttonframe, text="+", command=self.raise_part, image=common.Icons.up_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(buttonframe, text="-", command=self.lower_part, image=common.Icons.down_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(buttonframe, text=">", command=self.move_right, image=common.Icons.forward_arrow)
        button.pack(side=tk.LEFT)
        buttonframe.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)

        self.update()
        self._show_selection()

    def reset(self):
        self.modifier = [0,0,0]
        now = datetime.datetime.now()
        self.date_var.set(now.strftime("%m/%d/%Y"))
        self.selected_index = 0
        self.error_var.set("")
        self._show_selection()

    def update(self):
        now = datetime.datetime.now()
        self.modifier_var.set(str(self.modifier))
        self.original_date_var.set("{0:%x}".format(now))
        year = now.year + self.modifier[2]
        month = now.month + self.modifier[0]
        day = now.day + self.modifier[1]
        date = datetime.date(year=year, month=month, day=day)
        self.date_var.set(date.strftime("%m/%d/%Y"))

    def _show_selection(self):
        self.textbox.selection_clear()
        selection_start = (self.selected_index * 2) + (self.selected_index * 1)
        selection_end = selection_start + 2
        if self.selected_index == 2:
            selection_end = selection_start + 4
        self.textbox.selection_range(selection_start, selection_end)
    
    def move_left(self):
        self.selected_index -= 1
        if self.selected_index < 0:
            self.selected_index = 0
        self._show_selection()
        self.error_var.set("")

    def move_right(self):
        self.selected_index += 1
        if 2 < self.selected_index:
            self.selected_index = 2
        self._show_selection()
        self.error_var.set("")

    def raise_part(self):
        self.error_var.set("")
        value = self.modifier[self.selected_index] + 1
        if self._validate(self.selected_index, value):
            self.modifier[self.selected_index] = value
        self.update()

    def lower_part(self):
        self.error_var.set("")
        value = self.modifier[self.selected_index] - 1
        if self._validate(self.selected_index, value):
            self.modifier[self.selected_index] = value
        self.update()

    def _validate(self, index, value):
        now = datetime.datetime.now()
        adjusted_values = [now.month, now.day, now.year]
        for i in range(3):
            if index == i:
                adjusted_values[i] += value
            else:
                adjusted_values[i] += self.modifier[i]
        # validate
        try:
            date = datetime.date(year=adjusted_values[2], month=adjusted_values[0], day=adjusted_values[1])
        except ValueError as ex:
            print(ex)
            self.error_var.set(str(ex))
            return False
        return True
            
    def save(self):
        now = datetime.datetime.now()
        year = now.year + self.modifier[2]
        month = now.month + self.modifier[0]
        day = now.day + self.modifier[1]
        date = datetime.datetime(year=year, month=month, day=day,
                                 hour=now.hour, minute=now.minute, second=now.second)
        date_str = "{0:%x} {0:%X}".format(date)
        print(date_str)
        self.controller.ipc.send_command("controller datetime %s" % date_str)
        self.controller.show_frame(DateTimePage)

    def cancel(self):
        self.modifier = [0,0,0]
        self.controller.show_frame(DateTimePage)

class EditTimePage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'D3'

        title_area = common.TitleArea(self, "Edit Time")
        title_area.pack(fill=tk.X)

        self.time_var = tk.StringVar()
        self.modifier = [0,0,0]
        self.selected_index = 0
        
        self.modifier_var = tk.StringVar()
        self.original_time_var = tk.StringVar()
        #tk.Label(self, textvariable=self.modifier_var, font=common.Fonts.LABEL).pack()
        #tk.Label(self, textvariable=self.original_time_var, font=common.Fonts.LABEL).pack()
        self.valid_values = {0: (0,23), 1:(0,59), 2:(0,59)}
        self.error_var = tk.StringVar()
        tk.Label(self, textvariable=self.error_var, font=common.Fonts.ERROR, fg="Red").pack(pady=(15, 2))

        self.textbox = tk.Entry(self, textvariable=self.time_var, font=common.Fonts.ENTRY, state="readonly", width=10)
        self.textbox.pack()
        buttonframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(buttonframe, text="<", command=self.move_left, image=common.Icons.back_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(buttonframe, text="+", command=self.raise_part, image=common.Icons.up_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(buttonframe, text="-", command=self.lower_part, image=common.Icons.down_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(buttonframe, text=">", command=self.move_right, image=common.Icons.forward_arrow)
        button.pack(side=tk.LEFT)
        buttonframe.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)

        self.update()
        self._show_selection()

    def reset(self):
        self.modifier = [0,0,0]
        now = datetime.datetime.now()
        self.time_var.set(now.strftime("%X"))
        self.selected_index = 0
        self.error_var.set("")
        self._show_selection()

    def update(self):
        now = datetime.datetime.now()
        self.modifier_var.set(str(self.modifier))
        self.original_time_var.set("{0:%X}".format(now))
        delta = datetime.timedelta(hours=self.modifier[0], minutes=self.modifier[1], seconds=self.modifier[2])
        time = now + delta
        self.time_var.set(time.strftime("%X"))

    def _show_selection(self):
        self.textbox.selection_clear()
        selection_start = (self.selected_index * 2) + (self.selected_index * 1)
        selection_end = selection_start + 2
        self.textbox.selection_range(selection_start, selection_end)
    
    def move_left(self):
        self.selected_index -= 1
        if self.selected_index < 0:
            self.selected_index = 0
        self._show_selection()
        self.error_var.set("")

    def move_right(self):
        self.selected_index += 1
        if 2 < self.selected_index:
            self.selected_index = 2
        self._show_selection()
        self.error_var.set("")

    def raise_part(self):
        self.error_var.set("")
        value = self.modifier[self.selected_index] + 1
        if self._validate(self.selected_index, value):
            self.modifier[self.selected_index] = value
        self.update()

    def lower_part(self):
        self.error_var.set("")
        value = self.modifier[self.selected_index] - 1
        if self._validate(self.selected_index, value):
            self.modifier[self.selected_index] = value
        self.update()

    def _validate(self, index, value):
        now = datetime.datetime.now()
        # validate
        try:
            delta = datetime.timedelta(hours=self.modifier[0], minutes=self.modifier[1], seconds=self.modifier[2])
            time = now + delta
        except ValueError as ex:
            print(ex)
            self.error_var.set(str(ex))
            return False
        return True
            
    def save(self):
        now = datetime.datetime.now()
        self.modifier_var.set(str(self.modifier))
        self.original_time_var.set("{0:%X}".format(now))
        delta = datetime.timedelta(hours=self.modifier[0], minutes=self.modifier[1], seconds=self.modifier[2])
        time = now + delta
        date = datetime.datetime(year=now.year, month=now.month, day=now.day,
                                 hour=time.hour, minute=time.minute, second=time.second)
        date_str = "{0:%x} {0:%X}".format(date)
        print(date_str)
        self.controller.ipc.send_command("controller datetime %s" % date_str)
        self.controller.show_frame(DateTimePage)

    def cancel(self):
        self.modifier = [0,0,0]
        self.controller.show_frame(DateTimePage)
