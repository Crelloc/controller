import tkinter as tk
import socket, fcntl, struct
import subprocess

from gui_frame import PageFrame
import gui_common as common
import parse_stats as can_stats

class SiteInfoPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'IS'

        title_area = common.TitleArea(self, "Site Info", self.next_page, None)
        title_area.pack(fill=tk.X)

        info_frame = tk.Frame(self, bg=common.Colors.BG)
        self.site_name_var = tk.StringVar()
        self.uc_code_var = tk.StringVar()
        self.box_cycle_var = tk.StringVar()
        self.change_day_var = tk.StringVar()
        self.modules_online_var = tk.StringVar()

        self._add_row(info_frame, 0, "Site name: ", self.site_name_var)
        self._add_row(info_frame, 1, "UC Code: ", self.uc_code_var)
        self._add_row(info_frame, 2, "Box cycle: ", self.box_cycle_var)
        self._add_row(info_frame, 3, "Change day: ", self.change_day_var)
        self._add_row(info_frame, 4, "Modules online: ", self.modules_online_var)

        info_frame.pack()

    def _add_row(self, frame, row, title, textvariable):
        label = tk.Label(frame, text=title, font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=0, sticky=tk.E, padx=10)
        label = tk.Label(frame, textvariable=textvariable, font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=1, sticky=tk.W)

    def next_page(self):
        self.controller.show_frame(ModuleInfoPage)

    def reset(self):
        self.site_name_var.set(self.controller.config.site_name)
        self.uc_code_var.set(self.controller.config.uc_code)
        self.box_cycle_var.set(self.controller.config.ebox.filter_cartridge_schedule)
        self.change_day_var.set(self.controller.config.ebox.cartridge_change_day)

    def update(self):
        modules_online = self.controller.ipc.get_data('list modules online')
        self.modules_online_var.set(str(modules_online))

class ModuleInfoPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'IM'

        self.module_index = 0
        self.modules = []

        title = 'Module %d' % self.module_index
        self.title_area = common.TitleArea(self, title, self.next_page, self.previous_page)
        self.title_area.pack(fill=tk.X)

        #['pressure_averaging_length', 'last_main_loop_count', 'last_cyclone_error_count', 'version_minor',
        #'pump_state', 'last_orifice_temperature', 'online', 'last_pressure_time', 'motor_going_up', 'version_major',
        #'ebox_schedule', 'last_cyclone_temperature', 'last_orifice_pressure', 'can_id', 'version_date_string',
        #'average_orifice_pressure', 'module_position', 'average_cyclone_pressure', 'solenoids', 'failed_cmd_count',
        #'last_cyclone_pressure', 'motor_going_down', 'last_orifice_error_count', 'quiet_mode', 'last_current_reading'

        self.online_var = tk.StringVar()
        self.last_pressure_time_var = tk.StringVar()
        self.last_cyclone_pressure_var = tk.StringVar()
        self.last_orifice_pressure_var = tk.StringVar()
        self.last_current_reading_var = tk.StringVar()
        self.sampling_active_var = tk.StringVar()
        self.previous_samdat_var = tk.StringVar()
        self.next_samdat_var = tk.StringVar()
        self.cartridge_schedule_var = tk.StringVar()
        self.cartridge_schedule_index_var = tk.StringVar()
        self.position_index_var = tk.StringVar()

        info_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(info_frame, textvariable=self.online_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=0, sticky=tk.E, padx=10)
        label = tk.Label(info_frame, textvariable=self.last_pressure_time_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=1, sticky=tk.W)
        self._add_row(info_frame, 1, 'Cyclone: ', self.last_cyclone_pressure_var)
        self._add_row(info_frame, 2, 'Orifice: ', self.last_orifice_pressure_var)
        self._add_row(info_frame, 3, 'Current: ', self.last_current_reading_var)
        self._add_row(info_frame, 4, 'Sampling active: ', self.sampling_active_var)
        self._add_row(info_frame, 5, 'Prev SamDat: ', self.previous_samdat_var)
        self._add_row(info_frame, 6, 'Next SamDat: ', self.next_samdat_var)
        self._add_row(info_frame, 7, 'Schedule: ', self.cartridge_schedule_var)
        #self._add_row(info_frame, 8, 'Current week: ', self.cartridge_schedule_index_var)
        label = tk.Label(info_frame, textvariable=self.cartridge_schedule_index_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=8, column=0, sticky=tk.E, padx=10)
        label = tk.Label(info_frame, textvariable=self.position_index_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=8, column=1, sticky=tk.W)
        info_frame.pack()

    def reset(self):
        self.module_index = 0
        self.modules = self.controller.ipc.get_data('list modules enabled')
        self._update_title()
        self.update()

    def update(self):
        module_number = 1
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        if True == data['online']:
            self.online_var.set('Online: ')
        else:
            self.online_var.set('Offline: ')
        self.last_pressure_time_var.set('{0:%x} {0:%X}'.format(data['last_pressure_time']))
        self.last_cyclone_pressure_var.set('{:0.6f} " H₂O'.format(data['last_cyclone_pressure']))
        self.last_orifice_pressure_var.set('{:0.6f} psi'.format(data['last_orifice_pressure']))
        self.last_current_reading_var.set('{:0.2f} mA'.format(data['last_current_reading']))
        schedule = data['ebox_schedule']
        self.sampling_active_var.set(str(schedule['sampling_active']))
        self.previous_samdat_var.set('{0:%x} {0:%X}'.format(schedule['previous_samdat']))
        self.next_samdat_var.set('{0:%x} {0:%X}'.format(schedule['next_samdat']))
        self.cartridge_schedule_var.set(str(schedule['cartridge_schedule']))
        self.cartridge_schedule_index_var.set('week %s, ' % str(schedule['cartridge_schedule_index'] + 1))
        self.position_index_var.set('position %s' % str(schedule['position_index']))
        

    def next_page(self):
        if (self.module_index + 1) < len(self.modules):
            self.module_index += 1
            self._update_title()
        else:
            self.controller.show_frame(ControllerInfoPage)

    def previous_page(self):
        if self.module_index == 0:
            self.controller.show_frame(SiteInfoPage)
        else:
            self.module_index -= 1
            self._update_title()

    def _update_title(self):
        title = 'Module X'
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]
            title = 'Module %d' % module_number
        self.title_area.update_title(title)

    def _add_row(self, frame, row, title, textvariable):
        label = tk.Label(frame, text=title, font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=0, sticky=tk.E, padx=10)
        label = tk.Label(frame, textvariable=textvariable, font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=1, sticky=tk.W)


class ControllerInfoPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'IS'

        title_area = common.TitleArea(self, "Ctrlr Info", None, self.previous_page)
        title_area.pack(fill=tk.X)

        info_frame = tk.Frame(self, bg=common.Colors.BG)

        self.can_state_var = tk.StringVar()
        self.can_bus_off_var = tk.StringVar()
        self.can_rx_var = tk.StringVar()
        self.can_tx_var = tk.StringVar()
        self.can_errors_var = tk.StringVar()
        self.ip_addr_var = tk.StringVar()
        self.mainboard_current_var = tk.StringVar()
        
        self._add_row(info_frame, 0, "CAN state: ", self.can_state_var)
        self._add_row(info_frame, 1, "Bus restarts: ", self.can_bus_off_var)
        self._add_row(info_frame, 2, "CAN tx: ", self.can_tx_var)
        self._add_row(info_frame, 3, "CAN rx: ", self.can_rx_var)
        self._add_row(info_frame, 4, "CAN errors: ", self.can_errors_var)
        self._add_row(info_frame, 5, "IP addr: ", self.ip_addr_var)
        self._add_row(info_frame, 6, "MnBd current: ", self.mainboard_current_var)

        info_frame.pack()

    def _add_row(self, frame, row, title, textvariable):
        label = tk.Label(frame, text=title, font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=0, sticky=tk.E, padx=10)
        label = tk.Label(frame, textvariable=textvariable, font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=1, sticky=tk.W)

    def previous_page(self):
        self.controller.show_frame(ModuleInfoPage)

    def reset(self):
        #self.site_name_var.set(self.controller.config.site_name)
        #self.uc_code_var.set(self.controller.config.uc_code)
        #self.box_cycle_var.set(self.controller.config.ebox.filter_cartridge_schedule)
        #self.change_day_var.set(self.controller.config.ebox.cartridge_change_day)
        self.old_can_stats = can_stats.get_can_statistics()

    def update(self):
        #modules_online = self.controller.ipc.get_data('list modules online')
        #self.modules_online_var.set(str(modules_online))
        new_can_stats = can_stats.get_can_statistics()
        diff_values = can_stats.compare_statistics(self.old_can_stats, new_can_stats)
        self.can_state_var.set(new_can_stats['state'])
        self.can_bus_off_var.set('%d' % new_can_stats['bus-off'])
        self.can_tx_var.set('%d (%d)' % (new_can_stats['tx-packets'], diff_values['tx-packets']))
        self.can_rx_var.set('%d (%d)' % (new_can_stats['rx-packets'], diff_values['rx-packets']))
        self.can_errors_var.set('%d (%d)' % (new_can_stats['total-errors'], diff_values['total-errors']))
        self.old_can_stats = new_can_stats
        self.ip_addr_var.set(self._get_ip_address())
        self.mainboard_current_var.set(self._get_mainboard_current())

    def _get_ip_address(self):
        interfaces = [
            'eth0',
            'wlan0',
            'wifi0'
            ]
        for ifname in interfaces:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                var = struct.pack('256s', bytes(ifname[:15], 'utf-8'))
                ip = socket.inet_ntoa(fcntl.ioctl(
                    s.fileno(),
                    0x8915, # SIOCGIDADDR
                    var
                    )[20:24])
                #print(ifname)
                #print(ip)
                return ip
            except IOError:
                pass
        # nothing found
        return ''

    def _get_mainboard_current(self):
        path = "../tools/acs764_registers.py"
        cmd = "python3 " + path
        raw_output = subprocess.getoutput(cmd)
        lines = raw_output.split('\n')
        values = {}
        for line in lines:
            sections = line.split(':')
            key = sections[0]
            value = sections[1]
            values[key] = value
        str_current = values['current'].strip()
        current = float(str_current)
        raw_current = values['raw_current']
        gain_range = values['gain_range']
        output = '%.2f mA (raw:%s,%s)' % (current, raw_current, gain_range)
        
        return output
        
            
class MainMenuInfoPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'HELP'

        self.module_index = 0
        self.modules = []

        title = 'Help Page' 
