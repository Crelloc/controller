import subprocess
import re
import datetime
import time

def get_can_statistics(verbose=False):
    values = {}

    cmd = 'ip -details -stats link show can0'
    raw_output = subprocess.check_output(cmd, shell=True)
    output = raw_output.decode('utf-8').split('\n')

    if verbose:
        index = 0
        for line in output:
            print('%0.2d: %s' % (index, line))
            index += 1

    expression = 'can state \S+'
    p = re.compile(expression)
    match = p.findall(output[2])[0]
    state = match.strip().split(' ')[2]
    if verbose:
        print('state: "%s"' % state)
    values['state'] = state
        
    expression = '\wx \d+'
    p = re.compile(expression)
    matches = p.findall(output[2])
    for match in matches:
        split = match.split(' ')
        name = 'berr-%s' % split[0]
        values[name] = int(split[1])

    expression = '\s+\S+'
    p = re.compile(expression)
    names = p.findall(output[7])
    numbers = p.findall(output[8])
    for index in range(len(names)):
        name = names[index].strip()
        number = numbers[index].strip()
        values[name] = int(number)

    names = p.findall(output[9])
    numbers = p.findall(output[10])
    # there is an extra item ('RX:') so skip it by starting from 1
    for index in range(1, len(names)):
        name = 'rx-' + names[index].strip()
        number = numbers[index - 1].strip()
        values[name] = int(number)

    names = p.findall(output[11])
    numbers = p.findall(output[12])
    # there is an extra item ('TX:') so skip it by starting from 1
    for index in range(1, len(names)):
        name = 'tx-' + names[index].strip()
        number = numbers[index - 1].strip()
        values[name] = int(number)

    # sum all the errors
    total_rx_errors = values['rx-errors'] + values['rx-dropped'] + values['rx-overrun']
    total_tx_errors = values['tx-errors'] + values['tx-dropped'] + values['tx-carrier']
    total_errors = total_rx_errors + total_tx_errors
    values['total-errors'] = total_errors

    if verbose:
        print(values)
    return values

def print_can_statistics(values):
    print('state: %s' % values['state'], end='')
    print(', tx: %d, rx: %d' % (values['tx-packets'], values['rx-packets']), end='')
    print(', total-err: %d' % values['total-errors'], end='')
    print(', berr-tx: %d, berr-rx: %d' % (values['berr-tx'], values['berr-rx']), end='')
    print(', tx-errors: %d, rx-errors: %d' % (values['tx-errors'], values['rx-errors']), end='')
    print('')

def compare_statistics(old_values, new_values):
    diff_values = {}
    for key in new_values.keys():
        if key == 'state':
            continue
        diff_values[key] = new_values[key] - old_values[key]
    diff_values['state'] = new_values['state']
    return diff_values


if __name__ == "__main__":
    
    old_values = get_can_statistics(verbose=True)
    line_number = 0

    try:
        while True:
            values = get_can_statistics()
            #print_can_statistics(values)
            diff_values = compare_statistics(old_values, values)
            print('%0.2d ' % line_number, end='')
            print_can_statistics(diff_values)
            

            old_values = values
            line_number += 1
            if 99 < line_number:
                line_number = 0
            time.sleep(1)
    except KeyboardInterrupt:
        print("...done")
        
