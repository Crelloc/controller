import tkinter as tk

from gui_frame import PageFrame
import gui_common as common

class CalibrationPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'IM'

        self.module_index = 0
        self.modules = []

        title = 'Module %d' % self.module_index
        self.title_area = common.TitleArea(self, title, self.next_page, self.previous_page)
        self.title_area.pack(fill=tk.X)

        #['pressure_averaging_length', 'last_main_loop_count', 'last_cyclone_error_count', 'version_minor',
        #'pump_state', 'last_orifice_temperature', 'online', 'last_pressure_time', 'motor_going_up', 'version_major',
        #'ebox_schedule', 'last_cyclone_temperature', 'last_orifice_pressure', 'can_id', 'version_date_string',
        #'average_orifice_pressure', 'module_position', 'average_cyclone_pressure', 'solenoids', 'failed_cmd_count',
        #'last_cyclone_pressure', 'motor_going_down', 'last_orifice_error_count', 'quiet_mode', 'last_current_reading'

        self.paused = False
        self.online_var = tk.StringVar()
        self.last_pressure_time_var = tk.StringVar()
        self.last_cyclone_pressure_var = tk.StringVar()
        self.last_orifice_pressure_var = tk.StringVar()
        self.last_current_reading_var = tk.StringVar()
        self.sampling_active_var = tk.StringVar()
        self.previous_samdat_var = tk.StringVar()
        self.next_samdat_var = tk.StringVar()
        self.cartridge_schedule_var = tk.StringVar()
        self.cartridge_schedule_index_var = tk.StringVar()
        self.position_index_var = tk.StringVar()
        self.pump_solenoid_status_var = tk.StringVar()
        self.solenoid_1_status_var = tk.StringVar()
        self.solenoid_2_status_var = tk.StringVar()
        self.solenoid_3_status_var = tk.StringVar()
        self.solenoid_4_status_var = tk.StringVar()
        self.solenoid_status = []
        for index in range(4):
            self.solenoid_status.append(tk.StringVar())
        self.pump_status_var = tk.StringVar()
        self.running_status_var = tk.StringVar()

        info_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(info_frame, textvariable=self.online_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=0, sticky=tk.E, padx=10)
        label = tk.Label(info_frame, textvariable=self.last_pressure_time_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=1, sticky=tk.W)
        info_frame.pack()

        info_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(info_frame, text="Cyc: ", font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=0, sticky=tk.E, padx=10)
        label = tk.Label(info_frame, textvariable=self.last_cyclone_pressure_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=1, padx=10)
        label = tk.Label(info_frame, text="(Flow here?)", font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=2, padx=10)

        label = tk.Label(info_frame, text="Ori: ", font=common.Fonts.SMALL_LABEL)
        label.grid(row=1, column=0, sticky=tk.E, padx=10)
        label = tk.Label(info_frame, textvariable=self.last_orifice_pressure_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=1, column=1, padx=10)
        label = tk.Label(info_frame, text="(Flow here?)", font=common.Fonts.SMALL_LABEL)
        label.grid(row=1, column=2, padx=10)
        info_frame.pack(pady=(10,0))

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, textvariable=self.solenoid_status[0], font=common.Fonts.BUTTON,
                           command=lambda solenoid_number=1: self._solenoid_command(solenoid_number))
        button.pack(side=tk.LEFT)
        button = tk.Button(button_frame, textvariable=self.solenoid_status[1], font=common.Fonts.BUTTON,
                           command=lambda solenoid_number=2: self._solenoid_command(solenoid_number))
        button.pack(side=tk.LEFT)
        button = tk.Button(button_frame, textvariable=self.solenoid_status[2], font=common.Fonts.BUTTON,
                           command=lambda solenoid_number=3: self._solenoid_command(solenoid_number))
        button.pack(side=tk.LEFT)
        button_frame.pack(pady=(20,0))

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, textvariable=self.solenoid_status[3], font=common.Fonts.BUTTON,
                           command=lambda solenoid_number=4: self._solenoid_command(solenoid_number))
        button.pack(side=tk.LEFT)
        button = tk.Button(button_frame, textvariable=self.pump_status_var, font=common.Fonts.BUTTON,
                           command=self._pump_command)
        button.pack(side=tk.LEFT)
        button = tk.Button(button_frame, textvariable=self.running_status_var, font=common.Fonts.BUTTON,
                           command=self._pause_button_clicked)
        button.pack(side=tk.LEFT)
        button_frame.pack()

    def reset(self):
        self.paused = False
        self.running_status_var.set("||")
        self.module_index = 0
        self.modules = self.controller.ipc.get_data('list modules enabled')
        self._update_title()
        self.update()

    def update(self):
        if self.paused:
            return
        
        module_number = 1
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        if True == data['online']:
            self.online_var.set('Online: ')
        else:
            self.online_var.set('Offline: ')
        self.last_pressure_time_var.set('{0:%x} {0:%X}'.format(data['last_pressure_time']))
        self.last_cyclone_pressure_var.set('{:0.6f} " H₂O'.format(data['last_cyclone_pressure']))
        self.last_orifice_pressure_var.set('{:0.6f} psi'.format(data['last_orifice_pressure']))
        self.last_current_reading_var.set('{:0.2f} mA'.format(data['last_current_reading']))
        schedule = data['ebox_schedule']
        self.sampling_active_var.set(str(schedule['sampling_active']))
        self.previous_samdat_var.set('{0:%x} {0:%X}'.format(schedule['previous_samdat']))
        self.next_samdat_var.set('{0:%x} {0:%X}'.format(schedule['next_samdat']))
        self.cartridge_schedule_var.set(str(schedule['cartridge_schedule']))
        self.cartridge_schedule_index_var.set('week %s, ' % str(schedule['cartridge_schedule_index'] + 1))
        self.position_index_var.set('position %s' % str(schedule['position_index']))

        solenoids = data['solenoids']
        for solenoid_index in range(4):
            solenoid = solenoids[solenoid_index]
            solenoid_number = solenoid_index + 1
            if solenoid.name == 'Closed':
                self.solenoid_status[solenoid_index].set('S%d:Off' % solenoid_number)
            elif solenoid.name == 'Open':
                self.solenoid_status[solenoid_index].set('S%d:On' % solenoid_number)
            else:
                self.solenoid_status[solenoid_index].set('S%d:??' % solenoid_number)
        
        pump_state = data['pump_state']
        if pump_state == 'on':
            self.pump_status_var.set('Pump:On')
        else:
            self.pump_status_var.set('Pump:Off')
        
    def next_page(self):
        if (self.module_index + 1) < len(self.modules):
            self.module_index += 1
            self._update_title()
        self.update()

    def previous_page(self):
        if self.module_index == 0:
            #self.controller.show_frame(SiteInfoPage)
            pass
        else:
            self.module_index -= 1
            self._update_title()
        self.update()

    def _solenoid_command(self, solenoid_number):
        if self.paused:
            return
        
        module_number = 1
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        solenoids = data['solenoids']
        solenoid_index = solenoid_number - 1
        solenoid = solenoids[solenoid_index]
        if False == data['online']:
            return
        cmd = 'module %d solenoid %d ' % (module_number, solenoid_number)
        if solenoid.name == 'Closed':
            cmd += 'on'
        elif solenoid.name == 'Open':
            cmd += 'off'
        else:
            cmd += 'off'
        cmd += ' inclusive'
        self.controller.ipc.send_command(cmd)

    def _pump_command(self):
        if self.paused:
            return
        
        module_number = 1
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        pump_state = data['pump_state']
        if False == data['online']:
            return
        cmd = 'pump %d off' % module_number
        if pump_state == 'off':
            cmd = 'pump %d on' % module_number
        self.controller.ipc.send_command(cmd)

    def _update_title(self):
        title = 'Module X'
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]
            title = 'Module %d' % module_number
        self.title_area.update_title(title)

    def _pause_button_clicked(self):
        if self.paused:
            self.paused = False
            self.running_status_var.set("||")
        else:
            self.paused = True
            self.running_status_var.set(">")
        self.update()
