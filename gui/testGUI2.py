# User Interface for New Controller
# Last edited: 02/05/2015
# Edited by: Nicholas Spada

try: import Tkinter as tk                       # Python 2.x
except ImportError: import tkinter as tk        # Python 3.x
##from PIL import ImageTk, Image                # Need to install for images
import time                                     # Needed for getting system time and displaying it
import string                                   # Needed for displaying alphabet for user input
import random                                   # Temporary, for assigning values to readings

# Define global variables
# Display values, also change in the editDisplay class
##wFG = '#C99700'                                 # UCD Gold - window 
##wBG = '#002855'                                 # UCD Blue - window
##bBG = wFG                                       # UCD Gold - button
wFG = '#3DF25E'                                 # Green - window 
wBG = 'black'                                   # Black - window
bBG = wFG                                       # Green - button
iFont = 'Lucida Sans'                           # UCD Font - All

letts = list(string.ascii_uppercase)            # All the uppercase letters
numbs = list(range(10))                        # 0 - 9

# Keep track of frames being viewed so we can more efficiently navigate. This system isn't
# very smart. Can get stuck in a "back" loop. You can still press "home" but makes the
# back button less useful
ppf = 'home'                                    # Prior to previous frame
previousFrame = 'home'
currentFrame = 'home'
pipe = ''                                       # Which sequence of frames is the user navigating
finalsDone = 0                                  # Toggle to indicate whether final readings have taken place
initialsDone = 0                                # Toggle to indicate whether initial readings have taken place


opIndex = 0                                     # Toggle variable for determining what to do with
                                                # operator initials. 0 means operator selected a
                                                # value in the menubox, 1 means operator entered a
                                                # different set of initials.

# Define site info variables
siteName = 'ACAD1'                              # Set in Site Config screen
currentWeek = 2                                 # Determined by schedule
ucCode = 4027                                   # Set by techs
boxCycle = '3-2-2'                              # Set by techs
changeDay = 'Tuesday'                           # Standard
nextSample = '02/10/15'                         # Needs to be set by scheduler
status = 'Idle (Ready for sample change)'       # Status determined by time and state of HW
statBG = 'green'                                # BG should convey state of HW (grn, ylw, rd)
special = 0                                     # Determines if filter change is normal or special
oprtrs = [ 'ABC', 'DEF', 'GHI' ]                # Operator initials
currentOp = oprtrs[0]                           # Operating Operator

# Equipment parameters
param = [ 'ori', 'cyc', 'et', 'mxori' ]         # Filter reading parameters requiring variables
mods = [ 'A', 'B', 'C', 'D' ]                   # Variable modules, set up in site config
pos = [ 1, 2, 3 ]                               # Positions need to be set by week parameter
prepo = [ 'Initial', 'Final' ]                  # Pre/Post sampling status
currentMod = mods[0]                            # Identify current module for display
equipment = [ 'Pump', 'Pump Hose', 'E-Box', 'Relay Box', 'Module', 'Module Cable', 'Controller',
              'Stack', 'PM2.5 Inlet', 'Sierra Inlet', 'Manifold', 'Cyclone', 'Motor', 'Temp Probe',
              'Denuder' ]
# Parts for a specific module (note for multiple PM10 samplers, Sierra inlet may have multiple
# mods, same for denuder.)
modParts = [ 'Pump', 'Pump Hose', 'E-Box', 'Module', 'Module Cable', 'Stack', 'PM2.5 Inlet',
             'Sierra Inlet', 'Manifold', 'Cyclone', 'Motor', 'Denuder' ]
# Keep track of which part is selected for replacment instructions
currentPart = ''



# Use dictionary for storing each measured parameter
# Naming convention is: ex. "A1_Initial_ori"
readings = {}                                   # Dictionary containing each filter's readings
for x in prepo:
    for y in pos:
        for mod in mods:
            for z in param:
                name = mod + str(y) + '_' + x + '_' + z
                value = 10 * random.random()              # Placeholder value
                readings[name] = value


##homeImg = ImageTk.PhotoImage(Image.open('Home.gif'))    # Image for Home button
##backImg = ImageTk.PhotoImage(Image.open('Back.gif'))    # Image for Back button
##nextImg = ImageTk.PhotoImage(Image.open('Next.gif'))    # Image for Next button
##wIMG = '5'                                              # Width of button image in characters
##hIMG = '5'                                              # Height of button image in characters

########################################################################################################
#####################           Define Classes for Interface                 ########################
########################################################################################################

class Clock(tk.Tk):
    """ Keep track of time """
    def __init__(self, parent):
        # Create the clock widget, it's an ordinary Label element
        self.time = time.strftime("%H:%M:%S")
        self.widget = tk.Label(parent, text=self.time)
        self.widget.after(200, self.tick)       # Wait 200 ms, then run tick()

    def tick(self):
        # Update the display clock
        new_time = time.strftime("%H:%M:%S")
        
        if new_time != self.time:
            self.time = new_time
            self.widget.config(text=self.time)
        self.widget.after(200, self.tick)       # 200 =  millisecond delay

class Day(tk.Tk):
    """ Keep track of dates """
    def __init__(self, parent):
        # Create the day widget, it's an ordinary Label element
        self.date = time.strftime("%m-%d-%Y")
        self.widget = tk.Label(parent, text=self.date)
        self.widget.after(200, self.tock)       # Wait 200 ms, then run tock()
        
    def tock(self):
        # Update the display clock
        new_date = time.strftime("%m-%d-%Y")
        
        if new_date != self.date:
            self.date = new_date
            self.widget.config(text=self.date)
        self.widget.after(200, self.tock)       # 200 =  millisecond delay

class currentOps(tk.Tk):
    """ Keeping track of operator initials. Operators can select and add initials during filter
        readings and there is a separate screen where you can add and delete operators.         """
    def __init__(self, parent):
        global oprtrs
        self.ops = tk.StringVar()
        self.ops.set(oprtrs[0])
        self.ops.trace('w', parent.menuCall)
        self.widget = tk.OptionMenu(parent, self.ops, *oprtrs)
        self.ops.list = oprtrs
        self.widget.after(200, self.opsUpdate)

    def opsChange(self, menuOp, altOp):
        global opIndex
        global oprtrs
        global currentOp

        if menuOp == '' or opIndex == 1:
            currentOp = altOp
            if currentOp in oprtrs:
                oprtrs.insert(0, oprtrs.pop(oprtrs.index(currentOp)))
            else:
                oprtrs.insert(0, currentOp)                         # Move new OPRTR to front
            
        else:
            currentOp = menuOp
            oprtrs.insert(0, oprtrs.pop(oprtrs.index(currentOp)))   # Move currentOp to front
        
        self.ops.set(oprtrs[0])                                     # Now update menu list
        self.widget['menu'].delete(0, 'end')                        # Clear out old menu
        
        # Add first three operator initials to list
        for op in oprtrs[0:3]:
            self.widget['menu'].add_command(label=op, command=tk._setit(self.ops, op))
##        self.widget['menu'].add_command(label='Other', command=tk._setit(self.ops, 'Other'))
            
        return oprtrs

    def opsUpdate(self):
        global oprtrs
        if self.ops.list != oprtrs:
            try:
                self.ops.set(oprtrs[0])
            except Exception:
                self.ops.set('')
            self.widget['menu'].delete(0, 'end')                    # Clear out old menu

            # Add first three operator initials to list
            for op in oprtrs[0:3]:
                self.widget['menu'].add_command(label=op, command=tk._setit(self.ops, op))
##          self.widget['menu'].add_command(label='Other', command=tk._setit(self.ops, 'Other'))
            self.ops.list = oprtrs
        self.widget.after(200, self.opsUpdate)
        return oprtrs

class currentOpsLB(tk.Tk):
    """ Keeping track of operator initials (in the editing listbox). Operators can select and add
        initials during filter readings and there is a separate screen where you can add and
        delete operators.         """
    def __init__(self, parent):
        global oprtrs
        self.ops2 = tk.StringVar()
        self.ops2.set(oprtrs[0])
        self.ops2.trace('w', parent.menuCall)
        self.widget2 = tk.Listbox(parent)
        self.ops2.list = oprtrs
        for oper in oprtrs: self.widget2.insert(tk.END, oper)
        self.opsLBUpdate()

    def opsLBUpdate(self):
        # Doesn't get values from opInits for some reason.
        global oprtrs, currentFrame
        if self.ops2.list != oprtrs and str(currentFrame)[9:] == 'editOps':
           self.ops2.list = oprtrs
           self.widget2.delete(0, tk.END)                    # Clear out old menu
           for oper in oprtrs: self.widget2.insert(tk.END, oper)
           try: self.ops2.set(oprtrs[0])
           except Exception: self.ops2.set('')
        self.widget2.after(200, self.opsLBUpdate)
        return oprtrs

class currentMod(tk.Tk):
    """ Keeping track of mods in UI screens (display data for one at a time and be able to cycle
        around). Also needs to be flexible for cases of X mods, special studies, etc. """
    def __init__(self, parent):
        # Create mod label widget
        self.mod = tk.StringVar()
        self.modIndex = tk.IntVar()
        self.mod.set(mods[0])
        self.widget = tk.Label(parent, textvariable=self.mod)   

    def modChange(self, value):
        nextMod = self.modIndex.get() + value
        if nextMod == -1:
            self.modIndex.set(len(mods) - 1)    # If on first and going backwards, go to last
        elif nextMod == len(mods):
            self.modIndex.set(0)                # If on last and going forwards, go to first
        else:
            self.modIndex.set(nextMod)
        self.mod.set(mods[self.modIndex.get()])

class value(tk.Tk):
    """ Keeping track of values in UI screens (display data for one at a time and be able to cycle
        around). Also needs to be flexible for cases of X mods, special studies, etc. """
    def __init__(self, parent, module, position, prepost, parameter):
        # Create mod label widget
        self.mod = tk.DoubleVar()
        self.modIndex = tk.IntVar()
        # Create key for readings dictionary based on input
        key = module + str(position) + '_' + prepost + '_' + parameter
        # Loop to find value in readings and assign it to the widget
        for k, v in readings.items():
            if k == key: self.mod.set(round(v, 3))
        self.widget = tk.Label(parent, textvariable=self.mod)  

    def modChange(self, value, position, prepost, parameter):
        nextMod = self.modIndex.get() + value
        if nextMod == -1:
            self.modIndex.set(len(mods) - 1)    # If on first and going backwards, go to last
        elif nextMod == len(mods):
            self.modIndex.set(0)                # If on last and going forwards, go to first
        else:
            self.modIndex.set(nextMod)
        newMod = mods[self.modIndex.get()]
        # Create new key for readings dictionary based on input
        key = newMod + str(position) + '_' + prepost + '_' + parameter
        # Loop to find new value in readings and reassign it to the widget
        for k, v in readings.items():
            if k == key: self.mod.set(round(v, 3))
    
                                               
class Application(tk.Tk):
    """ This is the controller for the running application. """
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.wm_title(self, "Controller GUI")
        self.window = tk.Frame(self)
        
        self.window.pack(side="top", fill="both", expand = True)
        
        self.window.grid_rowconfigure(0, weight=1)
        self.window.grid_columnconfigure(0, weight=1)

        # Operator initials scrollbox values
        self.oper = currentOps(self)
        self.operLB = currentOpsLB(self)
        self.oper1 = tk.StringVar(self)
        self.oper2 = tk.StringVar(self)
        self.oper3 = tk.StringVar(self)

        self.frames = {}

        framesList = [
            home,
            mainMenu,
            info,
            settings,
            editTime,
            editOps,
            editDisplay,
            whatEquip,
            whatMod,
            eqInstruct,
            moreEq,
            quick,
            quick2,
            advanced,
            opInits,
            modTest,
            noErrors,
            badPump,
            sysError,
            troubleGen,
            finalReadings,
            changeNormal,
            changeSpecial,
            checkInstall,
            initialReadings
            ]

        for F in (framesList):
            frame = F(self.window, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
            frame.configure(background=wBG)

        self.show_frame(home)

    # Show a different frame and keep track of previous frame
    def show_frame(self, win):
        global ppf, previousFrame, currentFrame
        ppf = previousFrame
        previousFrame = currentFrame
        currentFrame = win
        frame = self.frames[win]
        frame.tkraise()
        return ppf, previousFrame, currentFrame

    def updatePipe(self, path):
        global pipe
        pipe = path
        return pipe

    def backOps(self):
        global pipe
        if pipe == 'filRead': self.show_frame(home)
        elif pipe == 'eqChange': self.show_frame(mainMenu)
        return pipe

    def postOps(self):
        global pipe
        if pipe == 'filRead': self.preModCheck()
        elif pipe == 'eqChange': self.show_frame(whatEquip)
        return pipe
        
    def backModCheck(self):
        global pipe, currentPart, special, previousFrame, ppf
        if pipe == 'filRead':
            if finalsDone == 0: self.show_frame(opInits)
            elif special == 0: self.show_frame(changeNormal)
            else: self.show_frame(changeSpecial) 
        elif pipe == 'eqChange':
            if str(previousFrame)[9:] == 'moreEq': self.show_frame(previousFrame)
            elif str(previousFrame)[9:] == 'sysError':
                if str(ppf)[9:] == 'moreEq': self.show_frame(ppf)
                else: self.show_frame(whatEq)
            else:
                try:
                    currentPart = part
                    if part in modParts:
                        self.show_frame(whatMod)
                    else:
                        self.show_frame(whatEquip)
                except:
                    self.show_frame(whatEquip)
        return pipe, currentPart, special, previousFrame, ppf


    def postModCheck(self):
        """ Make sure we navigate to the correct area after finishing the system check. """
        global pipe, finalsDone, previousFrame, ppf
        error = random.random()
        if error > 0.75: self.show_frame(sysError)
        elif pipe == 'filRead':
            if finalsDone == 0: self.show_frame(finalReadings)
            elif initialsDone == 0: self.show_frame(initialReadings)
            else: self.show_frame(noErrors)
        elif pipe == 'eqChange':
            if str(previousFrame)[9:] == 'moreEq': self.show_frame(moreEq)
            elif str(previousFrame)[9:] == 'sysError':
                if str(ppf)[9:] == 'moreEq': self.show_frame(noErrors)
                else: self.show_frame(eqInstruct)
            else: self.show_frame(eqInstruct)
        return pipe, finalsDone, previousFrame, ppf

    def preInits(self):
        print('Testing mods . . . ')                         # Test system here
        # Time delay not working
##        start = int(time.time())
##        delay = 10
##        stop = start + delay
##        while True:
##            self.show_frame(checkInstall)
##            if int(time.time()) == stop:
##                break
        self.preInits2()
            

    def preInits2(self):
        error = random.random()
        if error > 0.75: self.show_frame(sysError)
        else: self.show_frame(initialReadings)

    def preModCheck(self):
        print('Testing mods . . . ')                         # Perform system test
        self.show_frame(modTest)
            
    def postBadPump(self):
        global pipe
        if pipe == 'filRead': self.show_frame(finalReadings)
        elif pipe == 'eqChange': self.show_frame(eqInstruct)
        else: self.show_frame(quick)

    def troubleshoot(self):
        """ Create different pathways depending on the error. """
        self.show_frame(troubleGen)                         # Generic troubleshooting frame
         
    def post_whatEquip(self, part):
        currentPart = part
        if part in modParts:
            self.show_frame(whatMod)
        else:
            self.show_frame(modTest)

    def combine_funcs(*funcs):
        def combined_func(*args, **kwargs):
            for f in funcs:
                f(*args, **kwargs)
        return combined_func

    def postFinals(self):
        """ Check for special filter change and toggle finalsDone """
        global special
        if special == 0: self.show_frame(changeNormal)
        else: self.show_frame(changeSpecial)
        self.toggleFinals()
        return special

    def menuCall(self, *args):
        # Placeholder, unless need to do something else
        return

    def timeUpdate(self, time, date):
        # Update system time and date
        return #time and date

    def changeD(self):
        """ Change display settings """
        global wBG
        wBG='green'
        temp = Application()
        temp.geometry('384x197+600+600')
        temp.update_idletasks()
        return wBG

    def toggleFinals(self):
        global finalsDone
        if finalsDone == 0: finalsDone = 1
        else: finalsDone = 0
        return finalsDone

    def toggleInitials(self):
        global initialsDone
        if initialsDone == 0: initialsDone = 1
        else: initialsDone = 0
        return initialsDone

########################################################################################################
#####################           Begin User Interface Frames                     ########################
########################################################################################################
 
class home(tk.Frame):
    """     The Auto Mode Home Screen, the one the OPRTR will be presented with on arrival  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        self.clock = Clock(self)
        self.clock.widget.configure(foreground=wFG, background=wBG,
                           font=(iFont, 16), anchor="center", justify="center")
        self.clock.widget.grid(columnspan=3, padx=145, pady=3)
        
        self.day = Day(self)
        self.day.widget.configure(foreground=wFG, background=wBG,
                           font=(iFont, 11), anchor="center", justify="center")
        self.day.widget.grid(row=1, columnspan=3)

        self.siteLabel = tk.Label(self, text='Site Name: %s' % siteName, foreground=wFG,
                               background=wBG, font=(iFont, 11)).grid(row=2, columnspan=3)

        self.filterweekLabel = tk.Label(self, text='Sampling Filter Set Week %s' % currentWeek,
                                     foreground=wFG, background=wBG, font=(
                                         iFont, 11)).grid(row=3, columnspan=3)

        self.statusLabel = tk.Label(self, text='Status: %s' % status, foreground='black',
                                    background=statBG, font=(iFont, 11)).grid(row=4,
                                                                               columnspan=3)
        self.button1 = tk.Button(self, text='Filter \n Readings', width=10, height=2,
                              background=bBG, font=(iFont, 10), borderwidth=8,
                                 comman=lambda: controller.combine_funcs(
                                     controller.updatePipe('filRead'),
                                     controller.show_frame(opInits))).grid(row=5, column=0, pady=8)
        self.button2 = tk.Button(self, text='Info', width=10, height=2, borderwidth=8,
                              background=bBG, font=(iFont, 10),
                                 command=lambda: controller.show_frame(info)).grid(
                                  row=5, column=1, pady=8)
        self.button3 = tk.Button(self, text='Menu', width=10, height=2, borderwidth=8,
                              background=bBG, font=(iFont, 10),
                                 command=lambda: controller.show_frame(mainMenu)).grid(
                                  row=5, column=2, pady=8)

class mainMenu(tk.Frame):
    """     This menu directs the user to everything besides doing filter readings  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Main Menu', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=2, padx=80, pady=10,
                                        columnspan=2, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont, 10),
                              command=lambda: controller.show_frame(home)
                              ).grid(row=3, column=0, sticky='sw')

        self.grid_columnconfigure(1, minsize=20)

        self.siteDetails = tk.Button(self, text='Settings', width=10, height=2,
                              background=bBG, font=(iFont, 10), borderwidth=8,
                                command=lambda: controller.show_frame(settings)).grid(row=1,
                                column=2, padx=10, pady=6, sticky='w')
        self.quickChecks = tk.Button(self, text='Quick \n Checks', width=10, height=2,
                                     borderwidth=8, background=bBG, font=(iFont, 10),
                                     command=lambda: controller.show_frame(quick)).grid(
                                         row=2, column=2, padx=10, pady=5, sticky='w')
        self.equip = tk.Button(self, text='Equipment \n Change', width=10, height=2, borderwidth=8,
                              background=bBG, font=(iFont, 10),
                               command=lambda: controller.combine_funcs(
                                   controller.updatePipe('eqChange'),
                                   controller.show_frame(opInits))
                               ).grid(row=1, column=3, pady=6, padx=10, sticky='w')
        self.advanced = tk.Button(self, text='Advanced', width=10, height=2, borderwidth=8,
                              background=bBG, font=(iFont, 10),
                                  command=lambda: controller.show_frame(advanced)).grid(row=2,
                                    column=3, pady=5, padx=10, sticky='w')

class info(tk.Frame):
    """     This screen lists all pertinent information for the OPRTR and tech helper   """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Site Info', foreground=wFG, background=wBG,
                                        font=(iFont, 12)).grid(row=0, column=1, padx=80, pady=10,
                                            columnspan=2, sticky='ew')

         # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)
                              ).grid(row=7, column=0, sticky='sw')

        # Labels
        self.uccode = tk.Label(self, text='UC Code:', foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=1, column=1, padx=10,
                                            sticky='w')
        self.name = tk.Label(self, text='Site Name:', foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=2, column=1, padx=10,
                                            sticky='w')
        self.box = tk.Label(self, text='Box Cycle:', foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=3, column=1, padx=10,
                                            sticky='w')
        self.week = tk.Label(self, text='Current Week:', foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=4, column=1, padx=10,
                                            sticky='w')
        self.change = tk.Label(self, text='Sample Change Day:', foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=5, column=1, padx=10,
                                            sticky='w')
        self.sample = tk.Label(self, text='Next Sample Day:', foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=6, column=1, padx=10,
                                            sticky='w')

        # Values
        self.uccodev = tk.Label(self, text=ucCode, foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=1, column=2, padx=10,
                                            sticky='w')
        self.namev = tk.Label(self, text=siteName, foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=2, column=2, padx=10,
                                            sticky='w')
        self.boxv = tk.Label(self, text=boxCycle, foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=3, column=2, padx=10,
                                            sticky='w')
        self.weekv = tk.Label(self, text=currentWeek, foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=4, column=2, padx=10,
                                            sticky='w')
        self.changev = tk.Label(self, text=changeDay, foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=5, column=2, padx=10,
                                            sticky='w')
        self.samplev = tk.Label(self, text=nextSample, foreground=wFG, background=wBG,
                                        font=(iFont, 10)).grid(row=6, column=2, padx=10,
                                            sticky='w')

        self.help = tk.Label(self, text='For Help, Please Call 530-752-1123', foreground=wFG,
                             background=wBG, font=(iFont, 10)).grid(row=7, column=1, padx=10,
                                            columnspan=2, ipadx=35, pady=3)

########################################################################################################
#####################           User Settings                                   ########################
########################################################################################################
        

class settings(tk.Frame):
    """     This menu directs the user to settings like time, OPRTR Initials, and display settings  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Settings', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=80, pady=10,
                                        columnspan=2, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(mainMenu)).grid(row=3,
                                    column=0, sticky='sw')

        self.timeNdate = tk.Button(self, text='Date and Time', width=30, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8,
                                   command=lambda: controller.show_frame(editTime)).grid(row=1,
                                column=1, padx=30, pady=7)
        self.opInits = tk.Button(self, text='Operator Initials', width=30, height=1,
                                     borderwidth=8, background=bBG, font=(iFont, 10),
                                 command=lambda: controller.show_frame(editOps)).grid(
                                         row=2, column=1, padx=30, pady=7)
        self.display = tk.Button(self, text='Display Settings', width=30, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10),
                                 command=lambda: controller.show_frame(editDisplay)
                                 ).grid(row=3, column=1, pady=6, padx=30)

class editTime(tk.Frame):
    """     This screen allows the user to add and remove operators from the list.              """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # Temporary placeholders
        newTime = time.strftime("%H:%M:%S")
        newDate = time.strftime("%m-%d-%Y")

        self.screenLabel = tk.Label(self, text='Edit Date and Time', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1,padx=40, pady=10,
                                                           columnspan=3, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(
                                    controller.timeUpdate(newTime, newDate),
                                    controller.show_frame(home))
                              ).grid(row=0, column=0, sticky='nw')
        self.done = tk.Button(self, text='Done', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(
                                    controller.timeUpdate(newTime, newDate),
                                    controller.show_frame(settings))
                              ).grid(row=4, column=0, sticky='sw')

        self.clockLabel = tk.Label(self, text='Current Time:', foreground=wFG, background=wBG,
                                   font=(iFont, 14)).grid(row=2, column=1, pady=8,
                                                          sticky='w')
        self.clock = Clock(self)
        self.clock.widget.configure(foreground=wFG, background=wBG, font=(iFont, 14),
                                    anchor="center", justify="center")
        self.clock.widget.grid(row=2, column=2, columnspan=2, pady=8, sticky='w')

        
        self.dateLabel = tk.Label(self, text='Current Date:', foreground=wFG, background=wBG,
                                   font=(iFont, 14)).grid(row=3, column=1, pady=7,
                                                          sticky='w')
        self.date = Day(self)
        self.date.widget.configure(foreground=wFG, background=wBG, font=(iFont, 14),
                                  anchor="center", justify="center")
        self.date.widget.grid(row=3, column=2, columnspan=2, pady=7, sticky='w')

        dtList = [ 'Hours', 'Minutes', 'Day', 'Month', 'Year' ]

        self.selector = tk.StringVar()
        self.selector.set(dtList[0])
        self.selector.widget = tk.OptionMenu(self, self.selector, *dtList)
        self.selector.widget.configure(width=7, background=bBG, activebackground=bBG, bd=0,
                                       font=(iFont, 12))
        self.selector.widget["menu"].config(background=bBG, font=(iFont, 12))
        self.selector.widget.grid(row=1, column=1, padx=5, pady=5)

        self.up = tk.Button(self, text='^', width=5, height=1,background=bBG, font=(iFont, 16),
                            command=lambda: self.upValue()).grid(row=1, column=2, sticky='ew')
        self.down = tk.Button(self, text='v', width=5, height=1,background=bBG, font=(iFont, 16),
                            command=lambda: self.downValue()).grid(row=1, column=3, sticky='ew')

        
    # Methods to change time and date values of system clock. Remove "pass" when we put in the code.
    def upValue(self, *args):
        selct = self.selector.widget['text']
        if selct == 'Hours':
            # +1 to hour
            pass
        elif selct == 'Minutes':
            # +1 to min
            pass
        elif selct == 'Day':
            # +1 to day
            pass
        elif selct == 'Month':
            # +1 to month
            pass
        else:
            # +1 to year
            pass
        # Combine date and time values, assign to newTime and newDate
        newTime = time.strftime("%H:%M:%S")
        newDate = time.strftime("%m-%d-%Y")
        return newTime, newDate

    def downValue(self, *args):
        selct = self.selector.widget['text']
        if selct == 'Hours':
            # -1 to hour
            pass
        elif selct == 'Minutes':
            # -1 to min
            pass
        elif selct == 'Day':
            # -1 to day
            pass
        elif selct == 'Month':
            # -1 to month
            pass
        else:
            # -1 to year
            pass
        # Combine date and time values, assign to newTime and newDate
        newTime = time.strftime("%H:%M:%S")
        newDate = time.strftime("%m-%d-%Y")
        return newTime, newDate



class editOps(tk.Frame):
    """     This screen allows the user to add and remove operators from the list.              """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Edit Operator Initials', foreground=wFG,
                                    background=wBG, font=(iFont, 12)).grid(
                                        row=0, column=1,padx=80, pady=15, columnspan=4, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(
                                    controller.oper.opsUpdate(),
                                    controller.show_frame(home))
                              ).grid(row=0,
                                    column=0, sticky='nw')
        self.done = tk.Button(self, text='Done', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(
                                    controller.oper.opsUpdate(),
                                    controller.show_frame(settings))
                              ).grid(row=3, column=0, sticky='sw')

        # Add scrollbar for listbox
        self.listScroll = tk.Scrollbar(self, orient=tk.VERTICAL, bd=10)
        self.listScroll.grid(row=1, column=3, sticky='nsw')

##        self.operators = tk.Listbox(self)
        self.operators = currentOpsLB(self)
        self.operators.widget2.config(background = wBG, foreground=wFG, font=(iFont, 14), height=3, width=5,
                              selectforeground='black', selectmode=tk.MULTIPLE,
                              yscrollcommand=self.listScroll.set)
        self.operators.widget2.grid(row=1, column=2, sticky='nsew')
        self.listScroll['command'] = self.operators.widget2.yview

##        # Add initial values to listbox
##        for oper in oprtrs:
##            self.operators.insert(tk.END, oper)

        self.remove = tk.Button(self, text='Remove', width=7, background=bBG, bd=10,
                                font=(iFont, 12), command=lambda: self.delOps()
                                ).grid(row=1, column=4, sticky='nse')

        self.add = tk.Button(self, text='Add', width=7, background=bBG, bd=10,
                                font=(iFont, 12), command=lambda: self.addOps()
                                ).grid(row=2, column=4, rowspan=2, sticky='nse')

        self.spin1 = tk.Spinbox(self, values=letts, textvariable=controller.oper1, wrap=True,
                                width=2, background=wBG, foreground=wFG, font=(iFont, 18))
        self.spin1.grid(row=2, column=1, rowspan=2, pady=20)
        self.spin2 = tk.Spinbox(self, values=letts, textvariable=controller.oper2, wrap=True,
                                width=2, background=wBG, foreground=wFG, font=(iFont, 18))
        self.spin2.grid(row=2, column=2, rowspan=2, pady=20)
        self.spin3 = tk.Spinbox(self, values=letts, textvariable=controller.oper3, wrap=True,
                                width=2, background=wBG, foreground=wFG, font=(iFont, 18))
        self.spin3.grid(row=2, column=3, rowspan=2, pady=20)

    def delOps(self):
        global oprtrs
        currentList = self.operators.widget2.get(0, tk.END)              # Strings in list
        items = self.operators.widget2.curselection()                    # Indexes of selected
        pos = 0                                                         # Iterator
        for idx, val in enumerate(currentList):
            if str(idx) in items:
                self.operators.widget2.delete(pos)
                continue                                                # Bypass iteration of pos
            pos += 1
        try:
            oprtrs = list(self.operators.widget2.get(0, tk.END))
        except Exception:
            oprtrs = ('')
        return oprtrs

    def addOps(self):
        global oprtrs
        newOp = self.spin1.get() + self.spin2.get() + self.spin3.get()
        self.operators.widget2.insert(tk.END, newOp)
        oprtrs = list(self.operators.widget2.get(0, tk.END))
        return oprtrs

    def menuCall(self, *args):
        # Do nothing, just a placeholder. Unless we have something to do here.
        return
        

class editDisplay(tk.Frame):
    """     This menu allows the user to change the display characteristics.                    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Change Display Settings \n', foreground=wFG,
                                    background=wBG, font=(iFont, 12)).grid(
                                        row=0, column=1, padx=40, pady=10, columnspan=2, sticky='ew')

        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(
                                    # update display,
                                    controller.show_frame(home))
                              ).grid(row=0, column=0, sticky='nw')
        self.done = tk.Button(self, text='Done', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(
                                    # update display,
                                    controller.show_frame(settings))
                              ).grid(row=3, column=0, sticky='sw')

        self.nModeButton = tk.Button(self, text='Night Mode', background='black', foreground='white',
                                     font=(iFont, 12),
                                     command=lambda: self.changeD(controller))
        self.nModeButton.grid(row=1, column=1, padx=40, pady=40, sticky='nsew')

    def changeD(self, controller):
        global wFG, wBG, bBG
        if controller.title() == app.title():
            wFG = 'black'                               # Label text color
            wBG = '#34FA3B'                             # Background, bright green
            bBG = '#FA34F3'                             # Button color, fuschia?
            # And/Or per Brian's suggestion, we can change the brightness on the display
            temp = Application()
            temp.title('Night Mode Controller GUI')
            temp.geometry('384x197+600+600')
            temp.update_idletasks()
            return wFG, wBG, bBG
        else:
            wFG = '#C99700'                             # UCD Gold - window 
            wBG = '#002855'                             # UCD Blue - window
            bBG = '#C99700'                             # UCD Gold - button
            controller.destroy()
            return wFG, wBG, bBG

# When this comes back from night mode, whatever operators had been adjusted does not carry over.
# Also, can't see first oprtr in list in night mode. Can we transfer the OPRTR list from the temp
# instance over to the app instance?

########################################################################################################
#####################           Equipment Change                                ########################
########################################################################################################

class whatEquip(tk.Frame):
    """     This is where the user selects the equipment that is being changed.  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Select Equipment', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=2, padx=47, pady=10,
                                        columnspan=2, sticky='ew')

        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)
##        self.next = tk.Button(self, image=nextIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(opInits)).grid(row=3,
                                    column=0, sticky='sw')
        self.next = tk.Button(self, text='Next', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.post_whatEquip(
                                    self.selctEq.get(self.selctEq.curselection()))
                              ).grid(row=3, column=4, sticky='se')

        self.grid_columnconfigure(1, minsize=50)                # Add some spacing, needs tweaking
        # Add scrollbar for listbox
        self.listScroll2 = tk.Scrollbar(self, orient=tk.VERTICAL, bd=10)
        self.listScroll2.grid(row=1, column=3, pady=16, sticky='nsw')

        self.selctEq = tk.Listbox(self)
        self.selctEq.config(background = wBG, foreground=wFG, font=(iFont, 14), height=4, width=5,
                              selectforeground='black', selectmode=tk.SINGLE,
                              yscrollcommand=self.listScroll2.set)
        self.selctEq.grid(row=1, column=2, pady=16, sticky='nsew')
        self.listScroll2['command'] = self.selctEq.yview
        # Add initial values to listbox
        for part in equipment:
            self.selctEq.insert(tk.END, part)


class whatMod(tk.Frame):
    """     This is where the user selects the module that the equipment is going in.  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Select Module', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=2, padx=65, pady=13,
                                        columnspan=2, sticky='e')

        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)
##        self.next = tk.Button(self, image=nextIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(whatEquip)).grid(row=3,
                                    column=0, sticky='sw')
        self.next = tk.Button(self, text='Next', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.preModCheck()).grid(row=3,
                                    column=4, sticky='se')

        self.grid_columnconfigure(1, minsize=40)            # Add some spacing, needs tweaking
        # Add scrollbar for listbox - this way, if we decide to allow the user to select multiple
        # modules at once, it will be a lot easier.
        self.listScroll3 = tk.Scrollbar(self, orient=tk.VERTICAL, bd=10, activebackground=bBG, bg=bBG)
        self.listScroll3.grid(row=1, column=3, pady=13, sticky='nsw')

        self.selctMod = tk.Listbox(self)
        self.selctMod.config(background = wBG, foreground=wFG, font=(iFont, 14), height=4, width=5,
                              selectforeground='black', selectmode=tk.SINGLE,
                              yscrollcommand=self.listScroll3.set)
        self.selctMod.grid(row=1, column=2, pady=13, sticky='nsew')
        self.listScroll3['command'] = self.selctMod.yview
        # Add initial values to listbox
        for mod in mods:
            self.selctMod.insert(tk.END, mod)


class eqInstruct(tk.Frame):
    """     This is where the user is told how to replace the equipment.  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        instr = "Instructions for changing equipment \n using multiple lines \
of code \n \n \n \n"

        self.screenLabel = tk.Label(self, text='Instructions', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, padx=70, pady=10,
                                        columnspan=4, sticky='ew')

        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(whatEquip)).grid(row=3,
                                    column=0, sticky='sw')


        self.instruct = tk.Label(self, text=instr, foreground=wFG, background=wBG, font=(iFont, 12)
                                 ).grid(row=1, columnspan=3, padx=30)
        self.keepGoin = tk.Button(self, text='Continue', width=30,
                                  height=1, background=bBG, font=(iFont, 10), borderwidth=8,
                                  command=lambda: controller.show_frame(moreEq)).grid(row=3,
                                column=2, columnspan=2, sticky='se')

class moreEq(tk.Frame):
    """ The user specifies if there is more equipment to swap.      """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.question = tk.Label(self, text='Do you have more equipment to replace?',
                                 foreground=wFG, background=wBG, font=(iFont, 12)).grid(
                                     row=1, padx=30, pady=10, columnspan=2, sticky='ew')

        self.yes = tk.Button(self, text='YES', width=10, height=2, background=bBG, font=(iFont, 14),
                             borderwidth=8, command=lambda: controller.show_frame(whatEquip)
                             ).grid(row=2, column=0, pady=30)

        self.no = tk.Button(self, text='NO', width=10, height=2, background=bBG, font=(iFont, 14),
                            borderwidth=8, command=lambda: controller.show_frame(modTest)
                            ).grid(row=2, column=1, pady=30)

        


########################################################################################################
#####################           Quick Checks                                    ########################
########################################################################################################

class quick(tk.Frame):
    """     This menu allows the user to perform easy troubleshooting tests, like the old hotkeys  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Quick Checks (1/2)', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=80, pady=10,
                                        columnspan=2, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.more = tk.Button(self, text='More', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(quick2)).grid(row=2,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(mainMenu)).grid(row=3,
                                    column=0, sticky='sw')

        self.vac = tk.Button(self, text='Vacuum', width=14, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8).grid(row=1,
                                column=1, padx=10, pady=7, sticky='ew')
        self.solenoids = tk.Button(self, text='Solenoids', width=14, height=1,
                                     borderwidth=8, background=bBG, font=(iFont, 10)).grid(
                                         row=1, column=2, padx=10, pady=7, sticky='ew')
        self.leak = tk.Button(self, text='Leak Check', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=2, column=1, pady=7,
                                    padx=10, sticky='ew')
        self.exp = tk.Button(self, text='Expected Values', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=2, column=2, pady=7,
                                    padx=10, sticky='ew')
        self.zero = tk.Button(self, text='Zero Flows', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=3, column=1, pady=6,
                                    padx=10, sticky='ew')
        self.temp = tk.Button(self, text='Temperature', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=3, column=2, pady=6,
                                    padx=10, sticky='ew') 

class quick2(tk.Frame):
    """     This menu allows the user to perform easy troubleshooting tests, like the old hotkeys  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Quick Checks (2/2)', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=80, pady=10,
                                        columnspan=2, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(quick)).grid(row=3,
                                    column=0, sticky='sw')

        self.pumps = tk.Button(self, text='Pumps', width=14, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8).grid(row=1,
                                column=1, padx=10, pady=7, sticky='ew')
        self.filters = tk.Button(self, text='Filter Readings', width=14, height=1,
                                     borderwidth=8, background=bBG, font=(iFont, 10)).grid(
                                         row=1, column=2, padx=10, pady=7, sticky='ew')
        self.motors = tk.Button(self, text='Motors', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=2, column=1, pady=7,
                                    padx=10, sticky='ew')
        self.inv = tk.Button(self, text='Inventory', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=2, column=2, pady=7,
                                    padx=10, sticky='ew')
        self.data = tk.Button(self, text='Data Dump', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=3, column=1, pady=6,
                                    padx=10, sticky='ew')
        self.other = tk.Button(self, text='Other', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=3, column=2, pady=6,
                                    padx=10, sticky='ew') 

########################################################################################################
#####################           Advanced Settings                               ########################
########################################################################################################

class advanced(tk.Frame):
    """     This menu allows the user to perform more advanced tasks, usually reserved for techs  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Advanced', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=80, pady=10,
                                        columnspan=2, sticky='ew')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(mainMenu)).grid(row=3,
                                    column=0, sticky='sw')


        self.config = tk.Button(self, text='Site Config', width=14, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8).grid(row=1,
                                column=1, padx=10, pady=7, sticky='ew')
        self.schedule = tk.Button(self, text='Alt. Schedule', width=14, height=1,
                                     borderwidth=8, background=bBG, font=(iFont, 10)).grid(
                                         row=1, column=2, padx=10, pady=7, sticky='ew')
        self.inv = tk.Button(self, text='Inventory', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=2, column=1, pady=7,
                                    padx=10, sticky='ew')
        self.calib = tk.Button(self, text='Calibration', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=2, column=2, pady=7,
                                    padx=10, sticky='ew')
        self.ets = tk.Button(self, text='ET Reset', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=3, column=1, pady=6,
                                    padx=10, sticky='ew')
        self.filters = tk.Button(self, text='Adv. Filter Change', width=14, height=1, borderwidth=8,
                              background=bBG, font=(iFont, 10)).grid(row=3, column=2, pady=6,
                                    padx=10, sticky='ew')


########################################################################################################
#####################           Filter Reading Frames                           ########################
########################################################################################################

        
class finalReadings(tk.Frame):
    """     Display readings for OPRTR to copy to logsheet      """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # x will determine whether this screen is showing initial or final readings. It needs to be
        # dynamically connected to whatever filters are loaded in the samplers. We also want to make
        # sure we can see both initial and final readings in the "Quick Checks" menu.
        x = 1
        if x == 1: pList = ( 'ori', 'cyc', 'et' )                    # Finals parameters
        else: pList = ( 'mxori', 'ori', 'cyc' )                     # Initials parameters

        self.mod = currentMod(self)
        self.mod.widget.configure(foreground=wBG, background=bBG, font=(iFont, 15), relief='raised',
                                  width=2)
        self.mod.widget.grid(row=0, column=1, rowspan=2, sticky = 'nsew')
        
        self.screenLabel = tk.Label(self, text='%s Readings' % prepo[x], foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=3, columnspan=2, padx=50)
        self.filterWeek = tk.Label(self, text='Filter Week %s' % currentWeek, foreground=wFG,
                                   background=wBG, font=(iFont, 12)).grid(row=1, column=3,
                                                                          columnspan=2)
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)


        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=5, sticky='ne')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(modTest)).grid(row=1,
                                    column=5, sticky='e')
        self.next = tk.Button(self, text='Continue with Sample Change', width=5, height=1,
                              background=bBG, font=(iFont,10),
                              command=lambda: controller.postFinals()).grid(row=6,
                                    columnspan=6, pady=10, sticky='sew')


        self.backMod = tk.Button(self, text='<', width=1, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(self.mod.modChange(-1),
                                    self.value1_1.modChange(-1, 1, prepo[x], pList[0]),
                                    self.value1_2.modChange(-1, 1, prepo[x], pList[1]),
                                    self.value1_3.modChange(-1, 1, prepo[x], pList[2]),
                                    self.value2_1.modChange(-1, 2, prepo[x], pList[0]),
                                    self.value2_2.modChange(-1, 2, prepo[x], pList[1]),
                                    self.value2_3.modChange(-1, 2, prepo[x], pList[2]),
                                    self.value3_1.modChange(-1, 3, prepo[x], pList[0]),
                                    self.value3_2.modChange(-1, 3, prepo[x], pList[1]),
                                    self.value3_3.modChange(-1, 3, prepo[x], pList[2]))
                                 ).grid(row=0, column=0, rowspan=2, sticky='nsew')
        self.forwardMod = tk.Button(self, text='>', width=1, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(self.mod.modChange(1),
                                    self.value1_1.modChange(1, 1, prepo[x], pList[0]),
                                    self.value1_2.modChange(1, 1, prepo[x], pList[1]),
                                    self.value1_3.modChange(1, 1, prepo[x], pList[2]),
                                    self.value2_1.modChange(1, 2, prepo[x], pList[0]),
                                    self.value2_2.modChange(1, 2, prepo[x], pList[1]),
                                    self.value2_3.modChange(1, 2, prepo[x], pList[2]),
                                    self.value3_1.modChange(1, 3, prepo[x], pList[0]),
                                    self.value3_2.modChange(1, 3, prepo[x], pList[1]),
                                    self.value3_3.modChange(1, 3, prepo[x], pList[2]))
                                ).grid(row=0, column=2, rowspan=2, sticky='nsew')

        self.header1 = tk.Label(self, text='  Sample Date  ', height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG,  relief='groove').grid(
                                    row=2, column=0, columnspan=3, sticky='ew')
        # The rest of the headers should be interchangable with initial readings so we can just have
        # one screen for all filter readings
        self.header2 = tk.Label(self, text=pList[0].upper(), width=5, height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG, relief='groove').grid(row=2,
                                    column=3, sticky='ew')
        self.header3 = tk.Label(self, text=pList[1].upper(), width=5, height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG, relief='groove').grid(row=2,
                                    column=4, sticky='ew')
        self.header4 = tk.Label(self, text=pList[2].upper(), width=5, height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG, relief='groove').grid(row=2,
                                    column=5, sticky='ew')

        # Dates need to be linked to real dates of sampling
        self.date1 = tk.Label(self, text='02/02/2015', height=1, background=wBG, bd=5, font=(iFont,
                                10), foreground=wFG,  relief='groove').grid(row=3, column=0,
                                    columnspan=3, sticky='ew')
        self.date2 = tk.Label(self, text='02/05/2015', height=1, background=wBG, bd=5, font=(iFont,
                                10), foreground=wFG,  relief='groove').grid(row=4, column=0,
                                    columnspan=3, sticky='ew')
        self.date3 = tk.Label(self, text='02/08/2015', height=1, background=wBG, bd=5, font=(iFont,
                                10), foreground=wFG,  relief='groove').grid(row=5, column=0,
                                    columnspan=3, sticky='ew')

        # Likewise, each reading needs to be linked to the actual measurements. We're thinking that
        # the final readings will be the real readings taking at the very end of the sampling phase.
        # This way, the measurements will be linked directly to the sample instead of whatever may
        # have happened to the equipment between the end of sampling and the operator's visit.

        # Might need to make this more succinct
        self.value1_1 = value(self, self.mod.widget['text'], 1, prepo[x], pList[0])
        self.value1_1.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value1_1.widget.grid(row=3, column=3, sticky = 'ew')

        self.value1_2 = value(self, self.mod.widget['text'], 1, prepo[x], pList[1])
        self.value1_2.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value1_2.widget.grid(row=3, column=4, sticky = 'ew')

        self.value1_3 = value(self, self.mod.widget['text'], 1, prepo[x], pList[2])
        self.value1_3.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value1_3.widget.grid(row=3, column=5, sticky = 'ew')

        self.value2_1 = value(self, self.mod.widget['text'], 2, prepo[x], pList[0])
        self.value2_1.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value2_1.widget.grid(row=4, column=3, sticky = 'ew')

        self.value2_2 = value(self, self.mod.widget['text'], 2, prepo[x], pList[1])
        self.value2_2.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value2_2.widget.grid(row=4, column=4, sticky = 'ew')

        self.value2_3 = value(self, self.mod.widget['text'], 2, prepo[x], pList[2])
        self.value2_3.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value2_3.widget.grid(row=4, column=5, sticky = 'ew')

        self.value3_1 = value(self, self.mod.widget['text'], 2, prepo[x], pList[0])
        self.value3_1.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value3_1.widget.grid(row=5, column=3, sticky = 'ew')

        self.value3_2 = value(self, self.mod.widget['text'], 2, prepo[x], pList[1])
        self.value3_2.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value3_2.widget.grid(row=5, column=4, sticky = 'ew')

        self.value3_3 = value(self, self.mod.widget['text'], 2, prepo[x], pList[2])
        self.value3_3.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value3_3.widget.grid(row=5, column=5, sticky = 'ew')

class changeNormal(tk.Frame):
    """     Instruct OPRTR to change filters normally       """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        instr = "Instructions for normal filter change using \n multiple lines \
of code"

        self.screenLabel = tk.Label(self, text='\n', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=88, pady=10,
                                        sticky='w')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)        

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(opInits)).grid(row=3,
                                    column=0, sticky='sw')

        self.instruct = tk.Label(self, text=instr, foreground=wFG, background=wBG, font=(iFont, 12)
                                 ).grid(row=1, columnspan=2, padx=30)
        self.keepGoin = tk.Button(self, text='Continue with Initial Filter Readings', width=30,
                                  height=1, background=bBG, font=(iFont, 10), borderwidth=8,
                                  command=lambda: controller.combine_funcs(
                                      controller.show_frame(checkInstall),
                                      controller.preInits())).grid(row=2,
                                columnspan=2, padx=5, pady=15)

class changeSpecial(tk.Frame):
    """     Instruct OPRTR to change filters with 3rd position movement       """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        specInstr = "Instructions for special filter change using multiple lines \
of code"

        self.screenLabel = tk.Label(self, text='\n', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=88, pady=10,
                                        sticky='w')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)        

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(finalReadings)).grid(row=3,
                                    column=0, sticky='sw')

        self.instruct = tk.Label(self, text=specInstr, foreground=wFG, background=wBG,
                                 font=(iFont,12)).grid(row=1, columnspan=2, padx=30)
        self.keepGoin = tk.Button(self, text='Continue with Initial Filter Readings', width=30,
                                  height=1, background=bBG, font=(iFont, 10), borderwidth=8,
                                  command=lambda: controller.combine_funcs(
                                      controller.show_frame(checkInstall),
                                      controller.preInits())).grid(
                                          row=2, columnspan=2, padx=5, pady=15)

class checkInstall(tk.Frame):
    """ This frame informs the user that the system is turning on pumps and testing for proper
        installation of samples. In doing so, it should take readings. """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.msg = tk.Label(self, text='Testing for Proper installation \n of filters . . .',
                            foreground=wFG, background=wBG, font=(iFont, 14), justify="center").grid(
                                padx=40, pady=30)
    


class initialReadings(tk.Frame):
    """     Display readings for OPRTR to copy to logsheet      """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # x will determine whether this screen is showing initial or final readings. It needs to be
        # dynamically connected to whatever filters are loaded in the samplers. We also want to make
        # sure we can see both initial and final readings in the "Quick Checks" menu.
        x = 0
        if x == 1: pList = ( 'ori', 'cyc', 'et' )                    # Finals parameters
        else: pList = ( 'mxori', 'ori', 'cyc' )                     # Initials parameters

        self.mod = currentMod(self)
        self.mod.widget.configure(foreground=wBG, background=bBG, font=(iFont, 15), relief='raised',
                                  width=2)
        self.mod.widget.grid(row=0, column=1, rowspan=2, sticky = 'nsew')
        
        self.screenLabel = tk.Label(self, text='%s Readings' % prepo[x], foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=3, columnspan=2, padx=50)
        self.filterWeek = tk.Label(self, text='Filter Week %s' % currentWeek, foreground=wFG,
                                   background=wBG, font=(iFont, 12)).grid(row=1, column=3,
                                                                          columnspan=2)
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)


        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=5, sticky='ne')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.special()).grid(row=1,
                                    column=5, sticky='e')
        self.next = tk.Button(self, text='Finished With Sample Change', width=5, height=1,
                              background=bBG, font=(iFont,10),
                              command=lambda: controller.combine_funcs(controller.toggleInitials(),
                                  controller.show_frame(home))).grid(row=6, columnspan=6, pady=10,
                                                                    sticky='sew')


        self.backMod = tk.Button(self, text='<', width=1, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(self.mod.modChange(-1),
                                    self.value1_1.modChange(-1, 1, prepo[x], pList[0]),
                                    self.value1_2.modChange(-1, 1, prepo[x], pList[1]),
                                    self.value1_3.modChange(-1, 1, prepo[x], pList[2]),
                                    self.value2_1.modChange(-1, 2, prepo[x], pList[0]),
                                    self.value2_2.modChange(-1, 2, prepo[x], pList[1]),
                                    self.value2_3.modChange(-1, 2, prepo[x], pList[2]),
                                    self.value3_1.modChange(-1, 3, prepo[x], pList[0]),
                                    self.value3_2.modChange(-1, 3, prepo[x], pList[1]),
                                    self.value3_3.modChange(-1, 3, prepo[x], pList[2]))
                                 ).grid(row=0, column=0, rowspan=2, sticky='nsew')
        self.forwardMod = tk.Button(self, text='>', width=1, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.combine_funcs(self.mod.modChange(1),
                                    self.value1_1.modChange(1, 1, prepo[x], pList[0]),
                                    self.value1_2.modChange(1, 1, prepo[x], pList[1]),
                                    self.value1_3.modChange(1, 1, prepo[x], pList[2]),
                                    self.value2_1.modChange(1, 2, prepo[x], pList[0]),
                                    self.value2_2.modChange(1, 2, prepo[x], pList[1]),
                                    self.value2_3.modChange(1, 2, prepo[x], pList[2]),
                                    self.value3_1.modChange(1, 3, prepo[x], pList[0]),
                                    self.value3_2.modChange(1, 3, prepo[x], pList[1]),
                                    self.value3_3.modChange(1, 3, prepo[x], pList[2]))
                                ).grid(row=0, column=2, rowspan=2, sticky='nsew')

        self.header1 = tk.Label(self, text='  Sample Date  ', height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG,  relief='groove').grid(
                                    row=2, column=0, columnspan=3, sticky='ew')
        # The rest of the headers are interchangable with initial readings so theoretically,
        # we can just have one screen for all filter readings
        self.header2 = tk.Label(self, text=pList[0].upper(), width=5, height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG, relief='groove').grid(row=2,
                                    column=3, sticky='ew')
        self.header3 = tk.Label(self, text=pList[1].upper(), width=5, height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG, relief='groove').grid(row=2,
                                    column=4, sticky='ew')
        self.header4 = tk.Label(self, text=pList[2].upper(), width=5, height=1, background=wBG, bd=5,
                                font=(iFont, 10), foreground=wFG, relief='groove').grid(row=2,
                                    column=5, sticky='ew')

        # Dates need to be linked to real dates of sampling
        self.date1 = tk.Label(self, text='02/02/2015', height=1, background=wBG, bd=5, font=(iFont,
                                10), foreground=wFG,  relief='groove').grid(row=3, column=0,
                                    columnspan=3, sticky='ew')
        self.date2 = tk.Label(self, text='02/05/2015', height=1, background=wBG, bd=5, font=(iFont,
                                10), foreground=wFG,  relief='groove').grid(row=4, column=0,
                                    columnspan=3, sticky='ew')
        self.date3 = tk.Label(self, text='02/08/2015', height=1, background=wBG, bd=5, font=(iFont,
                                10), foreground=wFG,  relief='groove').grid(row=5, column=0,
                                    columnspan=3, sticky='ew')

        # Likewise, each reading needs to be linked to the actual measurements. We're thinking that
        # the final readings will be the real readings taking at the very end of the sampling phase.
        # This way, the measurements will be linked directly to the sample instead of whatever may
        # have happened to the equipment between the end of sampling and the operator's visit.

        # Can we make this more succinct?
        self.value1_1 = value(self, self.mod.widget['text'], 1, prepo[x], pList[0])
        self.value1_1.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value1_1.widget.grid(row=3, column=3, sticky = 'ew')

        self.value1_2 = value(self, self.mod.widget['text'], 1, prepo[x], pList[1])
        self.value1_2.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value1_2.widget.grid(row=3, column=4, sticky = 'ew')

        self.value1_3 = value(self, self.mod.widget['text'], 1, prepo[x], pList[2])
        self.value1_3.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value1_3.widget.grid(row=3, column=5, sticky = 'ew')

        self.value2_1 = value(self, self.mod.widget['text'], 2, prepo[x], pList[0])
        self.value2_1.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value2_1.widget.grid(row=4, column=3, sticky = 'ew')

        self.value2_2 = value(self, self.mod.widget['text'], 2, prepo[x], pList[1])
        self.value2_2.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value2_2.widget.grid(row=4, column=4, sticky = 'ew')

        self.value2_3 = value(self, self.mod.widget['text'], 2, prepo[x], pList[2])
        self.value2_3.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value2_3.widget.grid(row=4, column=5, sticky = 'ew')

        self.value3_1 = value(self, self.mod.widget['text'], 2, prepo[x], pList[0])
        self.value3_1.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value3_1.widget.grid(row=5, column=3, sticky = 'ew')

        self.value3_2 = value(self, self.mod.widget['text'], 2, prepo[x], pList[1])
        self.value3_2.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value3_2.widget.grid(row=5, column=4, sticky = 'ew')

        self.value3_3 = value(self, self.mod.widget['text'], 2, prepo[x], pList[2])
        self.value3_3.widget.configure(width=5, height=1, foreground=wFG, background=wBG, font=(iFont,
                                        10), relief='groove', bd=5)
        self.value3_3.widget.grid(row=5, column=5, sticky = 'ew')


########################################################################################################
#####################           Reusable Frames                                 ########################
########################################################################################################


class opInits(tk.Frame):
    """     This screen asks the user to input their initials during filter and hardware changes.  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Select Operator Initials', foreground=wFG,
                                    background=wBG, font=(iFont, 12)).grid(row=0, column=1,
                                        columnspan=4, padx=50, pady=10, sticky='w')
        
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)
##        self.next = tk.Button(self, image=nextIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.backOps()).grid(row=3,
                                    column=0, sticky='sw')


        self.next = tk.Button(self, text='Next', width=5, height=1,background=bBG, font=(iFont, 10),
                              command=lambda: controller.combine_funcs(
                                  self.opMenu.opsChange(self.opMenu.widget['text'],
                                  self.spin1.get()+self.spin2.get()+self.spin3.get()),
                                  controller.postOps())
                              ).grid(row=3,column=5, sticky='se')

        # For menu option, "Other" is the last option. When selected, the scrollboxes may be
        # enabled. Disable option menu? Might leave the OPRTR stranded if they accidentally clicked
        # other. Not enacted yet. If there is time at the end, we can implement this change.

        self.opMenu = currentOps(self)
        
        self.opMenu.widget.configure(width=5, background='red', foreground='black',
                                     activebackground=bBG, activeforeground='black',
                                     bd=0, font=(iFont, 18))
        self.opMenu.widget["menu"].config(background=bBG, font=(iFont, 18))
        self.opMenu.widget.grid(row=1, column=1, columnspan=4, padx=5, pady=12)


        self.new = tk.Label(self, text='Other:', foreground=wFG, background=wBG,
                            font=(iFont, 18)).grid(row=2, column=1, pady=15, sticky='w')
        
        self.spin1 = tk.Spinbox(self, values=letts, textvariable=controller.oper1, wrap=True, width=2,
                                background=wBG, foreground=wFG, font=(iFont, 18))
        self.spin1.grid(row=2, column=2, pady=15)
        self.spin2 = tk.Spinbox(self, values=letts, textvariable=controller.oper2, wrap=True, width=2,
                                background=wBG, foreground=wFG, font=(iFont, 18))
        self.spin2.grid(row=2, column=3, pady=15)
        self.spin3 = tk.Spinbox(self, values=letts, textvariable=controller.oper3, wrap=True, width=2,
                                background=wBG, foreground=wFG, font=(iFont, 18))
        self.spin3.grid(row=2, column=4, pady=15)

        self.opMenu.opsUpdate() # do timer update?

        # Trace when operator initials scrollboxes are changed
        controller.oper1.trace('w', self.altCall)
        controller.oper2.trace('w', self.altCall)
        controller.oper3.trace('w', self.altCall)

    # Instead of changing colors, disable the other field unless selected by the optionmenu
    def menuCall(self, *args):
        # Do local effects
        global opIndex
        opIndex = 0
        self.opMenu.widget.configure(background='red')
        self.spin1.config(background=wBG, foreground=wFG)
        self.spin2.config(background=wBG, foreground=wFG)
        self.spin3.config(background=wBG, foreground=wFG)
        return opIndex

    def altCall(self, *args):
        global opIndex
        opIndex = 1
        self.opMenu.widget.configure(background=bBG)
        self.spin1.config(background='red', foreground='black')
        self.spin2.config(background='red', foreground='black')
        self.spin3.config(background='red', foreground='black')
        return opIndex

class modTest(tk.Frame):
    """     This screen informs the user that the equipment is being tested, we can also add in
            the calls to the equipment to turn the pumps on, open and close solenoids, etc.     """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='Testing Equipment', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, columnspan=2, padx=116, pady=10,
                                        sticky='w')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)
##        self.next = tk.Button(self, image=nextIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.backModCheck()).grid(row=3,
                                    column=0, sticky='sw')

        self.next = tk.Button(self, text='No', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.postModCheck()).grid(row=3,
                                    column=1, sticky='se')

        self.question = tk.Label(self, text='Do any of the pumps sound abnormal?', foreground=wFG,
                                    background=wBG, font=(iFont, 12)).grid(row=1, columnspan=2, pady=15,
                                                                           sticky='ew')
        self.badPump = tk.Button(self, text='Yes', width=15, height=1, borderwidth=8, background=bBG,
                                 font=(iFont, 12), command=lambda: controller.show_frame(badPump)
                                 ).grid(row=2, columnspan=2, padx=5, pady=15)

class badPump(tk.Frame):
    """     In the case of a noisy pump, the hardware may not register the issue, so this screen
            instructs the user to call and report the problem.                                  """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='\n', foreground=wFG, background=wBG,
                                    font=(iFont, 12)).grid(row=0, column=1, padx=88, pady=10,
                                        sticky='w')
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')

        self.callUs = tk.Label(self, text='Please call 530-752-1123 for assistance.', foreground=wFG,
                                    background=wBG, font=(iFont, 12)).grid(row=1, columnspan=2,
                                                                           padx=30, pady=15)
        self.keepGoin = tk.Button(self, text='Continue', width=30, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8,
                                  command=lambda: controller.postBadPump()).grid(row=2,
                                columnspan=2, padx=5, pady=15)

class sysError(tk.Frame):
    """     This is a generic error frame to alert the user that there has been a problem detected.     """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.screenLabel = tk.Label(self, text='System Error Detected', foreground=wFG, background=wBG,
                                    font=(iFont, 12), justify="center").grid(row=0, columnspan=2,
                                                           pady=10)
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1,background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(modTest)).grid(row=2,
                                    column=0, sticky='sw')
        

        self.callUs = tk.Label(self, text='Please call 530-752-1123 for assistance. \n \n \n',
                               foreground=wFG, background=wBG, font=(iFont, 12), justify="center"
                               ).grid(row=1, columnspan=2, padx=26, pady=20)
        self.trouble = tk.Button(self, text='Troubleshoot', width=20, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8,
                                  command=lambda: controller.troubleshoot()).grid(row=2,
                                column=1, sticky='se')

class troubleGen(tk.Frame):
    """     This is a generic troubleshooting frame to help the user troubleshoot issues.     """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        instr = 'Please call 530-752-1123 for assistance. \n \n \n'

        self.screenLabel = tk.Label(self, text='Troubleshooting', foreground=wFG, background=wBG,
                                    font=(iFont, 12), justify="center").grid(row=0, columnspan=2,
                                                           pady=10)
        # Need to get .gifs and install PIL
##        self.home = tk.Button(self, image=homeIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=0,
##                                    column=0, sticky=NW)
##        self.back = tk.Button(self, image=backIMG, width=wIMG, height=hIMG,
##                              command=lambda: controller.show_frame(home)).grid(row=3,
##                                    column=0, sticky=SW)

        self.home = tk.Button(self, text='Home', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(home)).grid(row=0,
                                    column=0, sticky='nw')
        self.back = tk.Button(self, text='Back', width=5, height=1, background=bBG, font=(iFont,
                                10), command=lambda: controller.show_frame(sysError)).grid(row=2,
                                    column=0, sticky='sw')
        

        self.instruct = tk.Label(self, text=instr, foreground=wFG, background=wBG, font=(iFont, 12),
                               justify="center").grid(row=1, columnspan=2, padx=26, pady=20)
        # Needs changed when we create troubleshooting sequence.
        self.trouble = tk.Button(self, text='Continue', width=20, height=1,
                              background=bBG, font=(iFont, 10), borderwidth=8,
                                  command=lambda: controller.show_frame(home)).grid(row=2,
                                column=1, sticky='se')

class noErrors(tk.Frame):
    """ In the happy event that no errors are detected by the system, this screen relieves the worries
        of the user and informs them of that. Continue will take them back to the home frame. """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.msg = tk.Label(self, text='No Equipment Errors Were Detected', foreground=wFG,
                            background=wBG, font=(iFont, 14), justify="center").grid(padx=20, pady=30)

        self.cont = tk.Button(self, text='Continue', width=20, height=1, background=bBG, font=(iFont, 14),
                              command=lambda: controller.show_frame(home), justify="center").grid(
                                  row=1, pady=10)



########################################################################################################
#####################           End  Frames                                     ########################
########################################################################################################
 



app = Application()
app.title('Controller GUI')
app.geometry('384x197+600+600')
app.mainloop()
