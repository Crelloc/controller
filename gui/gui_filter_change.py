# need to add parent directory for includes
import os, sys
sys.path.insert(0,os.path.pardir)

import tkinter as tk
import datetime

from gui_frame import PageFrame
import gui_common as common
import gui_operator_initials as opinit
import gui_problems
from sdcard import SdcardState


class FilterChangeOperatorPage(opinit.ChooseInitialsPage):
    def __init__(self, parent, controller):
        opinit.ChooseInitialsPage.__init__(self, parent, controller)

        self.page_number = 'Page:\nEXRE1'
        self.back = None

        self.title_area.update_title("Operator:")

        self.next_page = ExposedTestRunPage
        self.custom_initials_page = FilterChangeCustomOperatorPage
        

class FilterChangeCustomOperatorPage(opinit.ChooseCustomInitialsPage):
    def __init__(self, parent, controller):
        opinit.ChooseCustomInitialsPage.__init__(self, parent, controller)

        self.page_number = "Page:\nOPTR1"
        self.back = FilterChangeOperatorPage

        self.next_page = ExposedTestRunPage

class ExposedTestRunPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nEXRE2"
        self.back = FilterChangeOperatorPage

        self.title_area = common.TitleArea(self, "Exposed Readings", None, None)
        self.title_area.pack(fill=tk.X)

        self.finish_time = None
        self.remaining_time_var = tk.StringVar()
        self.status_text_var = tk.StringVar()

        self.parent_frame = tk.Frame(self)
        self._build_frames(self.parent_frame)
        self.parent_frame.pack()

    def reset(self):
        children = self.parent_frame.pack_slaves()
        for child in children:
            child.pack_forget()

        data = self.controller.ipc.get_data('module -1 list')
        ebox_state = data['state']
        state_name = ebox_state.name.lower()
        print(state_name)
        
        restart_states = ['cartridges_changed', 'clean_test_running', 'clean_test_finished']
        if state_name in restart_states:
            self.restart_frame.pack()
        else:
            self.start_frame.pack()

    def update(self):
        if self.finish_time is not None:
            now = datetime.datetime.now()
            data = self.controller.ipc.get_data('module -1 list')
            ebox_state = data['state']
            state_name = ebox_state.name
            if state_name is None:
                state_name = ' '
            #print(state_name)
            if not state_name.lower().startswith('exposed_test'):
                # test failed to start
                # Send command to start test
                self.controller.ipc.send_command("controller leak final")
                # reset the countdown
                self.finish_time = self._calculate_finish_time()
                self.status_text_var.set("Initializing test")
            else:
                logsheet_data = data['ebox_logsheet']
                status = logsheet_data['status_text']
                self.status_text_var.set(status)
            
            finished = False
            if 0 != state_name.lower().count('finished'):
                finished = True
                
            if finished:
                self.finish_time = None
                self.running_frame.pack_forget()
                self.final_frame.pack()
                self.enable_home_button(True)
                self.enable_back_button(True)
            else:
                delta = self.finish_time - now
                seconds = delta.seconds
                if (self.finish_time < now):
                    seconds = 0
                self.remaining_time_var.set('time remaining: %d' % seconds)
            # update the screen
            self.update_idletasks()
        

    def _build_frames(self, parent_frame):
        # restart frame:
        self.restart_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.restart_frame, text='Filter change process was interrupted.',
                         font=common.Fonts.SMALL_LABEL)
        label.pack()
        restart_button_frame = tk.Frame(self.restart_frame, bg=common.Colors.BG)
        button = tk.Button(restart_button_frame, text='Restart', font=common.Fonts.BUTTON,
                           command=self._restart_test)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(restart_button_frame, text='Resume', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(CleanTestRunPage))
        button.pack(side=tk.LEFT, padx=10)
        restart_button_frame.pack(side=tk.BOTTOM)
        
        # start frame:
        self.start_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.start_frame, text="\n\n\u2022 Get logsheet.\n",
                         font=common.Fonts.SMALLER_LABEL)
        label.pack()
        label = tk.Label(self.start_frame, text="\u2022 As each pump starts, note any pumps that\n sound abnormal.\n\n",
                         font=common.Fonts.SMALLER_LABEL)
        label.pack()
        button = tk.Button(self.start_frame, text='Start', font=common.Fonts.BUTTON,
                           command=self._start_test)
        button.pack()

        # running frame:
        self.running_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.running_frame, text='Storing exposed readings', font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, textvariable=self.remaining_time_var, font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, textvariable=self.status_text_var, font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, text="\n\n\u2022 LISTEN FOR ABNORMAL SOUNDING PUMPS.\n",
                         font=common.Fonts.SMALL_LABEL2)
        label.pack()
        # final frame:
        self.final_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.final_frame, text='test finished', font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.final_frame, text='Do any pumps sound abnormal?',
                         font=common.Fonts.SMALL_LABEL)
        label.pack()
        button_frame = tk.Frame(self.final_frame, bg=common.Colors.BG)
        button = tk.Button(button_frame, text='Yes', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(FinalProblemsPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text='No', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ExposedTestResultsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

    def _restart_test(self):
        self.restart_frame.pack_forget()
        self.start_frame.pack()

    def _start_test(self):
        # Send command to start test
        self.controller.ipc.send_command("controller leak final")
        # Update the GUI
        self.start_frame.pack_forget()
        self.running_frame.pack()
        self.enable_home_button(False)
        self.enable_back_button(False)
        
        self.finish_time = self._calculate_finish_time()

    def _calculate_finish_time(self):
        # Do a rough calculation about how long the countdown should last
        modules = self.controller.ipc.get_data('list modules enabled')
        module_count = len(modules)
        countdown = module_count * 5 # 5 seconds per pump startup
        countdown += 3 * 2 # 2 seconds per solenoid test
        countdown += module_count * 2 # 2 seconds per pump cooldown
        countdown += 10 # extra time
        wait_time = datetime.timedelta(seconds=countdown)
        return datetime.datetime.now() + wait_time

        
class FinalProblemsPage(gui_problems.ProblemsPage):
    def __init__(self, parent, controller):
        gui_problems.ProblemsPage.__init__(self, parent, controller)

        self.page_number = "Page:\nPROB1"
        self.back = ExposedTestRunPage

class ExposedTestResultsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nEXRE3"
        self.back = ExposedTestRunPage

        self.title_area = common.TitleArea(self, "Exposed Filter Readings", self._next_module, self._previous_module)
        self.title_area.pack(fill=tk.X)

        self.module_data = []
        # sample date, ori, cyc, et
        for index in range(0,3):
            cassette_data = {}
            var_names = ['sample_date','ori','cyc','et']
            for name in var_names:
                var = tk.StringVar()
                var.set('xx')
                cassette_data[name] = var
            self.module_data.append(cassette_data)
        self.sampler_date_var = tk.StringVar()
        self.sampler_time_var = tk.StringVar()
        self.temperature_var = tk.StringVar()
        self.week_var = tk.StringVar()

        self._build_form()
        self.module_index = 0
        self.next_button = tk.Button(self, text="Next", font=common.Fonts.BUTTON,
                                     command=lambda: self.controller.show_frame(ExposedTestCheckLogsheetPage))

    def reset(self):
        self.module_index = 0
        self.modules = self.controller.ipc.get_data('list modules enabled')
        self._update_title()
        self._get_data(self.module_index)
        self.next_button.pack_forget()

    def _next_module(self):
        if (self.module_index + 1) < len(self.modules):
            self.module_index += 1
            self._update_title()
            self._get_data(self.module_index)
        # if we reached the last page display the next button
        if (self.module_index + 1) == len(self.modules):
            self.next_button.pack()

    def _previous_module(self):
        if self.module_index == 0:
            pass
        else:
            self.module_index -= 1
            self._update_title()
            self._get_data(self.module_index)

    def _get_data(self, module_index):
        module_number = 1
        if module_index < len(self.modules):
            module_number = self.modules[module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        logsheet_data = data['ebox_logsheet']
        sampler_datetime = logsheet_data['final_date']
        self.sampler_date_var.set(sampler_datetime.strftime('%x'))
        self.sampler_time_var.set(sampler_datetime.strftime('%X'))
        temperature = logsheet_data['temperature']
        self.temperature_var.set('{:0.2f}'.format(temperature))
        week = logsheet_data['week']
        self.week_var.set('Week %d' % week)
        cassettes = logsheet_data['cassette_data']

        index = 0
        for cassette in cassettes:
            module_display = self.module_data[index]
            sample_date = cassette['sample_date']
            module_display['sample_date'].set(sample_date.strftime("%x"))
            ori = cassette['orifice_pressure_final']
            module_display['ori'].set('{:0.2f}'.format(ori))
            cyc = cassette['cyclone_pressure_final']
            module_display['cyc'].set('{:0.4f}'.format(cyc))
            et = cassette['elapsed_sampling_time']
            module_display['et'].set(str(et))
            index += 1

        # check if there are extra display rows
        if len(cassettes) < len(self.module_data):
            cassette_data_end = len(cassettes)
            module_data_end = len(self.module_data)
            # blank the extra rows by clearing the related StringVars
            for index in range(cassette_data_end, module_data_end):
                module_display = self.module_data[index]
                module_display['sample_date'].set('')
                module_display['ori'].set('')
                module_display['cyc'].set('')
                module_display['et'].set('')

    def _update_title(self):
        title = 'Module X'
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]
            title = 'Module %d' % module_number
        self.title_area.update_title(title)

    def _build_form(self):
        frame = tk.Frame(self, bg=common.Colors.BG)

        padx = 10
        row = 0
        date_frame = tk.Frame(frame, bg=common.Colors.BG)
        label = tk.Label(date_frame, textvariable=self.sampler_date_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        label = tk.Label(date_frame, textvariable=self.sampler_time_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        label = tk.Label(date_frame, textvariable=self.week_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        date_frame.grid(row=row, column=0, columnspan=4, pady=(10, 3))

        row += 1
        temperature_frame = tk.Frame(frame, bg=common.Colors.BG)
        label = tk.Label(temperature_frame, text="Temperature", font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        label = tk.Label(temperature_frame, textvariable=self.temperature_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        temperature_frame.grid(row=row, column=0, columnspan=4, pady=(3, 15))
        
        row += 1
        label = tk.Label(frame, text="Sample date", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=0, padx=padx)
        label = tk.Label(frame, text="Ori", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=1, padx=padx)
        label = tk.Label(frame, text="Cyc", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=2, padx=padx)
        label = tk.Label(frame, text="ET", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=3, padx=padx)

        row += 1
        for cassette_data in self.module_data:
            label = tk.Label(frame, textvariable=cassette_data['sample_date'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=0, padx=padx)
            label = tk.Label(frame, textvariable=cassette_data['ori'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=1, padx=padx)
            label = tk.Label(frame, textvariable=cassette_data['cyc'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=2, padx=padx)
            label = tk.Label(frame, textvariable=cassette_data['et'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=3, padx=padx)
            row += 1
        frame.pack()


class ExposedTestCheckLogsheetPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nEXRE4"
        self.back = ExposedTestResultsPage

        self.title_area = common.TitleArea(self, "Check logsheet", None, None)
        self.title_area.pack(fill=tk.X)

        label = tk.Label(self, text="Please compare logsheet against sample logsheet.", font=common.Fonts.SMALL_LABEL,
                         wraplength=450)
        label.pack()
        label = tk.Label(self, text="Are the values within range?", font=common.Fonts.SMALL_LABEL)
        label.pack()

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Yes", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ChangeFilterInstructionsPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="No", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ExposedTestCheckLogsheetProblemsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)


class ExposedTestCheckLogsheetProblemsPage(gui_problems.ProblemsPage):
    def __init__(self, parent, controller):
        gui_problems.ProblemsPage.__init__(self, parent, controller)

        self.page_number = "Page:\nPROB2"
        self.back = ExposedTestCheckLogsheetPage
        

class ChangeFilterInstructionsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nCHNG1"
        self.back = ExposedTestCheckLogsheetPage

        self.title_area = common.TitleArea(self, "Change cartridges", None, None)
        self.title_area.pack(fill=tk.X)

        self.sampling_movable_cassette = False
        self.movable_cassette_date = None
        self.next_cartridge_week = 0
        self.next_cartridge_dates = []
        self.change_date = None

        self.movable_cassette_var = tk.StringVar()
        self.bag_week_var = tk.StringVar()
        self.movable_cassette_date_var = tk.StringVar()

        self.parent_frame = tk.Frame(self, bg=common.Colors.BG)
        self.parent_frame.pack()
        self._build_frames()
        self.current_page_index = 0
        
        button_frame = tk.Frame(self, bg=common.Colors.BG)
        self.next_button = tk.Button(button_frame, text="Next", font=common.Fonts.BUTTON, command=self._next_page)
        self.next_button.pack(side=tk.RIGHT, padx=2)
        self.done_button = tk.Button(button_frame, text="Done", font=common.Fonts.BUTTON, command=self._done)
        self.done_button.pack(side=tk.RIGHT, padx=2)
        self.previous_button = tk.Button(button_frame, text="Prev", font=common.Fonts.BUTTON, command=self._previous_page)
        self.previous_button.pack(side=tk.RIGHT, padx=2)
        button_frame.pack(side=tk.BOTTOM)

    def reset(self):
        data = self.controller.ipc.get_data('module -1 list')
        self.sampling_movable_cassette = data['sampling_movable_cassette']

        next_cartridge = data['next_cartridge']
        self.next_cartridge_week = next_cartridge['schedule_index'] + 1
        self.next_cartridge_dates = next_cartridge['dates']
        first_sampling_date = self.next_cartridge_dates[0]
        first_sampling_weekday = first_sampling_date.weekday() # should be 0-6
        change_day_weekday = self.controller.config.ebox.cartridge_change_day_weekday
        # need to adjust the date to be the change day, first figure out how many days we are off
        offset = 0
        if change_day_weekday < first_sampling_weekday:
            offset = first_sampling_weekday - change_day_weekday
        elif first_sampling_weekday < change_day_weekday:
            offset = (first_sampling_weekday + 7) - change_day_weekday
        delta = datetime.timedelta(days=offset)
        self.change_date = first_sampling_date - delta
        
        self.bag_week_var.set("Week %d (%s)" % (self.next_cartridge_week, self.change_date.strftime("%x")))

        if self.sampling_movable_cassette:
            self.movable_cassette_var.set("Movable cassette")
        else:
            self.movable_cassette_var.set("")
        # movable cartridge is the last date on the cassette
        self.movable_cassette_date = self.next_cartridge_dates[-1:][0]
        self.movable_cassette_date_var.set(self.movable_cassette_date.strftime("%x"))

        # initialize the page frames.  exclude the movable cassette instructions if not needed.
        self.page_frames = self.all_page_frames
        if not self.sampling_movable_cassette:
            self.page_frames = []
            for frame in self.all_page_frames:
                if frame not in self.movable_cassette_page_frames:
                    self.page_frames.append(frame)
        
        children = self.parent_frame.pack_slaves()
        for child in children:
            child.pack_forget()
        self.current_page_index = 0
        self.page_frames[self.current_page_index].pack()
        self.previous_button['state'] = tk.DISABLED

    def _build_frames(self):
        self.all_page_frames = []
        self.page_frames = []
        self.movable_cassette_page_frames = []
        
        # first page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        label = tk.Label(frame, text="Please change the cartridge.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, textvariable=self.movable_cassette_var, font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, textvariable=self.bag_week_var, font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, text="Press next to see more instructions", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, text="Press done when finished changing the cartridges.",
                         font=common.Fonts.SMALL_LABEL, wraplength=450)
        label.pack(pady=5)
        self.all_page_frames.append(frame)

        # second page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        label = tk.Label(frame, text="Page two of instructions.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        self.all_page_frames.append(frame)

        # third page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        label = tk.Label(frame, text="Page three of instructions.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, text="Please get the bag labeled:", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, textvariable=self.bag_week_var, font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        self.all_page_frames.append(frame)

        # fourth page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        test_image = tk.PhotoImage(file="images/pixelgrid.png")
        label = tk.Label(frame, image=test_image, height=300, width=640)
        # the picture must be assigned to a perminate object otherwise it will be go out of scope
        #  can also be assigned to the frame (self.test_image)
        label.image = test_image
        label.pack()
        self.all_page_frames.append(frame)

        # fourth page 2
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        test_image = tk.PhotoImage(file="images/pixelgrid.png")
        label = tk.Label(frame, image=test_image, height=300, width=300)
        label.image = test_image # needed to keep image from going out of scope, not actually used for anything else
        label.pack(side=tk.LEFT)
        # screen is about 650px across, set image to 300px and text wraplength to 300px to make sure it fits.
        #  can be adjusted up to fit tighter in the space.  Can also make picture smaller and text wider.
        label = tk.Label(frame, text='text next to the picture describing something',
                         wraplength=300, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT)
        self.all_page_frames.append(frame)

        # movable cassette first page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        label = tk.Label(frame, text="Movable cassette.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, text="Please move cassette labeled:", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, textvariable=self.movable_cassette_date_var, font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        self.all_page_frames.append(frame)
        self.movable_cassette_page_frames.append(frame)

        # movable cassette second page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        label = tk.Label(frame, text="Movable cassette 2.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        self.all_page_frames.append(frame)
        self.movable_cassette_page_frames.append(frame)

        # fifth page
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        label = tk.Label(frame, text="Page five of instructions.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        self.all_page_frames.append(frame)

    def _next_page(self):
        # hide the previous page/frame
        self.page_frames[self.current_page_index].pack_forget()
        self.current_page_index += 1
        if len(self.page_frames) <= self.current_page_index:
            self.current_page_index = len(self.page_frames) - 1
        # show the next page/frame
        self.page_frames[self.current_page_index].pack()
        # add the previous button back in if not already there
        if 1 == self.current_page_index: 
            self.previous_button['state'] = tk.NORMAL
            

    def _previous_page(self):
        # hide the previous page/frame
        self.page_frames[self.current_page_index].pack_forget()
        self.current_page_index -= 1
        if self.current_page_index < 0:
            self.current_page_index = 0
        # show the next page/frame
        self.page_frames[self.current_page_index].pack()
        # remove the previous button if we are on the first page
        if 0 == self.current_page_index: 
            self.previous_button['state'] = tk.DISABLED

    def _done(self):
        modules = self.controller.ipc.get_data('list modules enabled')
        for module in modules:
            cmd = 'module %d change cartridge' % module
            self.controller.ipc.send_command(cmd)
        self.controller.show_frame(CleanTestRunPage)
        

class CleanTestRunPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nCLRE1"
        self.back = CleanTestRunPage

        self.title_area = common.TitleArea(self, "Clean Readings", None, None)
        self.title_area.pack(fill=tk.X)

        self.finish_time = None
        self.remaining_time_var = tk.StringVar()
        self.status_text_var = tk.StringVar()

        self.parent_frame = tk.Frame(self)
        self._build_frames(self.parent_frame)
        self.parent_frame.pack()

    def reset(self):
        children = self.parent_frame.pack_slaves()
        for child in children:
            child.pack_forget()
        self.start_frame.pack()

    def update(self):
        if self.finish_time is not None:
            now = datetime.datetime.now()
            data = self.controller.ipc.get_data('module -1 list')
            ebox_state = data['state']
            state_name = ebox_state.name
            if state_name is None:
                state_name = ' '
            #print(state_name)
            if not state_name.lower().startswith('clean_test'):
                # test failed to start
                # Send command to start test
                self.controller.ipc.send_command("controller leak initial")
                # reset the countdown
                self.finish_time = self._calculate_finish_time()
                self.status_text_var.set("Initializing test")
            else:
                logsheet_data = data['ebox_logsheet']
                status = logsheet_data['status_text']
                self.status_text_var.set(status)
            
            finished = False
            if 0 != state_name.lower().count('finished'):
                finished = True
                
            if finished:
                self.finish_time = None
                self.enable_home_button(True)
                self.enable_back_button(True)
                self.running_frame.pack_forget()
                self.final_frame.pack()
            else:
                delta = self.finish_time - now
                seconds = delta.seconds
                if (self.finish_time < now):
                    seconds = 0
                self.remaining_time_var.set('time remaining: %d' % seconds)
            # update the screen
            self.update_idletasks()

    def _build_frames(self, parent_frame):
        # start frame:
        self.start_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.start_frame, text='Press button to start install test',
                         font=common.Fonts.SMALL_LABEL)
        label.pack()
        button = tk.Button(self.start_frame, text='Start', font=common.Fonts.BUTTON,
                           command=self._start_test)
        button.pack()

        # running frame:
        self.running_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.running_frame, text='running install test', font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, textvariable=self.remaining_time_var, font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, textvariable=self.status_text_var, font=common.Fonts.SMALL_LABEL)
        label.pack()

        # final frame:
        self.final_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.final_frame, text='test finished', font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.final_frame, text='Do any pumps sound abnormal?',
                         font=common.Fonts.SMALL_LABEL)
        label.pack()
        button_frame = tk.Frame(self.final_frame, bg=common.Colors.BG)
        button = tk.Button(button_frame, text='Yes', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(InitialProblemsPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text='No', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(CleanTestResultsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

    def _start_test(self):
        self.start_frame.pack_forget()
        self.running_frame.pack()
        self.enable_home_button(False)
        self.enable_back_button(False)
        self.finish_time = self._calculate_finish_time()
        self.controller.ipc.send_command("controller leak initial")

    def _calculate_finish_time(self):
        modules = self.controller.ipc.get_data('list modules enabled')
        module_count = len(modules)
        countdown = module_count * 5 # 5 seconds per pump startup
        countdown += 3 * 2 # 2 seconds per solenoid test
        countdown += module_count * 2 # 2 seconds per pump cooldown
        countdown += 10 # extra time
        wait_time = datetime.timedelta(seconds=countdown)
        return datetime.datetime.now() + wait_time
        

class InitialProblemsPage(gui_problems.ProblemsPage):
    def __init__(self, parent, controller):
        gui_problems.ProblemsPage.__init__(self, parent, controller)

        self.page_number = "Page:\nPROB3"
        self.back = CleanTestRunPage

class CleanTestResultsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nCLRE2"
        self.back = CleanTestRunPage

        self.title_area = common.TitleArea(self, "Initial Results", self._next_module, self._previous_module)
        self.title_area.pack(fill=tk.X)

        # Sample date, mxvac, Ori, Cyc

        self.module_data = []
        # sample date, ori, cyc, et
        for index in range(0,3):
            cassette_data = {}
            var_names = ['sample_date','mxvac','ori','cyc']
            for name in var_names:
                var = tk.StringVar()
                var.set('xx')
                cassette_data[name] = var
            self.module_data.append(cassette_data)
        self.sampler_date_var = tk.StringVar()
        self.sampler_time_var = tk.StringVar()
        self.week_var = tk.StringVar()

        self._build_form()
        self.module_index = 0
        self.next_button = tk.Button(self, text="Done", font=common.Fonts.BUTTON,
                                     command=lambda: self.controller.show_frame(CleanTestCheckLogsheetPage))

    def reset(self):
        self.module_index = 0
        self.modules = self.controller.ipc.get_data('list modules enabled')
        self._update_title()
        self._get_data(self.module_index)
        self.next_button.pack_forget()

    def _next_module(self):
        if (self.module_index + 1) < len(self.modules):
            self.module_index += 1
            self._update_title()
            self._get_data(self.module_index)
        # if we reached the last page display the next button
        if (self.module_index + 1) == len(self.modules):
            self.next_button.pack()

    def _previous_module(self):
        if self.module_index == 0:
            pass
        else:
            self.module_index -= 1
            self._update_title()
            self._get_data(self.module_index)

    def _get_data(self, module_index):
        module_number = 1
        if module_index < len(self.modules):
            module_number = self.modules[module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        logsheet_data = data['ebox_logsheet']
        cassettes = logsheet_data['cassette_data']
        mxvac = logsheet_data['orifice_pressure_min']
        sampler_datetime = logsheet_data['initial_date']
        self.sampler_date_var.set(sampler_datetime.strftime('%x'))
        self.sampler_time_var.set(sampler_datetime.strftime('%X'))
        week = logsheet_data['week']
        self.week_var.set('Week %d' % week)

        index = 0
        for cassette in cassettes:
            module_display = self.module_data[index]
            sample_date = cassette['sample_date']
            module_display['sample_date'].set(sample_date.strftime("%x"))
            ori = cassette['orifice_pressure_initial']
            module_display['ori'].set('{:0.2f}'.format(ori))
            cyc = cassette['cyclone_pressure_initial']
            module_display['cyc'].set('{:0.4f}'.format(cyc))
            # only show the max ori on the first line
            if 0 == index:
                module_display['mxvac'].set('{:0.2f}'.format(mxvac))
            else:
                module_display['mxvac'].set('')
            index += 1

        # check if there are extra display rows
        if len(cassettes) < len(self.module_data):
            cassette_data_end = len(cassettes)
            module_data_end = len(self.module_data)
            # blank the extra rows by clearing the related StringVars
            for index in range(cassette_data_end, module_data_end):
                module_display = self.module_data[index]
                module_display['sample_date'].set('')
                module_display['mxvac'].set('')
                module_display['ori'].set('')
                module_display['cyc'].set('')

    def _update_title(self):
        title = 'Module X'
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]
            title = 'Module %d' % module_number
        self.title_area.update_title(title)

    def _build_form(self):
        frame = tk.Frame(self, bg=common.Colors.BG)

        padx = 10
        row = 0
        date_frame = tk.Frame(frame, bg=common.Colors.BG)
        label = tk.Label(date_frame, textvariable=self.sampler_date_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        label = tk.Label(date_frame, textvariable=self.sampler_time_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        label = tk.Label(date_frame, textvariable=self.week_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=padx)
        date_frame.grid(row=row, column=0, columnspan=4, pady=(10, 15))

        row += 1
        label = tk.Label(frame, text="Sample date", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=0, padx=padx)
        label = tk.Label(frame, text="MxVac", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=1, padx=padx)
        label = tk.Label(frame, text="Ori", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=2, padx=padx)
        label = tk.Label(frame, text="Cyc", font=common.Fonts.SMALL_LABEL)
        label.grid(row=row, column=3, padx=padx)

        row += 1
        for cassette_data in self.module_data:
            label = tk.Label(frame, textvariable=cassette_data['sample_date'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=0, padx=padx)
            label = tk.Label(frame, textvariable=cassette_data['mxvac'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=1, padx=padx)
            label = tk.Label(frame, textvariable=cassette_data['ori'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=2, padx=padx)
            label = tk.Label(frame, textvariable=cassette_data['cyc'], font=common.Fonts.SMALL_LABEL)
            label.grid(row=row, column=3, padx=padx)
            row += 1
        frame.pack()
        

class CleanTestCheckLogsheetPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nCLRE3"
        self.back = CleanTestResultsPage

        self.title_area = common.TitleArea(self, "Check logsheet", None, None)
        self.title_area.pack(fill=tk.X)

        label = tk.Label(self, text="Please compare logsheet against sample logsheet.", font=common.Fonts.SMALL_LABEL,
                         wraplength=450)
        label.pack()
        label = tk.Label(self, text="Are the values within range?", font=common.Fonts.SMALL_LABEL)
        label.pack()

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Yes", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ChangeFlashcardPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="No", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(CleanTestCheckLogsheetProblemsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)


class CleanTestCheckLogsheetProblemsPage(gui_problems.ProblemsPage):
    def __init__(self, parent, controller):
        gui_problems.ProblemsPage.__init__(self, parent, controller)

        self.page_number = "Page:\nPROB4"
        self.back = CleanTestCheckLogsheetPage


class ChangeFlashcardPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nCHNG2"
        self.back = CleanTestCheckLogsheetPage

        self.title_area = common.TitleArea(self, "Change flashcard", None, None)
        self.title_area.pack(fill=tk.X)

        self.parent_frame = tk.Frame(self, bg=common.Colors.BG)
        self.parent_frame.pack()
        self._build_frames()
        self.current_page_index = 0
        
        button_frame = tk.Frame(self, bg=common.Colors.BG)
        self.next_button = tk.Button(button_frame, text="Next", font=common.Fonts.BUTTON, command=self._next_page)
        self.next_button.pack(side=tk.RIGHT, padx=2)
        self.done_button = tk.Button(button_frame, text="Done", font=common.Fonts.BUTTON, command=self._done)
        self.done_button.pack(side=tk.RIGHT, padx=2)
        self.previous_button = tk.Button(button_frame, text="Prev", font=common.Fonts.BUTTON, command=self._previous_page)
        self.previous_button.pack(side=tk.RIGHT, padx=2)
        button_frame.pack()

    def reset(self):
        children = self.parent_frame.pack_slaves()
        for child in children:
            child.pack_forget()
        self.current_page_index = 0
        self.page_frames[self.current_page_index].pack()
        self.previous_button.pack_forget()
        # let the backend know to start the flashcard change process
        self.controller.ipc.send_command("controller flashcard change")

    def _build_frames(self):
        self.page_frames = []
        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        self.page_frames.append(frame)

        label = tk.Label(frame, text="Please change the flashcard.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, text="Press next to see more instructions", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        label = tk.Label(frame, text="Press done when finished changing the flashcard.",
                         font=common.Fonts.SMALL_LABEL, wraplength=450)
        label.pack(pady=5)

        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        self.page_frames.append(frame)

        label = tk.Label(frame, text="Page one of instructions.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)
        

        frame = tk.Frame(self.parent_frame, bg=common.Colors.BG)
        self.page_frames.append(frame)

        label = tk.Label(frame, text="Page two of instructions.", font=common.Fonts.SMALL_LABEL)
        label.pack(pady=5)

    def _next_page(self):
        # hide the previous page/frame
        self.page_frames[self.current_page_index].pack_forget()
        self.current_page_index += 1
        if len(self.page_frames) <= self.current_page_index:
            self.current_page_index = len(self.page_frames) - 1
        # show the next page/frame
        self.page_frames[self.current_page_index].pack()
        # add the previous button back in if not already there
        if 1 == self.current_page_index and not self.previous_button.winfo_ismapped():
            self.previous_button.pack()
            

    def _previous_page(self):
        # hide the previous page/frame
        self.page_frames[self.current_page_index].pack_forget()
        self.current_page_index -= 1
        if self.current_page_index < 0:
            self.current_page_index = 0
        # show the next page/frame
        self.page_frames[self.current_page_index].pack()
        # remove the previous button if we are on the first page
        if 0 == self.current_page_index and self.previous_button.winfo_ismapped():
            self.previous_button.pack_forget()

    def _done(self):
        self.controller.ipc.send_command("controller flashcard check")
        self.controller.show_frame(ChangeFlashcardCheckPage)

class ChangeFlashcardCheckPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "Page:\nCLRE4"
        self.back = CleanTestResultsPage

        self.title_area = common.TitleArea(self, "Flashcard check", None, None)
        self.title_area.pack(fill=tk.X)

        self.fail_time = None
        self.timeout_time = None
        self.remaining_time_var = tk.StringVar()
        self.status_text_var = tk.StringVar()
        self.state_var = tk.StringVar()

        self.parent_frame = tk.Frame(self)
        self._build_frames(self.parent_frame)
        self.parent_frame.pack()

    def reset(self):
        children = self.parent_frame.pack_slaves()
        for child in children:
            child.pack_forget()
        self._start_test()

    def _build_frames(self, parent_frame):

        # running frame:
        self.running_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        
        label = tk.Label(self.running_frame, textvariable=self.remaining_time_var, font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, textvariable=self.status_text_var, font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.running_frame, textvariable=self.state_var, font=common.Fonts.SMALL_LABEL)
        label.pack()

        # error frame:
        self.error_frame = tk.Frame(parent_frame, bg=common.Colors.BG)

        label = tk.Label(self.error_frame, text='SD card initialization failed', font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.error_frame, textvariable=self.status_text_var, font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(self.error_frame, textvariable=self.state_var, font=common.Fonts.SMALL_LABEL)
        label.pack()

        button_frame = tk.Frame(self.error_frame, bg=common.Colors.BG)
        button = tk.Button(button_frame, text='Back', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ChangeFlashcardPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text='Skip', font=common.Fonts.BUTTON,
                           command=self._done)
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

        # final frame:
        self.final_frame = tk.Frame(parent_frame, bg=common.Colors.BG)
        label = tk.Label(self.final_frame, text='SD card initialization finished', font=common.Fonts.SMALL_LABEL)
        label.pack()
        
        button_frame = tk.Frame(self.final_frame, bg=common.Colors.BG)
        button = tk.Button(button_frame, text='Yes', font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(InitialProblemsPage))
        #button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text='Done', font=common.Fonts.BUTTON,
                           command=self._done)
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

    def update(self):
        now = datetime.datetime.now()
        data = self.controller.ipc.get_data('controller flashcard status')
        state = data['state']
        status = data['status']

        self.state_var.set('state: %s' % str(state))
        self.status_text_var.set('status: %s' % str(status))
        
        if self.fail_time is not None:
            
            finished = False
            if state == SdcardState.READY:
                finished = True
            elif self.fail_time < now and state == SdcardState.ERROR:
                finished = True
            elif self.timeout_time < now and state == SdcardState.INITIALIZING:
                finished = True
                
            if finished:
                self._finished_init(state)
            else:
                delta = self.timeout_time - now
                seconds = delta.seconds
                self.remaining_time_var.set('time remaining: %d' % seconds)
                
            # update the screen
            self.update_idletasks()

    def _finished_init(self, sdcard_state):
        self.fail_time = None
        self.timeout_time = None
        self.enable_home_button(True)
        self.enable_back_button(True)
        self.running_frame.pack_forget()
        if sdcard_state == SdcardState.READY:
            self.final_frame.pack()
        else:
            self.error_frame.pack()

    def _done(self):
        self.controller.ipc.send_command("controller leak finished")
        self.controller.show_frame(None)

    def _start_test(self):
        self.running_frame.pack()
        self.enable_home_button(False)
        self.enable_back_button(False)
        fail_wait_delta = datetime.timedelta(seconds=15)
        timeout_wait_delta = datetime.timedelta(seconds=60)
        now = datetime.datetime.now()
        self.fail_time = now + fail_wait_delta
        self.timeout_time = now + timeout_wait_delta

