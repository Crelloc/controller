import tkinter as tk
import sys

from gui_frame import PageFrame
import gui_common as common

class ExitOptionsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'E0'

        title_area = common.TitleArea(self, "Exit Menu")
        title_area.pack(fill=tk.X)

        tk.Button(self, text="Shutdown", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(ExitShutdownPage)).pack()

        tk.Button(self, text="Exit app", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(ExitAppPage)).pack()

        tk.Button(self, text="Exit to desktop", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(ExitToDesktopPage)).pack()


class ExitShutdownPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'E0'

        title_area = common.TitleArea(self, "Shutdown")
        title_area.pack(fill=tk.X)

        label = tk.Label(self, text="Are you sure you want to shutdown the controller?", font=common.Fonts.SMALL_LABEL,
                         wraplength=450)
        label.pack()

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Yes", font=common.Fonts.BUTTON,
                           command=self._exit)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="Cancel", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ExitOptionsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

    def _exit(self):
        cmd = 'exit shutdown'
        self.controller.ipc.send_command(cmd)
        quit()

        
class ExitAppPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'E0'

        title_area = common.TitleArea(self, "Exit Program")
        title_area.pack(fill=tk.X)

        label = tk.Label(self, text="Are you sure you want to close the program?", font=common.Fonts.SMALL_LABEL,
                         wraplength=450)
        label.pack()

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Yes", font=common.Fonts.BUTTON,
                           command=self._exit)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="Cancel", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ExitOptionsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

    def _exit(self):
        cmd = 'exit program'
        self.controller.ipc.send_command(cmd)
        quit()

class ExitToDesktopPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'E0'

        title_area = common.TitleArea(self, "Exit to Desktop")
        title_area.pack(fill=tk.X)

        label = tk.Label(self, text="Are you sure you want to exit to desktop?", font=common.Fonts.SMALL_LABEL,
                         wraplength=450)
        label.pack()

        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Yes", font=common.Fonts.BUTTON,
                           command=self._exit)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="Cancel", font=common.Fonts.BUTTON,
                           command=lambda: self.controller.show_frame(ExitOptionsPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack(pady=20)

    def _exit(self):
        cmd = 'exit to desktop'
        self.controller.ipc.send_command(cmd)
        quit()
