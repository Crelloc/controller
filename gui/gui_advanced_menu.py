import tkinter as tk
import sys

from gui_frame import PageFrame
import gui_common as common
import gui_calibration
import gui_exit

class AdvancedMenuPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'A0'

        title_area = common.TitleArea(self, "Advanced Menu")
        title_area.pack(fill=tk.X)

        tk.Button(self, text="Site Config", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(SiteConfigPage)).pack()

        tk.Button(self, text="Edit Schedule", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(EditSchedulePage)).pack()

        tk.Button(self, text="More", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(MoreAdvancedMenuPage)).pack()

class MoreAdvancedMenuPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'A0'

        title_area = common.TitleArea(self, "Advanced Menu")
        title_area.pack(fill=tk.X)

        tk.Button(self, text="Config Modules", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(ModuleConfigPage)).pack()

        tk.Button(self, text="Calibration", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(gui_calibration.CalibrationPage)).pack()

        tk.Button(self, text="Exit", font=common.Fonts.BUTTON,
                  command=lambda: controller.show_frame(gui_exit.ExitOptionsPage)).pack()




class SiteConfigPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'A1'
        self.back = AdvancedMenuPage

        title_area = common.TitleArea(self, "Site Config")
        title_area.pack(fill=tk.X)

        self.site_var = tk.StringVar()
        self.site_var.set(controller.config.site_name)
        site_frame = tk.Frame(self, bg=common.Colors.BG)
        tk.Label(site_frame, text="Site: ", font=common.Fonts.LABEL).pack(side=tk.LEFT)
        tk.Label(site_frame, textvariable=self.site_var, font=common.Fonts.LABEL).pack(side=tk.LEFT)
        tk.Button(site_frame, text="Edit", image=common.Icons.edit,
                  command=lambda: controller.show_frame(EditSitePage)).pack(side=tk.LEFT, padx=15)
        site_frame.pack(pady=10)

        self.uc_code_var = tk.StringVar()
        self.uc_code_var.set(controller.config.uc_code)
        uc_code_frame = tk.Frame(self, bg=common.Colors.BG)
        tk.Label(uc_code_frame, text="UC code: ", font=common.Fonts.LABEL).pack(side=tk.LEFT)
        tk.Label(uc_code_frame, textvariable=self.uc_code_var, font=common.Fonts.LABEL).pack(side=tk.LEFT)
        tk.Button(uc_code_frame, text="Edit", image=common.Icons.edit,
                  command=lambda: controller.show_frame(EditUcCodePage)).pack(side=tk.LEFT, padx=15)
        uc_code_frame.pack(pady=10)
        
        
    def reset(self):
        self.site_var.set(self.controller.config.site_name)
        self.uc_code_var.set(self.controller.config.uc_code)


class EditSitePage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'A0'
        self.back = SiteConfigPage

        title_area = common.TitleArea(self, "Edit Site")
        title_area.pack(fill=tk.X)

        self.site_var = tk.StringVar()
        self.site_var.set(controller.config.site_name)
        self.site_var.trace("w", self._site_name_changed)
        allowed_chars = 'abcdefghijklmnopqrstuvwxyz0123456789'.upper()
        self.text_frame = common.TextControl(self, self.site_var, allowed_chars)
        self.text_frame.pack(pady=20)

        self.error_var = tk.StringVar()
        error_label = tk.Label(self, textvariable=self.error_var, font=common.Fonts.ERROR, fg="Red", wraplength=600)
        error_label.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)


    def reset(self):
        self.site_var.set(self.controller.config.site_name)
        self.text_frame.reset()

    def save(self):
        value = self.site_var.get()
        if self._validate(value):
            old_value = self.controller.config.site_name
            self.controller.config.site_name = value
            self.controller.save_configuration(True, "Updated site name from '%s' to '%s'." % (old_value, value))
            self.controller.show_frame(SiteConfigPage)

    def cancel(self):
        self.controller.show_frame(SiteConfigPage)

    def _site_name_changed(self, *args):
        """Used to clear the error message."""
        self.error_var.set("")

    def _validate(self, value):
        if '-' in value:
            self.error_var.set("Must be five characters and no '-'.")
            return False

        return True


class EditUcCodePage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'A0'
        self.back = SiteConfigPage

        title_area = common.TitleArea(self, "Edit UC Code")
        title_area.pack(fill=tk.X)

        self.uc_code_var = tk.StringVar()
        self.uc_code_var.set(controller.config.uc_code)
        self.uc_code_var.trace("w", self._uc_code_changed)
        allowed_chars = '0123456789'
        self.text_frame = common.TextControl(self, self.uc_code_var, allowed_chars)
        self.text_frame.pack(pady=20)

        self.error_var = tk.StringVar()
        error_label = tk.Label(self, textvariable=self.error_var, font=common.Fonts.ERROR, fg="Red", wraplength=600)
        error_label.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)


    def reset(self):
        self.uc_code_var.set(self.controller.config.uc_code)
        self.text_frame.reset()

    def save(self):
        value = self.uc_code_var.get()
        if self._validate(value):
            old_value = self.controller.config.uc_code
            self.controller.config.uc_code = value
            self.controller.save_configuration(False, "Updated UC code from '%s' to '%s'." % (old_value, value))
            self.controller.show_frame(SiteConfigPage)

    def cancel(self):
        self.controller.show_frame(SiteConfigPage)

    def _uc_code_changed(self, *args):
        """Used to clear the error message."""
        self.error_var.set("")

    def _validate(self, value):
        if '-' in value:
            self.error_var.set("Must be five characters and no '-'.")
            return False

        return True


class EditSchedulePage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'S0'
        self.back = AdvancedMenuPage

        title_area = common.TitleArea(self, "Edit Schedule")
        title_area.pack(fill=tk.X)

        change_day_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(change_day_frame, text="Change day: ", font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT)
        
        self.change_day_list = ["Tuesday", "Thursday"]
        self.change_day_var = tk.StringVar()
        self.change_day_var.set(self.change_day_list[0])
        option = tk.OptionMenu(change_day_frame, self.change_day_var, *self.change_day_list)
        option.config(font=common.Fonts.OPTIONMENU)
        option['menu'].config(font=common.Fonts.OPTIONMENU)
        option.pack(side=tk.LEFT)

        schedule_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(schedule_frame, text="Schedule: ", font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT)

        self.schedule_list = ["2-3-2", "3-2-2"]
        self.schedule_var = tk.StringVar()
        self.schedule_var.set(self.schedule_list[0])
        option = tk.OptionMenu(schedule_frame, self.schedule_var, *self.schedule_list)
        option.config(font=common.Fonts.OPTIONMENU)
        option['menu'].config(font=common.Fonts.OPTIONMENU)
        option.pack(side=tk.LEFT)


        change_day_frame.pack(pady=(20,10))
        schedule_frame.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)

        self.offset_count = {"2-3-2": {"Tuesday": 0, "Thursday": 3},
                             "3-2-2": {"Tuesday": 2, "Thursday": 0}
                            }


    def reset(self):
        schedule = self.controller.config.ebox.filter_cartridge_schedule
        if schedule in self.schedule_list:
            self.schedule_var.set(schedule)
        else:
            self.schedule_var.set(self.schedule_list[0])
            
        change_day = self.controller.config.ebox.cartridge_change_day
        if change_day in self.change_day_list:
            self.change_day_var.set(change_day)
        else:
            self.change_day_var.set(self.change_day_list[0])

    def save(self):
        schedule = self.schedule_var.get()
        old_schedule = self.controller.config.ebox.filter_cartridge_schedule
        self.controller.config.ebox.filter_cartridge_schedule = schedule
        change_day = self.change_day_var.get()
        old_change_day = self.controller.config.ebox.cartridge_change_day
        self.controller.config.ebox.cartridge_change_day = change_day
        cycle_offset = self.offset_count[schedule][change_day]
        self.controller.config.ebox.sampling_cycle_offset_count = str(cycle_offset)
        msg = "Updated schedule from '%s' to '%s' and/or change day from '%s' to '%s'." % (old_schedule, schedule, old_change_day, change_day)
        self.controller.save_configuration(True, msg)
        self.controller.show_frame(AdvancedMenuPage)

    def cancel(self):
        self.controller.show_frame(AdvancedMenuPage)


class ModuleConfigPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'IM'

        self.module_index = 0
        self.modules = []

        title = 'Module %d' % self.module_index
        self.title_area = common.TitleArea(self, title, self.next_page, self.previous_page)
        self.title_area.pack(fill=tk.X)

        self.online_var = tk.StringVar()
        self.last_pressure_time_var = tk.StringVar()

        info_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(info_frame, textvariable=self.online_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=0, sticky=tk.E, padx=10)
        label = tk.Label(info_frame, textvariable=self.last_pressure_time_var, font=common.Fonts.SMALL_LABEL)
        label.grid(row=0, column=1, sticky=tk.W)
        info_frame.pack()

        self.module_enabled_var = tk.IntVar()
        cb = tk.Checkbutton(self, text='Enabled:', variable=self.module_enabled_var,
                            activebackground=common.Colors.BG,
                            bg=common.Colors.BG, font=common.Fonts.BUTTON, indicatoron=0, padx=10,
                            image=common.Icons.checkbox_outline,
                            selectimage=common.Icons.checkbox_checked, compound=tk.RIGHT,
                            command=self._update_module_enabled)
        cb.pack()

        self.module_types = ["A", "B", "C", "D", "E", "F", "X", "-"]
        self.module_types_index = 0
        
        type_frame = tk.Frame(self, bg=common.Colors.BG)
        label = tk.Label(type_frame, text="Type:", font=common.Fonts.LABEL)
        label.pack(side=tk.LEFT, padx=10)
        self.type_var = tk.StringVar()
        entry = tk.Entry(type_frame, textvariable=self.type_var, font=common.Fonts.ENTRY, state="readonly", width=2)
        entry.pack(side=tk.LEFT)
        button = tk.Button(type_frame, text="+", image=common.Icons.up_arrow, command=self.next_type)
        button.pack(side=tk.LEFT)
        button = tk.Button(type_frame, text="-", image=common.Icons.down_arrow, command=self.previous_type)
        button.pack(side=tk.LEFT)
        type_frame.pack()

    def reset(self):
        self.module_index = 0
        self.modules = self.controller.ipc.get_data('list modules all')
        self._update_title()
        self._update_from_config()
        self.update()

    def update(self):
        module_number = 1
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]

        data = self.controller.ipc.get_data('module %d list' % module_number)
        if True == data['online']:
            self.online_var.set('Online: ')
        else:
            self.online_var.set('Offline: ')
        self.last_pressure_time_var.set('{0:%x} {0:%X}'.format(data['last_pressure_time']))

    def _update_from_config(self):
        module_number = 1
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]
            
        self.module_config = self._get_module_config(module_number)
        if self.module_config is None:
            return
        
        if self.module_config.module_enabled:
            self.module_enabled_var.set(1)
        else:
            self.module_enabled_var.set(0)
        
        type_ = self.module_config.module_type
        for self.module_types_index in range(len(self.module_types)):
            if type_ == self.module_types[self.module_types_index]:
                break;
        # if no match found it will be set to the last entry which is "-"
        self.type_var.set(self.module_types[self.module_types_index])
        
    def next_page(self):
        if (self.module_index + 1) < len(self.modules):
            self.module_index += 1
            self._update_title()
            self._update_from_config()

    def previous_page(self):
        if self.module_index == 0:
            pass
        else:
            self.module_index -= 1
            self._update_title()
            self._update_from_config()

    def _update_title(self):
        title = 'Module X'
        if self.module_index < len(self.modules):
            module_number = self.modules[self.module_index]
            title = 'Module %d' % module_number
        self.title_area.update_title(title)

    def _get_module_config(self, module_number):
        for module in self.controller.config.modules:
            if module.module_position == module_number:
                return module
        return None

    def _update_module_enabled(self):
        if 1 == self.module_enabled_var.get():
            self.module_config.module_enabled = True
        else:
            self.module_config.module_enabled = False
        value = self.module_config.module_enabled
        msg = "Updated module %d ModuleEnabled to %s" % (self.module_config.module_position, str(value))
        self.controller.save_configuration(True, msg)

    def next_type(self):
        self.module_types_index += 1
        if len(self.module_types) <= self.module_types_index:
            self.module_types_index = 0
        self._update_type()

    def previous_type(self):
        self.module_types_index -= 1
        if self.module_types_index < 0:
            self.module_types_index = len(self.module_types) - 1
        self._update_type()

    def _update_type(self):
        type_ = self.module_types[self.module_types_index]
        self.type_var.set(type_)
        old_type = self.module_config.module_type
        self.module_config.module_type = type_
        msg = "Updated module %d ModuleType from '%s' to '%s'." % (self.module_config.module_position, old_type, type_)
        self.controller.save_configuration(False, msg)
        
