import tkinter as tk
from tkinter import ttk
import sys

import datetime

from gui_frame import PageFrame
import gui_common as common
import gui_ipc as ipc
import gui_datetime
import gui_operator_initials as gui_opinit
import gui_advanced_menu
import gui_config_file
import gui_info_pages as info_pages
import gui_filter_change as filter_pages
import gui_calibration
import gui_exit


class MainWindow(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.option_add('*Label*Background', common.Colors.BG)
        self.config(bg=common.Colors.BG)

        common.Icons.initialize()
        self.initialize_data()
        self.ipc = ipc.Ipc()
        self.ipc.initialize()
    
        toolbar = self.build_toolbar()
        toolbar.pack(side=tk.LEFT, fill=tk.Y, padx=(0,6))

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage,PageOne,PageTwo, SettingsPage, MenuPage,
                  gui_datetime.DateTimePage, gui_datetime.EditDatePage, gui_datetime.EditTimePage,
                  gui_opinit.OpInitialsPage, gui_opinit.AddOpInitialsPage, gui_opinit.ChooseInitialsPage,
                  gui_opinit.ChooseCustomInitialsPage,
                  gui_advanced_menu.AdvancedMenuPage, gui_advanced_menu.MoreAdvancedMenuPage,
                  gui_advanced_menu.SiteConfigPage, gui_advanced_menu.ModuleConfigPage,
                  gui_advanced_menu.EditSitePage, gui_advanced_menu.EditUcCodePage,
                  gui_advanced_menu.EditSchedulePage,
                  info_pages.SiteInfoPage, info_pages.ModuleInfoPage, info_pages.ControllerInfoPage,
                  info_pages.MainMenuInfoPage,
                  filter_pages.FilterChangeOperatorPage, filter_pages.FilterChangeCustomOperatorPage,
                  filter_pages.ExposedTestRunPage, filter_pages.ExposedTestResultsPage,
                  filter_pages.FinalProblemsPage, filter_pages.ExposedTestCheckLogsheetPage,
                  filter_pages.ExposedTestCheckLogsheetProblemsPage,
                  filter_pages.ChangeFilterInstructionsPage, filter_pages.CleanTestRunPage,
                  filter_pages.CleanTestResultsPage, filter_pages.InitialProblemsPage,
                  filter_pages.CleanTestCheckLogsheetPage, filter_pages.CleanTestCheckLogsheetProblemsPage,
                  filter_pages.ChangeFlashcardPage, filter_pages.ChangeFlashcardCheckPage,
                  gui_calibration.CalibrationPage,
                  gui_exit.ExitOptionsPage, gui_exit.ExitShutdownPage,
                  gui_exit.ExitAppPage, gui_exit.ExitToDesktopPage
                  ):
            frame = F(container, self)
            frame.enable_home_button = self.enable_home_button
            frame.enable_back_button = self.enable_back_button
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky='nsew')

        self.active_frame = self.frames[StartPage]
        self.show_frame(StartPage)
        self.update_frame()

    def show_frame(self, next_frame, previous_frame=None):
        # by default go back to the start page
        if next_frame is None:
            next_frame = StartPage
        # find the key for the previous page
        previous_key = None
        for (key, frame) in self.frames.items():
            if frame == self.active_frame:
                previous_key = key
                break
        # switch to the new frame
        self.active_frame = self.frames[next_frame]
        self.page_number_var.set(str(self.active_frame.page_number))
        # if we wanted to override where back went, set it here
        if previous_frame is not None:
            self.active_frame.back = previous_frame
        # if there is no back frame set, use the previous page
        if self.active_frame.back is None:
            self.active_frame.back = previous_key
        # if there is a reset function for setting up the new page, call it
        if 'reset' in dir(self.active_frame):
            self.active_frame.reset()
        self.active_frame.tkraise()

    def update_frame(self):
        if 'update' in dir(self.active_frame):
            self.active_frame.update()
        self.after(500, self.update_frame)

    def enable_back_button(self, state=True):
        if state:
            self.back_button['state'] = tk.NORMAL
        else:
            self.back_button['state'] = tk.DISABLED
        
    def navigate_back(self):
        if self.active_frame.back is None:
            self.show_frame(StartPage)
        else:
            self.show_frame(self.active_frame.back)

    def enable_home_button(self, state=True):
        if state:
            self.home_button['state'] = tk.NORMAL
        else:
            self.home_button['state'] = tk.DISABLED

    def navigate_home(self):
        self.show_frame(StartPage)

    def build_toolbar(self):

        toolbar = tk.Frame(self, bd=1, relief=tk.RAISED, bg=common.Colors.BACKGROUND_GRAY)
        
        self.home_button = tk.Button(toolbar, text="Hm", command=self.navigate_home, image=common.Icons.home)
        self.home_button.pack(side=tk.TOP)
        self.back_button = tk.Button(toolbar, text="<=", command=self.navigate_back, image=common.Icons.back)
        self.back_button.pack(side=tk.TOP)

        self.problem_button = tk.Button(toolbar, text="pb", command=self.navigate_home, image=common.Icons.problem,
                                        state=tk.DISABLED)
        self.problem_button.pack(side=tk.TOP)

        #exit_button = tk.Button(toolbar, text="X", command=self.quit, image=common.Icons.close)
        #exit_button.pack(side=tk.BOTTOM)

        help_button = tk.Button(toolbar, text="?", font=common.Fonts.BUTTON,
                                #, command=lambda: controller.show_frame(help_pages.ExplainUIPage)
                                image=common.Icons.info)
        help_button.pack(side=tk.BOTTOM)

        self.page_number_var = tk.StringVar()
        self.page_number_var.set('~~')
        page_label = tk.Label(toolbar, textvariable=self.page_number_var, font=common.LARGE_FONT,
                              bg=common.Colors.BACKGROUND_GRAY)
        page_label.pack(side=tk.BOTTOM)

        return toolbar

    def get_operator_initials(self):
        return self.operator_initials

    def initialize_data(self):
        
        self.config_file_path = "../config.ini"
        self.config = gui_config_file.ConfigFile()
        self.config.load(self.config_file_path)

    def save_configuration(self, restart_eboxes=True, message=None):
        self.config.save(self.config_file_path)
        #send message to backend here
        cmd = "controller configuration"
        if restart_eboxes:
            cmd += " restart needed"
        if not message is None:
            cmd += " %s" % message
        self.ipc.send_command(cmd)
        

class StartPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'Page:\nHOME1'

        self.datetime_var = tk.StringVar()
        self.datetime_var.set('')
        label = tk.Label(self, textvariable=self.datetime_var, font=common.Fonts.LABEL)
        label.pack()

        subframe = tk.Frame(self, bg=common.Colors.BG)

        self.site_name_var = tk.StringVar()
        self.site_name_var.set(self.controller.config.site_name)
        label = tk.Label(subframe, textvariable=self.site_name_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=15)

        self.temperature_var = tk.StringVar()
        self.temperature_var.set('')
        label = tk.Label(subframe, textvariable=self.temperature_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=10)

        self.temperature_mv_var = tk.StringVar()
        self.temperature_mv_var.set('')
        label = tk.Label(subframe, textvariable=self.temperature_mv_var, font=common.Fonts.SMALL_LABEL)
        label.pack(side=tk.LEFT, padx=5)

        subframe.pack()

        self.week_var = tk.StringVar()
        self.week_var.set('')
        label = tk.Label(self, textvariable=self.week_var, font=common.Fonts.LABEL)
        label.pack()

        self.status_var = tk.StringVar()
        self.status_var.set('')
        label = tk.Label(self, textvariable=self.status_var, font=common.Fonts.LABEL)
        label.pack()

        button = tk.Button(self, text="Filter Readings", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(filter_pages.FilterChangeOperatorPage))
        button.pack(pady=10)
        button_frame = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Help", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(info_pages.MainMenuInfoPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="Info", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(info_pages.SiteInfoPage))
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(button_frame, text="Menu", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(MenuPage))
        button.pack(side=tk.LEFT, padx=10)
        button_frame.pack()

    def update(self):
        now = datetime.datetime.now()
        self.datetime_var.set("{0:%x} {0:%X}".format(now))

        modules = self.controller.ipc.get_data('list modules')
        module = []
        for module_number in modules:
            module_data = self.controller.ipc.get_data('module %d list' % module_number)
            if module_data['online']:
                module = module_data
                break
        else:
            self.week_var.set('--')
            self.status_var.set('All Offline')
            return

        schedule = module['ebox_schedule']
        
        week_number = schedule['cartridge_schedule_index'] + 1
        self.week_var.set("Sampling Week %d" % week_number)

        if schedule['locked_until_cartridge_change']:
            status = "Change filters"
        elif schedule['sampling_active']:
            status = "Running"
        else:
            status = "Idle"
        self.status_var.set("%s" % status)

        temperature = module['last_temperature_probe_reading']
        temperature_mv = module['last_temperature_probe_reading_mv']
        
        self.temperature_var.set('%0.2f C' % temperature)
        self.temperature_mv_var.set('%0.2f mV' % temperature_mv)

    def next_page(self):
        self.controller.show_frame(PageOne)

    def reset(self):
        self.site_name_var.set(self.controller.config.site_name)

class MenuPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)
        self.page_number = 'Page:\nMENU1'
        self.back = StartPage

        title_area = common.TitleArea(self, "Menu", None, None)
        title_area.pack(fill=tk.X)

        button = tk.Button(self, text="Settings", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(SettingsPage))
        button.pack()
        button = tk.Button(self, text="Quick Checks", font=common.Fonts.BUTTON)
        button.pack()
        button = tk.Button(self, text="Equipment Change", font=common.Fonts.BUTTON)
        button.pack()
        button = tk.Button(self, text="Advanced Menu", font=common.Fonts.BUTTON,
                            command=lambda: controller.show_frame(gui_advanced_menu.AdvancedMenuPage, MenuPage))
        button.pack()

class SettingsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)
        self.page_number = "Page:\nCNFG1"
        self.back = MenuPage

        title_area = common.TitleArea(self, "Settings", None, None)
        title_area.pack(fill=tk.X)

        button = tk.Button(self, text="Date and Time", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(gui_datetime.DateTimePage))
        button.pack()
        button = tk.Button(self, text="Operator Initials", font=common.Fonts.BUTTON,
                           command=lambda: controller.show_frame(gui_opinit.OpInitialsPage))
        button.pack()
        

class PageOne(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.back = StartPage
        self.page_number = 'Page:\nHOME2'

        title_area = common.TitleArea(self, "One Page", self.next_page, self.previous_page)
        title_area.pack(fill=tk.X)

        date_label = tk.Label(self, text="Date:", font=common.Fonts.LABEL)
        date_label.pack()
        self.date_var = tk.StringVar()
        date_field = tk.Label(self, textvariable=self.date_var, font=common.Fonts.LABEL)
        date_field.pack()
        time_label = tk.Label(self, text="Time:", font=common.Fonts.LABEL)
        time_label.pack()
        self.time_var = tk.StringVar()
        time_field = tk.Label(self, textvariable=self.time_var, font=common.Fonts.LABEL)
        time_field.pack()

        self.update()

    def update(self):
        now = datetime.datetime.now()
        self.date_var.set(now.strftime("%x"))
        self.time_var.set("{0:%X}".format(now))

    def next_page(self):
        self.controller.show_frame(PageTwo)

    def previous_page(self):
        self.controller.show_frame(StartPage)



class PageTwo(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.back = PageOne
        self.page_number = 'Page:\nHOME3'

        title_area = common.TitleArea(self, "Two Page", None, self.previous_page)
        title_area.pack(fill=tk.X)


    def next_page(self):
        pass
    def previous_page(self):
        self.controller.show_frame(PageOne)
        

if __name__ == "__main__":
    app = MainWindow()
    if '--full' in sys.argv:
        # http://stackoverflow.com/a/15982406/76010
        app.attributes('-fullscreen', True)
    else:
        app.wm_geometry("800x480")
    app.mainloop()
