import configparser
import datetime

class ConfigFile(object):
    def __init__(self):
        pass

    def load(self, file_path):
        self.config = configparser.ConfigParser()
        # by default configparser converts everything to lower case,
        #  setting optionxform overrides that
        self.config.optionxform = str
        
        #check if file exists here
        
        # load config file
        self.config.read(file_path)

        self.controller_config = self.config["Controller"]
        
        ebox_config = self.config["ModuleDefaults"]
        self.ebox = EboxConfig(ebox_config)

        # filter the list of config sections to only have the modules
        sections = [section for section in self.config.sections() if self.config[section]["Type"] == "Module"]
        # then load the config for each section
        self.modules = [ModuleConfig(self.config[section]) for section in sections]

    def save(self, file_path):
        # check that path exists here

        # save configuration file (this will overwrite any existing file)
        with open(file_path, "w") as config_file:
            self.config.write(config_file)

    @property
    def site_name(self):
        return self.controller_config.get("SiteName", "-----")
    @site_name.setter
    def site_name(self, value):
        self.controller_config["SiteName"] = value

    @property
    def uc_code(self):
        return self.controller_config.get("UcCode", "----")
    @uc_code.setter
    def uc_code(self, value):
        self.controller_config["UcCode"] = value

    @property
    def operator_initials(self):
        value = self.controller_config.get("OperatorInitials", "")
        # convert the comma delimited string into a list
        list_ = [x.strip() for x in value.split(',')]
        # remove any blank entries
        list_ = list(filter(None, list_))
        return list_
    @operator_initials.setter
    def operator_initials(self, value):
        # check if list, if so turn into comma delimited string
        if isinstance(value, list):
            value = ','.join(value)
        self.controller_config["OperatorInitials"] = value
        


class EboxConfig(object):
    def __init__(self, config_file):
        self.config = config_file

    @property
    def sampling_cycle_offset_count(self):
        return self.config.get("SamplingCycleOffsetCount", 0)
    @sampling_cycle_offset_count.setter
    def sampling_cycle_offset_count(self, value):
        self.config["SamplingCycleOffsetCount"] = value

    @property
    def filter_cartridge_schedule(self):
        return self.config.get("FilterCartridgeSchedule", "2-3-2")
    @filter_cartridge_schedule.setter
    def filter_cartridge_schedule(self, value):
        self.config["FilterCartridgeSchedule"] = value

    @property
    def cartridge_change_day(self):
        return self.config.get("CartridgeChangeDay", "Tuesday")
    @cartridge_change_day.setter
    def cartridge_change_day(self, value):
        self.config["CartridgeChangeDay"] = value

    # this is a helper property to make conversion easier.
    #   datetime.weekday() returns the day as an int with Monday as 0 to Sunday as 6
    #   https://docs.python.org/3.4/library/datetime.html#datetime.datetime.weekday
    @property
    def cartridge_change_day_weekday(self):
        day_name = self.cartridge_change_day
        day_to_number = {"Monday": 0, "Tuesday": 1, "Wednesday": 2,
                         "Thursday": 3, "Friday": 4, "Saturday": 5,
                         "Sunday": 6}
        return day_to_number[day_name]
    
    
class ModuleConfig(object):
    def __init__(self, config_file):
        self.config = config_file

    @property
    def module_position(self):
        return int(self.config.get("ModulePosition", 0))

    @property
    def bus_address(self):
        return int(self.config.get("BusAddress", 0x00))

    @property
    def module_enabled(self):
        return self.config.getboolean("ModuleEnabled", True)
    @module_enabled.setter
    def module_enabled(self, value):
        self.config["ModuleEnabled"] = str(value)

    @property
    def module_type(self):
        return self.config.get("ModuleType", "-")
    @module_type.setter
    def module_type(self, value):
        self.config["ModuleType"] = value
