
import tkinter as tk


LARGE_FONT = ("Verdana", 24)

class Colors(object):
    BACKGROUND_GRAY = '#d9d9d9'
    BG = 'white'

class Fonts(object):

    ENTRY = ("Courier 10 Pitch", 68)
    BUTTON = ("Courier 10 Pitch", 48)
    TITLE = ("Courier 10 Pitch", 48, "underline")
    LABEL = ("Courier 10 Pitch", 48)
    SMALL_LABEL = ("Courier 10 Pitch", 24)
    SMALL_LABEL2 = ("Courier 10 Pitch", 23, "bold")
    ERROR = ("Courier 10 Pitch", 24, "bold")
    LISTBOX = ("Courier 10 Pitch", 48)
    OPTIONMENU = ("Courier 10 Pitch", 48)
    SMALLER_LABEL = ("Courier 10 Pitch", 20, "bold")

class Icons(object):

    back_arrow = None
    forward_arrow = None
    up_arrow = None
    down_arrow = None

    check = None
    cancel = None
    back = None
    close = None
    
    home = None
    info = None
    problem = None
    refresh = None

    edit = None
    add = None
    remove = None

    checkbox_checked = None
    checkbox_outline = None

    def __init__(self):
        pass

    @classmethod
    def initialize(cls):
        cls.back_arrow = tk.PhotoImage(file="images/ic_keyboard_arrow_left_black_48dp.png")
        cls.forward_arrow = tk.PhotoImage(file="images/ic_keyboard_arrow_right_black_48dp.png")
        cls.up_arrow = tk.PhotoImage(file="images/ic_keyboard_arrow_up_black_48dp.png")
        cls.down_arrow = tk.PhotoImage(file="images/ic_keyboard_arrow_down_black_48dp.png")

        cls.check = tk.PhotoImage(file="images/ic_check_black_48dp.png")
        cls.cancel = tk.PhotoImage(file="images/ic_cancel_black_48dp.png")
        cls.back = tk.PhotoImage(file="images/ic_arrow_back_black_48dp.png")
        cls.close = tk.PhotoImage(file="images/ic_close_black_48dp.png")
        
        cls.home = tk.PhotoImage(file="images/ic_home_black_48dp.png")
        cls.info = tk.PhotoImage(file="images/ic_info_black_48dp.png")
        cls.problem = tk.PhotoImage(file="images/ic_report_problem_black_48dp.png")
        cls.refresh = tk.PhotoImage(file="images/ic_refresh_black_48dp.png")

        cls.edit = tk.PhotoImage(file="images/ic_mode_edit_black_48dp.png")
        cls.add = tk.PhotoImage(file="images/ic_add_black_48dp.png")
        cls.remove = tk.PhotoImage(file="images/ic_remove_black_48dp.png")

        cls.checkbox_checked = tk.PhotoImage(file="images/ic_check_box_black_48dp.png")
        cls.checkbox_outline = tk.PhotoImage(file="images/ic_check_box_outline_blank_black_48dp.png")


class TitleArea(tk.Frame):
    def __init__(self, parent, title, next_=None, previous=None):
        tk.Frame.__init__(self, parent)

        self.config(bg=Colors.BG)

        if previous is None:
            pass  # adjust title centering here?
        else:
            self.previous_button = tk.Button(self, text="<=", image=Icons.back_arrow, command=previous)
            self.previous_button.grid(column=0, row=0)

        self.title_var = tk.StringVar()
        self.title_var.set(title)
        label = tk.Label(self, textvariable=self.title_var, font=Fonts.TITLE)
        label.grid(column=1, row=0)

        if next_ is None:
            pass  # adjust title centering here?
        else:
            self.forward_button = tk.Button(self, text="=>", image=Icons.forward_arrow, command=next_)
            self.forward_button.grid(column=2, row=0)

        self.grid_columnconfigure(1, weight=1)

    def update_title(self, title):
        self.title_var.set(title)


class TextControl(tk.Frame):
    def __init__(self, parent, stringvar, allowed_chars):
        tk.Frame.__init__(self, parent)

        self.config(bg=Colors.BG)

        self.variable = stringvar
        self.allowed_chars = allowed_chars
    
        width = len(self.variable.get()) + 1
        self.textbox = tk.Entry(self, textvariable=self.variable, font=Fonts.ENTRY, state="readonly", width=width)
        self.textbox.pack(side=tk.LEFT)
        
        button = tk.Button(self, text="<", command=self.move_left, image=Icons.back_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(self, text="+", command=self.raise_char, image=Icons.up_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(self, text="-", command=self.lower_char, image=Icons.down_arrow)
        button.pack(side=tk.LEFT)
        button = tk.Button(self, text=">", command=self.move_right, image=Icons.forward_arrow)
        button.pack(side=tk.LEFT)

        self.reset()

    def reset(self):
        self.selected_index = 0
        self.textbox.selection_clear()
        self.textbox.selection_range(self.selected_index, self.selected_index + 1)

    def move_left(self):
        self.selected_index -= 1
        if self.selected_index < 0:
            self.selected_index = 0
        self.textbox.selection_clear()
        self.textbox.selection_range(self.selected_index, self.selected_index + 1)

    def move_right(self):
        self.selected_index += 1
        length = len(self.variable.get())
        if length <= self.selected_index:
            self.selected_index = length - 1
        self.textbox.selection_clear()
        self.textbox.selection_range(self.selected_index, self.selected_index + 1)

    def raise_char(self):
        text = list(self.variable.get())
        value = text[self.selected_index]
        index = self.allowed_chars.find(value)
        index += 1
        if len(self.allowed_chars) <= index:
            index = 0
        value = self.allowed_chars[index:index+1]
        text[self.selected_index] = value
        self.variable.set(''.join(text))

    def lower_char(self):
        text = list(self.variable.get())
        value = text[self.selected_index]
        index = self.allowed_chars.find(value)
        index -= 1
        if index < 0:
            index = len(self.allowed_chars) - 1
        value = self.allowed_chars[index:index+1]
        text[self.selected_index] = value
        self.variable.set(''.join(text))
