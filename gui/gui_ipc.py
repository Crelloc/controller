# need to add parent directory for includes
import os, sys
sys.path.insert(0,os.path.pardir)

from multiprocessing.connection import Client
# need to add this to correctly unpickle the data coming from the backend
from ebox import SolenoidPosition 

class Ipc(object):
    def __init__(self):
        pass

    def initialize(self):
        ipc_address = ('localhost', 6000)
        self.ipc_connection = Client(ipc_address, authkey=b'secret password')

    def send_command(self, command):
        self.ipc_connection.send(command)

    def get_data(self, command):
        self.send_command(command)
        response = self.ipc_connection.recv()
        return response
