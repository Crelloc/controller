import tkinter as tk
import sys

from gui_frame import PageFrame
import gui_common as common

class ProblemsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "P0"

        self.title_area = common.TitleArea(self, "Problem", None, None)
        self.title_area.pack(fill=tk.X)

        self.message_frame = tk.Frame(self, bg=common.Colors.BG)
        self._fill_message_frame()
        self.message_frame.pack()

    def _fill_message_frame(self):
        frame = self.message_frame
        label = tk.Label(frame, text="Problem detected.", font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(frame, text="Please call", font=common.Fonts.SMALL_LABEL)
        label.pack()
        label = tk.Label(frame, text="530-752-1123", font=common.Fonts.LABEL)
        label.pack()
        label = tk.Label(frame, text="for assistance.", font=common.Fonts.SMALL_LABEL)
        label.pack()
