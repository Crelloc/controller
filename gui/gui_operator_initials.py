import tkinter as tk
import sys

from gui_frame import PageFrame
import gui_common as common

class OpInitialsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = 'I0'

        title_area = common.TitleArea(self, "Operator Initials")
        title_area.pack(fill=tk.X)

        control_frame = tk.Frame(self, bg=common.Colors.BG)

        listbox_frame = tk.Frame(control_frame)
        scrollbar = tk.Scrollbar(listbox_frame, orient=tk.VERTICAL, width=96)
        self.listbox = tk.Listbox(listbox_frame, selectmode=tk.SINGLE, exportselection=0,
                                  font=common.Fonts.LISTBOX, yscrollcommand=scrollbar.set
                                  )
        scrollbar.config(command=self.listbox.yview)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        button_frame = tk.Frame(control_frame, bg=common.Colors.BG)
        button = tk.Button(button_frame, text="Add", command=lambda: controller.show_frame(AddOpInitialsPage),
                           font=common.Fonts.BUTTON, image=common.Icons.add, compound=tk.LEFT)
        button.pack(side=tk.TOP, pady=10, fill=tk.X)
        button = tk.Button(button_frame, text="Remove", command=self._remove_selected,
                           font=common.Fonts.BUTTON, image=common.Icons.remove, compound=tk.LEFT)
        button.pack(side=tk.TOP, pady=10)
        
        button_frame.pack(side=tk.RIGHT, padx=5)
        listbox_frame.pack(side=tk.LEFT, fill=tk.BOTH)
        control_frame.pack()

        self._load_listbox()

    def reset(self):
        # reload list of initials here
        self._load_listbox()
        pass

    def _load_listbox(self):
        initials = self.controller.config.operator_initials

        self.listbox.delete(0, tk.END)
        for item in initials:
            self.listbox.insert(tk.END, item)

    def _remove_selected(self):
        data = self.controller.config.operator_initials
        selected_items = map(int, self.listbox.curselection())
        selected_items = [data[index] for index in selected_items]
        for item in selected_items:
            data.remove(item)
            # need to assign it back to update the config file property
            self.controller.config.operator_initials = data
            self.controller.save_configuration(False)
        self._load_listbox()


class AddOpInitialsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "I1"
        self.back = OpInitialsPage

        title_area = common.TitleArea(self, "Add Op Initials")
        title_area.pack(fill=tk.X)

        self.initials_var = tk.StringVar()
        self.initials_var.set('---')
        self.initials_var.trace("w", self._initials_changed)
        allowed_chars = 'abcdefghijklmnopqrstuvwxyz'.upper()
        self.text_frame = common.TextControl(self, self.initials_var, allowed_chars)
        self.text_frame.pack(pady=20)

        self.error_var = tk.StringVar()
        error_label = tk.Label(self, textvariable=self.error_var, font=common.Fonts.ERROR, fg="Red", wraplength=600)
        error_label.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)

    def reset(self):
        self.initials_var.set("---")
        self.text_frame.reset()

    def save(self):
        value = self.initials_var.get()
        if self._validate(value):
            data = self.controller.config.operator_initials
            data.append(value)
            data.sort()
            # we have to assign it back to set the config file field
            self.controller.config.operator_initials = data
            self.controller.save_configuration(False)
            self.controller.show_frame(OpInitialsPage)

    def cancel(self):
        self.controller.show_frame(OpInitialsPage)

    def _initials_changed(self, *args):
        """Used to clear the error message."""
        self.error_var.set("")

    def _validate(self, value):
        if '-' in value:
            self.error_var.set("Must be three characters and no '-'.")
            return False
        if value in self.controller.config.operator_initials:
            self.error_var.set("'%s' is already in the operator initials list." % value)
            return False

        return True

class ChooseInitialsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "I7"
        self.back = OpInitialsPage

        self.title_area = common.TitleArea(self, "Choose Operator", self._next_initials_page, self._previous_initials_page)
        self.title_area.pack(fill=tk.X)

        self.initials_grid = tk.Frame(self, bg=common.Colors.BG)
        self.custom_initials_button = tk.Button(self.initials_grid, text="Custom", font=common.Fonts.BUTTON,
                                                width=7, command=self._enter_custom_initials)
        self.custom_initials_button.grid(row=0, column=0)
        self.initials_grid.pack()

        self.page_index = 0
        self.initials_per_page = 6
        
        self.next_page = None
        self.custom_initials_page = None

    def reset(self):
        self.page_index = 0
        self._update_page()

    def _update_page(self):
        children = self.initials_grid.grid_slaves()
        for child in children:
            child.grid_forget()

        initials = self.controller.config.operator_initials
        start = 0 + (self.page_index * self.initials_per_page)
        end = start + self.initials_per_page
        self.selected_initials = initials[start:end]
        
        row = 0
        column = 0
        for initial in self.selected_initials:
            button = tk.Button(self.initials_grid, text=initial, font=common.Fonts.BUTTON, width=7,
                               command=lambda operator=initial: self._select_operator(operator)
                               )
            button.grid(row=row, column=column, padx=5, pady=5)
            column += 1
            if column == 2:
                column = 0
                row += 1
        if len(self.selected_initials) < self.initials_per_page:
            self.custom_initials_button.grid(row=row, column=column, padx=5, pady=5)

    def _next_initials_page(self):
        if len(self.selected_initials) == self.initials_per_page:
            self.page_index += 1
        self._update_page()

    def _previous_initials_page(self):
        self.page_index -= 1
        if self.page_index < 0:
            self.page_index = 0
        self._update_page()

    def _select_operator(self, operator):
        self.controller.current_operator = operator
        self.controller.ipc.send_command("set operator %s" % operator)
        if self.next_page is None:
            print("No next page defined")
            self.controller.show_frame(None)
            return
        self.controller.show_frame(self.next_page)

    def _enter_custom_initials(self):
        if self.custom_initials_page is None:
            print("No custom initials page defined")
            self.controller.show_frame(None)
            return
        self.controller.show_frame(self.custom_initials_page)

class ChooseCustomInitialsPage(PageFrame):
    def __init__(self, parent, controller):
        PageFrame.__init__(self, parent, controller)

        self.page_number = "I1"
        self.back = OpInitialsPage

        title_area = common.TitleArea(self, "Add Op Initials")
        title_area.pack(fill=tk.X)

        self.initials_var = tk.StringVar()
        self.initials_var.set('---')
        self.initials_var.trace("w", self._initials_changed)
        allowed_chars = 'abcdefghijklmnopqrstuvwxyz0123456789'.upper()
        self.text_frame = common.TextControl(self, self.initials_var, allowed_chars)
        self.text_frame.pack(pady=20)

        self.error_var = tk.StringVar()
        error_label = tk.Label(self, textvariable=self.error_var, font=common.Fonts.ERROR, fg="Red", wraplength=600)
        error_label.pack()

        submitframe = tk.Frame(self, bg=common.Colors.BG)
        button = tk.Button(submitframe, text="Cancel", font=common.Fonts.BUTTON, command=self.cancel,
                           image=common.Icons.cancel, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        button = tk.Button(submitframe, text="Save", font=common.Fonts.BUTTON, command=self.save,
                           image=common.Icons.check, compound=tk.LEFT)
        button.pack(side=tk.LEFT, padx=10)
        submitframe.pack(side=tk.BOTTOM)

    def reset(self):
        self.initials_var.set("---")
        self.text_frame.reset()

    def save(self):
        value = self.initials_var.get()
        if not self._validate(value):
            return
        # check if initials are already in list, if not add them so they will be there next time.
        if not (value in self.controller.config.operator_initials):
            data = self.controller.config.operator_initials
            data.append(value)
            data.sort()
            # we have to assign it back to set the config file field
            self.controller.config.operator_initials = data
            self.controller.save_configuration(False)

        self.controller.current_operator = value
        self.controller.ipc.send_command("set operator %s" % value)
        if self.next_page is None:
            print("No next page defined")
            self.controller.show_frame(None)
            return
        self.controller.show_frame(self.next_page)
        #self.controller.show_frame(OpInitialsPage)

    def cancel(self):
        self.controller.show_frame(OpInitialsPage)

    def _initials_changed(self, *args):
        """Used to clear the error message."""
        self.error_var.set("")

    def _validate(self, value):
        if '-' in value:
            self.error_var.set("Must be three characters and no '-'.")
            return False

        return True
