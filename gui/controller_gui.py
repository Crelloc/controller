# need to add parent directory for includes
import os, sys
sys.path.insert(0,os.path.pardir)

import socket, fcntl, struct
import tkinter as tk
import tkinter.font as tkfont
from time import sleep
from multiprocessing.connection import Client
from ebox import SolenoidPosition, EboxState

class ModuleInfo(tk.Frame):
    def __init__(self, parent, module_list, ipc_connection):
        tk.Frame.__init__(self, parent)

        self.module_list = module_list
        self.ipc_connection = ipc_connection
        self.module_data = {}

        self.selected_module = tk.IntVar(self)
        self.selected_module.set(module_list[0])
        tk.Label(self, text='Module:').grid(row=0, column=0, padx=10)
        tk.OptionMenu(self, self.selected_module, *module_list).grid(row=0, column=1, pady=5)

        ip = self._get_ip_address()
        self.ip_address_label = tk.Label(self, text=ip)
        self.ip_address_label.grid(row=0, column=2)

        self.columnconfigure(2, weight=1)

        tk.Button(self, text='X', command=quit).grid(row=0, column=3)

        self.canvas = tk.Canvas(self, borderwidth=0, background='#ff0000')
        self.canvas.grid(row=1, column=0, columnspan=3, sticky='nsew')
        self.canvas_frame = tk.Frame(self.canvas, background='#0000ff')
        self.canvas.create_window((0,0), window=self.canvas_frame, anchor='nw', tags='self.canvas_frame')

        self.scrollbar = tk.Scrollbar(self, orient='vertical', width=48)
        self.scrollbar.grid(row=1, column=3, sticky='ns')
        self.canvas.configure(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.canvas.yview)

        self.rowconfigure(1, weight=1)

        self.canvas_frame.bind("<Configure>", self._on_frame_configure)

        self.prop_vars = {}
        module_data = self._get_module_data(self.selected_module.get())
        self._build_ui_property_list(module_data, self.canvas_frame)

    def _on_frame_configure(self, event):
        """Reset the scroll region to encompass the inner frame."""
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def _get_ip_address(self):
        interfaces = [
            'eth0',
            'wlan0',
            'wifi0'
            ]
        for ifname in interfaces:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                var = struct.pack('256s', bytes(ifname[:15], 'utf-8'))
                ip = socket.inet_ntoa(fcntl.ioctl(
                    s.fileno(),
                    0x8915, # SIOCGIDADDR
                    var
                    )[20:24])
                print(ifname)
                print(ip)
                return ip
            except IOError:
                pass

    def _get_module_data(self, selected_module):
        #print('get data for %d' % selected_module)
        self.ipc_connection.send('module %d list' % selected_module)
        module_data = self.ipc_connection.recv()
        return module_data

    def _send_command(self, cmd):
        self.ipc_connection.send(cmd)

    def _turn_on_pump(self):
        pump_number = self.selected_module.get()
        cmd = 'pump %d on' % pump_number
        self._send_command(cmd)

    def _turn_off_pump(self):
        pump_number = self.selected_module.get()
        cmd = 'pump %d off' % pump_number
        self._send_command(cmd)

    def _command_solenoid(self, solenoid_number, action):
        module_number = self.selected_module.get()
        cmd = 'module %d solenoid %d %s' % (module_number, solenoid_number, action)
        self._send_command(cmd)

    def _command_change_cartridge(self):
        module_number = self.selected_module.get()
        cmd = 'module %d change cartridge' % module_number
        self._send_command(cmd)

    def _build_ui_property_list(self, module_data, parent_frame):
        special_properties = ['solenoids', 'ebox_schedule', 'online', 'last_cyclone_pressure',
                              'last_orifice_pressure', 'last_current_reading', 'last_pressure_time',
                              'pump_state', 'locked_until_cartridge_change'
                              ]
        schedule_data = module_data['ebox_schedule']
        row = 0
        # we want some properties at the top
        prop = 'online'
        self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
        row += 1
        prop = 'last_pressure_time'
        self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
        row += 1
        prop = 'last_cyclone_pressure'
        self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
        row += 1
        prop = 'last_orifice_pressure'
        self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
        row += 1
        prop = 'last_current_reading'
        self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
        row += 1
        prop = 'locked_until_cartridge_change'
        value = schedule_data[prop]
        prop_name = 'schd_' + prop
        self._build_ui_add_property(parent_frame, row, prop_name, value)
        row += 1
        tk.Button(parent_frame, text='change cartridge',
                  command=self._command_change_cartridge
                  ).grid(row=row, column=1, sticky=tk.W, padx=5, pady=2)
        row += 1
        
        
        # solenoids:
        solenoids = module_data['solenoids']
        for index in range(4):
            prop = 'solenoid_%d' % (index + 1)
            value = solenoids[index].name
            self._build_ui_add_property(parent_frame, row, prop, value)
            row += 1
            tk.Button(parent_frame, text=prop + ' open',
                      command=lambda solenoid_number=(index+1): self._command_solenoid(solenoid_number, 'on')
                      ).grid(row=row, column=0, sticky=tk.E, padx=5, pady=2)
            tk.Button(parent_frame, text=prop + ' close',
                      command=lambda solenoid_number=(index+1): self._command_solenoid(solenoid_number, 'off')
                      ).grid(row=row, column=1, sticky=tk.W, padx=5, pady=2)
            row += 1

        prop = 'pump_state'
        self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
        row += 1
        tk.Button(parent_frame, text='turn on pump', command=self._turn_on_pump).grid(row=row, column=0, sticky=tk.E, padx=5, pady=2)
        tk.Button(parent_frame, text='turn off pump', command=self._turn_off_pump).grid(row=row, column=1, sticky=tk.W, padx=5, pady=2)
        row += 1


        # handle all of the normal properties
        for prop in sorted(module_data):
            # skip the special properties
            if prop in special_properties: continue
            # add the properties to the list
            self._build_ui_add_property(parent_frame, row, prop, module_data[prop])
            row += 1

        # schedule:
        for prop in sorted(schedule_data):
            # skip the special properties
            if prop in special_properties: continue
            # alter the name to avoid name conflicts and add the properties to the list
            prop_name = 'schd_' + str(prop)
            value = str(schedule_data[prop])
            self._build_ui_add_property(parent_frame, row, prop_name, value)
            row += 1

    def _build_ui_add_property(self, parent_frame, row, prop, value):
        prop_name = str(prop)
        if 20 < len(prop_name):
            prop_name = prop_name.replace('cartridge', 'ctg')
        if 20 < len(prop_name):
            prop_name = prop_name.replace('schedule', 'sched')
        if 20 < len(prop_name):
            prop_name = prop_name.replace('average', 'ave')
            prop_name = prop_name.replace('averaging', 'ave')
        if 20 < len(prop_name):
            prop_name = prop_name.replace('temperature', 'temp')
        if 24 < len(prop_name):
            prop_name = '%.22s...' % prop_name
        tk.Label(parent_frame, text=prop_name).grid(row=row, column=0, sticky=tk.E)
        variable = tk.StringVar()
        variable.set(str(value))
        tk.Label(parent_frame, textvariable=variable).grid(row=row, column=1, sticky=tk.W, padx=5, pady=2)
        self.prop_vars[prop] = variable

    def update_ui(self):
        special_properties = ['solenoids', 'ebox_schedule']
        module_data = self._get_module_data(self.selected_module.get())
        for prop in module_data:
            # skip the special properties, they will be handled below
            if prop in special_properties: continue
            # process the normal properties
            if prop in self.prop_vars:
                self.prop_vars[prop].set(self._format_field(module_data[prop]))
        # solenoids
        solenoids = module_data['solenoids']
        for index in range(4):
            prop = 'solenoid_%d' % (index + 1)
            value = solenoids[index].name
            if prop in self.prop_vars:
                self.prop_vars[prop].set(value)
        # schedule
        ebox_schedule = module_data['ebox_schedule']
        for prop in sorted(ebox_schedule):
            prop_name = 'schd_' + str(prop)
            value = self._format_field(ebox_schedule[prop])
            if prop_name in self.prop_vars:
                self.prop_vars[prop_name].set(value)

    def _format_field(self, value):
        if type(value) == float:
            return '{:0.6f}'.format(value)
        elif type(value) == list:
            string = '['
            for item in value:
                string += '%s,' % self._format_field(item)
            return string + ']'
        else:    
            return str(value)


class MainWindow(tk.Tk):
    def __init__(self, args=[]):
        tk.Tk.__init__(self)

        self._init_geometry(args)

        ipc_address = ('localhost', 6000)
        ipc_connection = Client(ipc_address, authkey=b'secret password')
        print('connected')

        print('list modules')
        ipc_connection.send('list modules')
        module_list = ipc_connection.recv()
        print(module_list)

        default_font = tkfont.nametofont("TkDefaultFont")
        default_font.configure(size=24)
        self.option_add('*Font', default_font)

        self.frame = ModuleInfo(self, module_list, ipc_connection)
        self.frame.config(background='#00ff00', height=200, width=200)
        self.frame.grid(row=0, column=0, sticky='nsew')
        

        # start background update:
        self.after(1000, self._update)

    def _init_geometry(self, args):
        if '--full' in args:
            # http://stackoverflow.com/a/15982406/76010
            self.attributes('-fullscreen', True)
        else:
            self.wm_geometry("800x480+100+100")

    def _update(self):
        self.frame.update_ui()
        self.after(1000, self._update)


if __name__ == "__main__":    

    win = MainWindow(sys.argv)
    win.columnconfigure(0, weight=1)
    win.rowconfigure(0, weight=1)
    win.mainloop()    
