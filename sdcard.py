import os
import subprocess
from enum import Enum

class SdcardState(Enum):
    NULL = 0
    ERROR = -1
    READY = 1
    INITIALIZING = 2

class Sdcard(object):
    def __init__(self, config):
        self.config = config
        self.status = ''
        self.state = SdcardState.NULL

    def available(self):
        mnt_dir = self.config.sdcard_mount_directory
        mmc_bus = self.config.sdcard_hardware_bus
        # see what device is attached to mmc2:
        # > ls -l /sys/block/ | grep mmc2
        raw_output = subprocess.getoutput('ls -l /sys/block')
        mmc_lines = [line for line in raw_output.splitlines() if mmc_bus in line]
        if len(mmc_lines) < 1:
            print(mmc_lines)
            self.status = 'block device (SD card) for %s not found' % mmc_bus
            print(self.status)
            self.state = SdcardState.ERROR
            return False
        elif 1 < len(mmc_lines):
            # more than one found (don't know what to do)
            print(mmc_lines)
            self.status = "more than one block device found associated with %s:" % mmc_bus
            print(self.status)
            self.state = SdcardState.ERROR
        device_line = mmc_lines[0]
        block_device = mmc_lines[0].split('/')[-1:]
        if 0 < len(block_device):
            block_device = block_device[0]
        else:
            print(device_line)
            print(block_device)
            self.status = 'error getting block device'
            print(self.status)
            self.state = SdcardState.ERROR
            return False
        
        # see what partitions are on that device:
        # > cat /proc/partitions | grep mmcblk1
        # list device partition size and if mounted where mounted:
        # > lsblk | grep mmcblk1
        
        # check mnt to see if device mounted
        raw_output = subprocess.getoutput('mount')
        device_lines = [line for line in raw_output.splitlines() if (block_device in line and mnt_dir in line)]
        
        if len(device_lines) < 1:
            # need to mount the device?
            print(device_lines)
            self.status = "device not mounted"
            print(self.status)
            self.state = SdcardState.ERROR
            return False

        self.status = ''
        self.state = SdcardState.READY
        return True

    def unmount(self):
        if not self.available():
            return

        cmd = 'umount ' + self.config.sdcard_mount_directory
        raw_output = subprocess.getoutput(cmd)
        print(raw_output)

        self.status = 'sdcard unmounted'
        self.state = SdcardState.NULL
