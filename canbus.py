import socket
import struct

class CanSocket(object):
    def __init__(self):
        self.s = socket.socket(socket.AF_CAN, socket.SOCK_RAW, socket.CAN_RAW)
        self.s.bind(("can0",))

    def receive(self):
        frame = CanFrame()
        cf, addr = self.s.recvfrom(16)
        frame.unpack(cf)
        return frame

    def send(self, canFrame):
        # need more error handling here to recover from errors.
        try:
            self.s.send(canFrame.pack())
        # [Errno 105] No buffer space available
        except OSError as ex:
            print("Received and ate OSError exception:")
            print(ex)

class CanFrame(object):
    # CAN frame packing/unpacking (see 'struct can_frame' in <linux/can.h>)
    can_frame_fmt = "=IB3x8s"

    def __init__(self):
        self.can_id = 0
        self.can_dlc = 0
        self.data = bytearray(8)

    def pack(self): 
        self.can_dlc = len(self.data)
        self.data = self.data.ljust(8, b'\x00')
        return struct.pack(self.can_frame_fmt, self.can_id, self.can_dlc, self.data)

    def unpack(self, packedFrame):
        self.can_id, self.can_dlc, self.data = struct.unpack(self.can_frame_fmt, packedFrame)
        

    def hexDataString(self):
        return ''.join(format(x, '02x') for x in self.data)

    def initCmdFrame(self, dest_id, cmd):
        self.data = bytearray(8)
        self.data[0] = dest_id
        self.data[1] = cmd

    #------------ CAN command constants--------------
    CAN_COMMAND_INVALID = 0x00

    # debugging commands
    # just parrot back the data
    CAN_COMMAND_REPEAT = 0x01
    # data[0] = GPIO #, data[1] = GPIO state {0 = off, 1 = on, 2 = toggle}
    CAN_COMMAND_GPIO = 0x02
    #
    CAN_COMMAND_REPORT_VERSION = 0x03

    # Set Solenoid: data[0] = Solenoid #, data[1] = on(1)/off(0)
    CAN_COMMAND_SET_SOLENOID = 0x10
    # Raise/Lower Motor: data[0] = reserved, data[1] = direction
    CAN_COMMAND_MOVE_MOTOR = 0x11
    # report the current measured in the current sensing chip (24V current)
    CAN_COMMAND_REPORT_CURRENT = 0x12
    # : data[0-3] = solenoid on(1)/off(0), data[4] = motor  on down(2)/on up(1)/$
    CAN_COMMAND_REPORT_POSITIONS = 0x13

    # set the time data = timestamp
    CAN_COMMAND_SET_TIME = 0x20
    # get the time data = timestamp
    CAN_COMMAND_REPORT_TIME = 0x21
    # set the date: data[0] = 
    CAN_COMMAND_SET_DATE = 0x22
    #
    CAN_COMMAND_REPORT_DATE = 0x23
    #
    CAN_COMMAND_SET_SAMPLE_RATE = 0x24
    #
    CAN_COMMAND_SET_AVERAGING_DEPTH = 0x25

    # get the pressure reading for the two pressure sensors
    # data[0-3] = cyclone, data[4-7] = orifice
    CAN_COMMAND_REPORT_PRESSURE = 0x30
    #
    CAN_COMMAND_REPORT_TEMPERATURE = 0x31
    # data[0-3] = cyclone, data[4-6] = orifice
    CAN_COMMAND_REPORT_RAW_PRESSURE = 0x32
    #
    CAN_COMMAND_REPORT_RAW_TEMPERATURE = 0x33

    #
    CAN_COMMAND_REPORT_I2C_STATS_CYCLONE = 0x40
    #
    CAN_COMMAND_REPORT_I2C_STATS_ORIFICE = 0x41
    #
    CAN_COMMAND_REPORT_I2C_STATS_CURRENT = 0x42
    # data[0] = stale data: yes(1)/no(0), data[1-2]: success count, data[3-4]: stale count, data[5-6]: fail count
    CAN_COMMAND_REPORT_PRESSURESENSOR_STATS_CYCLONE = 0x43
    #
    CAN_COMMAND_REPORT_PRESSURESENSOR_STATS_ORIFICE = 0x44
    

def CanQueueItem(Object):
    def __init__(self, canFrame, waitForReply, timeout):
        self.canFrame = canFrame
        self.waitForReply = waitForReply
        self.timeout = timeout
        self.frameSent = False
        self.noResponse = False
        


