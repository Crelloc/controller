#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <signal.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/time.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <errno.h>
#include <time.h>

#define CAN_MTU sizeof(struct can_frame)
#define MESSAGE_MAX_LENGTH 256

typedef enum
{
    CAN_COMMAND_INVALID = 0x00,

            // debugging commands
            // just parrot back the data
            CAN_COMMAND_REPEAT = 0x01,
            // data[0] = GPIO #, data[1] = GPIO state {0 = off, 1 = on, 2 = toggle}
            CAN_COMMAND_GPIO = 0x02,

            // Set Solenoid: data[0] = Solenoid #, data[1] = on(1)/off(0)
            CAN_COMMAND_SET_SOLENOID = 0x10,
            // Raise/Lower Motor: data[0] = reserved, data[1] = direction
            CAN_COMMAND_MOVE_MOTOR = 0x11,
            // report the current measured in the current sensing chip (24V current)
            CAN_COMMAND_REPORT_CURRENT = 0x12,
            // : data[0-3] = solenoid on(1)/off(0), data[4] = motor  on down(2)/on up(1)/off(0), data[5] = motor position down(0)/up(1)/in transit(2)
            CAN_COMMAND_REPORT_POSITIONS = 0x13,

            // set the time data = timestamp
            CAN_COMMAND_SET_TIME = 0x20,
            // get the time data = timestamp
            CAN_COMMAND_REPORT_TIME = 0x21,
            //
            CAN_COMMAND_SET_SAMPLE_RATE = 0x22,
            //
            CAN_COMMAND_SET_AVERAGING_DEPTH = 0x23,

            // get the pressure reading for the two pressure sensors
            // data[0-3] = cyclone, data[4-7] = orifice
            CAN_COMMAND_REPORT_PRESSURE = 0x30,
            //
            CAN_COMMAND_REPORT_TEMPERATURE = 0x31,
            // data[0-3] = cyclone, data[4-6] = orifice
            CAN_COMMAND_REPORT_RAW_PRESSURE = 0x32,
            //
            CAN_COMMAND_REPORT_RAW_TEMPERATURE = 0x33,

            //
            CAN_COMMAND_REPORT_I2C_STATS = 0x40,
            // data[0] = stale data: yes(1)/no(0), data[1-2]: success count, data[3-4]: stale count, data[5-6]: fail count
            CAN_COMMAND_REPORT_PRESSURESENSOR_STATS = 0x41,


}CAN_COMMAND;

static volatile int running = 1;

void sigterm(int signo)
{
	running = 0;

//	exit(1);
}

unsigned char asc2nibble(char c) {

    if ((c >= '0') && (c <= '9'))
        return c - '0';

    if ((c >= 'A') && (c <= 'F'))
        return c - 'A' + 10;

    if ((c >= 'a') && (c <= 'f'))
        return c - 'a' + 10;

    return 16; /* error */
}

int CreateSocket(struct ifreq *ifr, struct sockaddr_can *addr)
{
        int s; /* can raw socket */

        if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
                perror("socket");
                return -1;
        }

        addr->can_family = AF_CAN;

        if (ioctl(s, SIOCGIFINDEX, ifr) < 0) {
                perror("SIOCGIFINDEX");
                return -1;
        }
        addr->can_ifindex = ifr->ifr_ifindex;

        if (bind(s, (struct sockaddr *)addr, sizeof(*addr)) < 0) {
                perror("bind");
                return -1;
        }

        return s;
}

/* Options for direction are POLLIN or POLLOUT */
int WaitForReady(int socket, int direction)
{
	int rv;
	struct pollfd ufds;

	//while(running) {

		ufds.fd = socket;
		ufds.events = direction;

		/* Timeout every 100ms */
		rv = poll(&ufds, 1, 100);

		if (-1 == rv) {
			perror("poll");
			return -1;
		} else if (0 == rv) {
			//printf("\nTimeout occurred\n");
			/* keep looping */
		} else if (ufds.revents & direction) {
			//printf(".");
			return 0;
		} else if (ufds.revents & POLLERR) {
			printf("!");
		}
	//}

	/* only way this should be reached is if running == 0 */
	printf("\nTimed out waiting for socket to be ready\n");
	return -1;
}

int SendFrame(int socket, struct can_frame* frame)
{
        int rc;
        int code;
        int count;

        if (0 != WaitForReady(socket, POLLOUT)) {
		return -1;
	}

        if (!running) {
                return -1;
        }

        rc = write(socket, frame, CAN_MTU);

        if (rc != CAN_MTU) {
                if (ENOBUFS != errno) {
                        printf("err: %u %u\n", errno);
                }
                code = errno;
                count = 1000;
                while (ENOBUFS == code && 0 < count) {
			putc('-', stdout);
			usleep(100);

                        code = 0;
                        rc = write(socket, frame, CAN_MTU);
                        if (rc != CAN_MTU) {
                                code = errno;
                        }
                        count--;
                }
        }

        return rc;
}


int ReadFrame(int socket, struct can_frame *frame)
{
	struct sockaddr_can addr;
	socklen_t addrLength = sizeof(addr);
	size_t nbytes;

	if (0 != WaitForReady(socket, POLLIN)) {
		return -1;
	}

	if (!running) {
		return -1;
	}

	nbytes = recvfrom(socket, frame, sizeof(struct can_frame), 0, (struct sockaddr*)&addr, &addrLength);

	if (nbytes < 0) {
		perror("read");
		return -1;
	}
	else if (nbytes < CAN_MTU) {
		fprintf(stderr, "read: incomplete CAN frame\n");
		return -1;
	}

	return nbytes;
}

void InitFrame(can_frame* frame, unsigned int can_id, unsigned char dest_id, unsigned char cmd)
{
	frame->can_id = can_id;
	frame->data[0] = dest_id;
	frame->data[1] = cmd;
	frame->data[2] = 0x00;
	frame->data[3] = 0x00;
	frame->data[4] = 0x00;
	frame->data[5] = 0x00;
	frame->data[6] = 0x00;
	frame->data[7] = 0x00;
	frame->can_dlc = 8;
}

int TestLoop(int socket, size_t loopCount, int src_addr, int dst_addr)
{
        struct can_frame src, rcv;
        int nbytes;

	InitFrame(&src, src_addr, dst_addr, CAN_COMMAND_REPEAT);
	src.data[2] = 0x00;
	src.can_dlc = 8;

	int dlcOffCount = 0;
	int dataOffCount = 0;

	printf("\nStarting loop (%d)\n", loopCount);

	for (int i = 0; i < loopCount; i++)
	{
		if (SendFrame(socket, &src) != CAN_MTU) {
	                perror("write");
	                return -1;
        	}

		nbytes = ReadFrame(socket, &rcv);

		if (-1 == nbytes) {
			printf("loop failed\n");
			return -1;
		}
		else if (nbytes < CAN_MTU) {
			/* error happened */
			perror("read");
			return -1;
		}
		

		if (src.can_dlc != rcv.can_dlc)
		{
			printf("!");
			dlcOffCount++;
		}
		for (int j = 0; j < src.can_dlc; j++)
		{
			if (src.data[j] != rcv.data[j])
			{
				printf("~");
				dataOffCount++;
			}
		}

		// increment the count
		src.data[6] = src.data[7] == 0xFF ? src.data[6] + 1 : src.data[6];
		src.data[7] = src.data[7] == 0xFF ? 0 : src.data[7] + 1;
	}

	if (dlcOffCount || dataOffCount)
	{
		printf("\ndlc didn't match %d times and data didn't match %d times\n", dlcOffCount, dataOffCount);
	}
	
	return 0;
}

void SetSolenoid(int socket, int solenoid, int isActive, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_SET_SOLENOID);
	frame.data[2] = solenoid;
	frame.data[3] = isActive;
	frame.can_dlc = 4;

	SendFrame(socket, &frame);
}

void GetPositions(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_POSITIONS);
	frame.data[2] = 0;
	frame.can_dlc = 3;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	printf("Positions: \t");
	printf(" S 1: ");
	if (frame.data[1])
		printf("on");
	else
		printf("off");
	printf("\t S 2: ");
	if (frame.data[2])
		printf("on");
	else
		printf("off");
	printf("\t S 3: ");
	if (frame.data[3])
		printf("on");
	else
		printf("off");
	printf("\t S 4: ");
	if (frame.data[4])
		printf("on");
	else
		printf("off");

	printf("\n");
}

void GetCurrent(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	int stale, status, rawCurrent, averagePoints, gainSetting, faultSetting;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_CURRENT);

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	stale = frame.data[1];
	status = frame.data[2];
	rawCurrent = (frame.data[3] << 8) + frame.data[4];

	averagePoints = frame.data[5];
	gainSetting = frame.data[6];
	faultSetting = frame.data[7];

	printf("Current: \t");
	printf("status: %x\t", status);
	printf("current: %d (%x)\t", rawCurrent, rawCurrent);
	printf("avgPts: %x, gain: %x, fault: %x ", averagePoints, gainSetting, faultSetting);

	printf("\n");

}

void SetTime(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_SET_TIME);
	frame.data[2] = 0x00;
	frame.data[3] = 0x00;
	frame.can_dlc = 4;

	SendFrame(socket, &frame);
}

int GetI2cStats(int socket, int src_addr, int dst_addr, int source, int type)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_I2C_STATS);
	frame.data[2] = source;
	frame.data[3] = type;
	frame.can_dlc = 4;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	int stale = frame.data[1];
	int number1 = (frame.data[2] << 8) + frame.data[3];
	int number2 = (frame.data[4] << 8) + frame.data[5];
	int number3 = (frame.data[6] << 8) + frame.data[7];

	if (source == 0x00)
		printf("Cyclone I2C Stats: "); 
	else
		printf("Orifice I2C Stats: ");

	if (type == 0x00)
		printf(" \t Success: %d, LoopCount: %d, Fail: %d\n", number1, number2, number3);
	else if (type == 0x01)
		printf(" \t Loop max: %d, Loop min: %d, Fail max: %d\n", number1, number2, number3);
	else 
		printf(" \t Loop ave: %d, Fail since last checked: %d\n", number2, number3);

	return 0;
}

int GetPressureSensorStats(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_PRESSURESENSOR_STATS);
	frame.data[2] = 0x00;
	frame.can_dlc = 3;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	int successCount = (frame.data[2] << 8) + frame.data[3];
	int staleCount = (frame.data[4] << 8) + frame.data[5];
	int failCount = (frame.data[6] << 8) + frame.data[7];

	printf("Pressure Sensor Stats: \t Success: %d, Stale: %d, Fail: %d\n", successCount, staleCount, failCount);

	return 0;
}


inline double CalculatePressure(int rawPressure, int pressureMax, int pressureMin)
{
    int outputMax = 0x3999;
    int outputMin = 0x0666;
    int output = rawPressure;

    double pressure = (output - outputMin);
    pressure *= (pressureMax - pressureMin);
    pressure /= (outputMax - outputMin);
    pressure += pressureMin;

    return pressure;
}

int GetPressure(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_PRESSURE);
	frame.data[2] = 0x00;
	frame.can_dlc = 3;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	int adjustedCycloneReading = (frame.data[1] << 16) + (frame.data[2] << 8) + frame.data[3];
	int adjustedOrificeReading = (frame.data[4] << 16) + (frame.data[5] << 8) + frame.data[6];

	double cycloneReading = (double)adjustedCycloneReading / 1000.0;
	double orificeReading = (double)adjustedOrificeReading / 1000.0;

	printf("Pressure: \t Cyclone: %f,\t\t Orifice: %f\n", cycloneReading, orificeReading);

	return 0;
}

int GetRawPressure(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_RAW_PRESSURE);
	frame.data[2] = 0x00;
	frame.can_dlc = 3;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	int rawCycloneReading = (frame.data[1] << 8) + frame.data[2];
	int rawOrificeReading = (frame.data[3] << 8) + frame.data[4];

	double cycloneReading = CalculatePressure(rawCycloneReading, 1, -1);
	double orificeReading = CalculatePressure(rawOrificeReading, 15, 0);

	printf("Pressure: \t Cyclone: %f (%x),\t Orifice: %f (%x)\n", cycloneReading, rawCycloneReading, orificeReading, rawOrificeReading);

	return 0;
}

int GetTemperature(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_TEMPERATURE);
	frame.data[2] = 0x00;
	frame.can_dlc = 3;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	int adjustedCycloneReading = (frame.data[1] << 16) + (frame.data[2] << 8) + frame.data[3];
	int adjustedOrificeReading = (frame.data[4] << 16) + (frame.data[5] << 8) + frame.data[6];

	double cycloneReading = (double)adjustedCycloneReading / 1000.0;
	double orificeReading = (double)adjustedOrificeReading / 1000.0;

	printf("Temperature: \t Cyclone: %f,\t\t Orifice: %f\n", cycloneReading, orificeReading);

	return 0;
}

int GetRawTemperature(int socket, int src_addr, int dst_addr)
{
	can_frame frame;
	int nbytes;

	InitFrame(&frame, src_addr, dst_addr, CAN_COMMAND_REPORT_RAW_TEMPERATURE);
	frame.data[2] = 0x00;
	frame.can_dlc = 3;

	SendFrame(socket, &frame);
	nbytes = ReadFrame(socket, &frame);

	int rawCycloneReading = (frame.data[1] << 8) + frame.data[2];
	int rawOrificeReading = (frame.data[3] << 8) + frame.data[4];

	double cycloneReading = (((double)rawCycloneReading / 2047.0) * 200.0) - 50.0;
	double orificeReading = (((double)rawOrificeReading / 2047.0) * 200.0) - 50.0;

	printf("Temperature: \t Cyclone: %f (%x),\t Orifice: %f (%x)\n", cycloneReading, rawCycloneReading, orificeReading, rawOrificeReading);

	return 0;
}

int TestModule(int s, int src_addr, int dst_addr)
{
	struct timeval start, end;
	long mtime, seconds, useconds;

	can_frame frame;

	printf("dst addr: %x\n", dst_addr);
	printf("------------------------------------------------------\n");

	// need to clear any left over packets from the previous loop
	while(-1 != ReadFrame(s, &frame))
	{
		printf("leftover frame found\n");
	}

	SetSolenoid(s, 1, 1, src_addr, dst_addr);
	SetSolenoid(s, 2, 0, src_addr, dst_addr);
	SetSolenoid(s, 3, 0, src_addr, dst_addr);
	SetSolenoid(s, 4, 0, src_addr, dst_addr);

	GetPressureSensorStats(s, src_addr, dst_addr);
	GetRawPressure(s, src_addr, dst_addr);
	//GetPressure(s, src_addr, dst_addr);
	GetRawTemperature(s, src_addr, dst_addr);
	//GetTemperature(s, src_addr, dst_addr);

	SetSolenoid(s, 1, 1, src_addr, dst_addr);
	SetSolenoid(s, 2, 0, src_addr, dst_addr);
	//SetSolenoid(s, 3, 1, src_addr, dst_addr);
	//SetSolenoid(s, 4, 0, src_addr, dst_addr);
	GetPositions(s, src_addr, dst_addr);

	if (dst_addr == 0x41)
		GetCurrent(s, src_addr, dst_addr);

	// start of time measurement loop
	gettimeofday(&start, NULL);
	
	int loopCount = 0x4bf * 2;
	if (0 != TestLoop(s, loopCount, src_addr, dst_addr)) {
		printf("test failed\n");
		return -1;
	}
	
	// end of time measurement loop
	gettimeofday(&end, NULL);

	seconds = end.tv_sec - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf(" --- Elapsed time: %ld milliseconds\n", mtime);

	GetPressureSensorStats(s, src_addr, dst_addr);
	//GetRawPressure(s, src_addr, dst_addr);
	//GetPressure(s, src_addr, dst_addr);
	//GetRawTemperature(s, src_addr, dst_addr);
	//GetTemperature(s, src_addr, dst_addr);

	
	GetI2cStats(s, src_addr, dst_addr, 0x00, 0x02);
	GetI2cStats(s, src_addr, dst_addr, 0x00, 0x01);
	GetI2cStats(s, src_addr, dst_addr, 0x01, 0x02);
	GetI2cStats(s, src_addr, dst_addr, 0x01, 0x01);
	

	SetSolenoid(s, 1, 0, src_addr, dst_addr);
	SetSolenoid(s, 2, 1, src_addr, dst_addr);
	//SetSolenoid(s, 3, 0, src_addr, dst_addr);
	//SetSolenoid(s, 4, 1, src_addr, dst_addr);

	printf("\n------------------------------------------------------\n");

	return 0;
}

int main()
{

        int s; /* can raw socket */
        struct sockaddr_can addr;
        struct can_frame src, rcv;
        int nbytes;
        struct ifreq ifr;
	int i;

	struct timeval start, end;
	long mtime, seconds, useconds;
	double hours;

	char message[MESSAGE_MAX_LENGTH];

	/* Handle ctrl-c and other keys */
	signal(SIGTERM, sigterm);
	signal(SIGHUP, sigterm);
	signal(SIGINT, sigterm);

        strcpy(ifr.ifr_name, "can0");

        if ((s = CreateSocket((struct ifreq *)&ifr, (struct sockaddr_can *)&addr)) < 0) {
                return 1;
        }

        printf("\ncan_family: %u, can_ifindex: %i, rx_id: %u, tx_id: %u \n", addr.can_family, addr.can_ifindex, addr.can_addr.tp.rx_id, addr.can_addr.tp.tx_id);

	printf("\n------------------------------------------------------\n");

	const int addressSlots = 6;
	int address[addressSlots];
	for (i = 0; i < addressSlots; i++)
		address[i] = 0;
	double timeout[addressSlots];
	for (i = 0; i < addressSlots; i++)
		timeout[i] = 0;

	address[0] = 0x41;
	address[1] = 0x42;
	address[2] = 0x43;
	address[3] = 0x44;
	address[4] = 0x45;

	int validAddressCount = 0;
	for (i = 0; i < addressSlots; i++)
		if (0 != address[i])
			validAddressCount++;
	int failedAddressCount = 0;

	int src_addr = 0x123;
	int loopCount = 0;

	// start of time measurement loop
	gettimeofday(&start, NULL);

	while (running == 1)
	{
		// end of time measurement loop
		gettimeofday(&end, NULL);

		seconds = end.tv_sec - start.tv_sec;
		useconds = end.tv_usec - start.tv_usec;
		hours = (double)seconds / 60.0 / 60.0; // sixty seconds in a minute, 60 minutes in an hour)

		printf("Loop %d: (h: %f)  [", loopCount, hours);
		for (i = 0; i < addressSlots; i++)
		{
			if (0 != address[i] && 0 != timeout[i])
				printf("{0x%x: %.2f}", address[i], timeout[i]);
		}
		printf("]\n---------------------------------------------\n\n");

		GetI2cStats(s, src_addr, 0x43, 0, 0); 
		GetI2cStats(s, src_addr, 0x43, 1, 0); 
		
		printf("\n---------------------------------------------\n\n");
	
		for (i = 0; i < addressSlots; i++) ;
			SetTime(s, src_addr, address[i]);

		failedAddressCount = 0;
		for (i = 0; i < addressSlots; i++)
		{
			if (0 != address[i] && 0 == timeout[i]) 
			{
				if (0 != TestModule(s, src_addr, address[i]))
				{
					timeout[i] = hours;
					printf("module failed (%.2f)\n", timeout[i]);
					printf("\n------------------------------------------------------\n");
				}
			}
			else if (0 != timeout[i])
			{
				failedAddressCount++;
			}
		}

		loopCount++;	

		if (failedAddressCount == validAddressCount)
		{
			printf("All modules have failed\nExiting...\n");
			break;
		}
	}

	close(s);

	return 0;
}
