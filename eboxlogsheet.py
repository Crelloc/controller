import datetime
import csv
import io

class EboxLogsheetCassetteData(object):
    def __init__(self, sample_date):
        self.sample_date = sample_date
        self.elapsed_sampling_time = datetime.timedelta(seconds=0)

        self.cyclone_pressure_initial = 0
        self.cyclone_pressure_initial_raw = []
        self.cyclone_pressure_final = 0
        self.cyclone_pressure_final_raw = []

        self.orifice_pressure_initial = 0
        self.orifice_pressure_initial_raw = []
        self.orifice_pressure_final = 0
        self.orifice_pressure_final_raw = []

        self.orifice_pressure_min = 1000

        self.moved_from_previous_cartridge = False

    def generate_summary(self, raw_data_array):
        if len(raw_data_array) < 1:
            return 0
        # calculate the average
        total = 0
        for value in raw_data_array:
            total += value
        average = total / len(raw_data_array)
        return average

    def to_dictionary(self):
        properties = ['sample_date', 'elapsed_sampling_time',
                      'cyclone_pressure_initial', 'cyclone_pressure_final',
                      'orifice_pressure_initial', 'orifice_pressure_final',
                      'orifice_pressure_min', 'moved_from_previous_cartridge'
                      ]
        dict_ = {}
        for prop in properties:
            value = getattr(self, prop)
            dict_[prop] = value
        return dict_

class EboxLogsheetData(object):
    def __init__(self, cartridge_schedule_index, sample_dates):
        self.orifice_pressure_min = 1000
        self.status_text = ""
        self.cassette_data = []
        for sample_date in sample_dates:
            self.cassette_data.append(EboxLogsheetCassetteData(sample_date))
        self.initial_date = datetime.datetime.min
        self.final_date = datetime.datetime.min
        self.initial_initials = ""
        self.final_initials = ""
        self.temperature = 0
        self.week_number = cartridge_schedule_index + 1
        self.pump_startup_cyclone_data = []
        self.pump_startup_orifice_data = []
        self.leak_check_cyclone_data = []
        self.leak_check_orifice_data = []
        self.log_file_row_count = 0

    @property
    def cassette_count(self):
        return len(self.cassette_data)

    @property
    def first_sampling_date(self):
        if 0 < len(self.cassette_data):
            return self.cassette_data[0].sample_date
        else:
            return None

    @property
    def elapsed_times(self):
        dict_ = {}
        for cassette in self.cassette_data:
            dict_[cassette.sample_date] = cassette.elapsed_sampling_time
        return dict_

    def to_dictionary(self):
        dict_ = {}
        cassette_data_formatted = []
        for cassette in self.cassette_data:
            cassette_data_formatted.append(cassette.to_dictionary())
        dict_['cassette_data'] = cassette_data_formatted
        dict_['status_text'] = self.status_text
        dict_['orifice_pressure_min'] = self.orifice_pressure_min
        dict_['initial_date'] = self.initial_date
        dict_['final_date'] = self.final_date
        dict_['initial_initials'] = self.initial_initials
        dict_['final_initials'] = self.final_initials
        dict_['temperature'] = self.temperature
        dict_['week'] = self.week_number
        return dict_

    def clear_raw_data_initial(self):
        self.pump_startup_cyclone_data = []
        self.pump_startup_orifice_data = []
        for cassette in self.cassette_data:
            cassette.cyclone_pressure_initial_raw = []
            cassette.orifice_pressure_initial_raw = []
        self.orifice_pressure_min = 1000
        self.leak_check_cyclone_data = []
        self.leak_check_orifice_data = []

    def clear_raw_data_final(self):
        for cassette in self.cassette_data:
            cassette.cyclone_pressure_final_raw = []
            cassette.orifice_pressure_final_raw = []

    def add_pressure_initial(self, cassette_index, cyclone_pressure, orifice_pressure):
        if cassette_index < 0 or len(self.cassette_data) <= cassette_index:
            # out of range so return
            print('!')
            return
        cassette = self.cassette_data[cassette_index]
        cassette.cyclone_pressure_initial_raw.append(cyclone_pressure)
        cassette.orifice_pressure_initial_raw.append(orifice_pressure)
        if orifice_pressure < self.orifice_pressure_min:
            self.orifice_pressure_min = orifice_pressure

    def add_pressure_final(self, cassette_index, cyclone_pressure, orifice_pressure):
        if cassette_index < 0 or len(self.cassette_data) <= cassette_index:
            # out of range so return
            print('!')
            return
        cassette = self.cassette_data[cassette_index]
        cassette.cyclone_pressure_final_raw.append(cyclone_pressure)
        cassette.orifice_pressure_final_raw.append(orifice_pressure)

    def add_pressure_pump_startup(self, cyclone_pressure, orifice_pressure):
        self.pump_startup_cyclone_data.append(cyclone_pressure)
        self.pump_startup_orifice_data.append(orifice_pressure)

    def add_pressure_leak_check(self, cyclone_pressure, orifice_pressure):
        self.leak_check_cyclone_data.append(cyclone_pressure)
        self.leak_check_orifice_data.append(orifice_pressure)
        if orifice_pressure < self.orifice_pressure_min:
            self.orifice_pressure_min = orifice_pressure

    def add_elapsed_time(self, cassette_index, timedelta):
        if cassette_index < 0 or len(self.cassette_data) <= cassette_index:
            # out of range so return
            return
        self.cassette_data[cassette_index].elapsed_sampling_time += timedelta
        
    def generate_summary_final(self):
        for cassette in self.cassette_data:
            cassette.cyclone_pressure_final = cassette.generate_summary(cassette.cyclone_pressure_final_raw)
            cassette.orifice_pressure_final = cassette.generate_summary(cassette.orifice_pressure_final_raw)
        self.final_date = datetime.datetime.now()

    def generate_summary_initial(self):
        for cassette in self.cassette_data:
            cassette.cyclone_pressure_initial = cassette.generate_summary(cassette.cyclone_pressure_initial_raw)
            cassette.orifice_pressure_initial = cassette.generate_summary(cassette.orifice_pressure_initial_raw)
        self.initial_date = datetime.datetime.now()

    def generate_log_filename(self, module_position):
        date = self.first_sampling_date
        date_str = '%04d-%02d-%02d' % (date.year, date.month, date.day)
        filename = 'module%02d-logsheet-%s.log' % (module_position, date_str)
        return filename

    def _write_data_to_csv(self, csv_writer, section_name, start_count, cyclone_data, orifice_data):
        count = start_count
        data_count = len(cyclone_data)
        for data_index in range(data_count):
            cyclone = cyclone_data[data_index]
            orifice = orifice_data[data_index]
            data = [section_name,
                    count,
                    '{:.6f}'.format(cyclone),
                    '{:.6f}'.format(orifice)
                    ]
            csv_writer.writerow(data)
            count += 1
        return count

    def get_data_initial(self):
        row_count = 0
        file_data = ""

        output = io.StringIO()
        writer = csv.writer(output)

        section_name = 'pump startup'
        # pump startup
        row_count = self._write_data_to_csv(writer, section_name, row_count,
                                              self.pump_startup_cyclone_data,
                                              self.pump_startup_orifice_data
                                              )
                    
        # positions
        for cassette in self.cassette_data:
            date_str = cassette.sample_date.strftime('%x %X')
            section_name = '%s initial' % date_str
            row_count = self._write_data_to_csv(writer, section_name, row_count,
                                                  cassette.cyclone_pressure_initial_raw,
                                                  cassette.orifice_pressure_initial_raw
                                                  )

        # leak check
        section_name = 'leak check'
        row_count = self._write_data_to_csv(writer, section_name, row_count,
                                              self.leak_check_cyclone_data,
                                              self.leak_check_orifice_data
                                              )
        file_data = output.getvalue()
        self.log_file_row_count = row_count
        return file_data

    def get_data_final(self):
        row_count = self.log_file_row_count
        file_data = ""

        output = io.StringIO()
        writer = csv.writer(output)

        for cassette in self.cassette_data:
            date_str = cassette.sample_date.strftime('%x %X')
            section_name = '%s final' % date_str
            row_count = self._write_data_to_csv(writer, section_name, row_count,
                                                 cassette.cyclone_pressure_final_raw,
                                                 cassette.orifice_pressure_final_raw
                                                 )
        file_data = output.getvalue()
        self.log_file_row_count = row_count
        return file_data
