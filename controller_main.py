#! /usr/bin/python3

from time import sleep
from enum import Enum
import threading
import queue
import multiprocessing
import select
import ebox
import eboxconfig
import eboxschedule
import controllerconfig
import sdcard
import datetime
import configparser
from canbus import *
from gpio import GPIO
from multiprocessing.connection import Listener, wait
import sys
import os
import subprocess


class ControllerState(Enum):
    INITIALIZING = 0
    INITIALIZING_EBOXES = 10
    INACTIVE_NORMAL = 20
    SAMPLING_NORMAL = 30
    CHANGING_CARTRIDGES = 40
    

modules = {}
messageQueue = multiprocessing.JoinableQueue()
connectionQueue = multiprocessing.JoinableQueue()

class IpcThread(threading.Thread):
    def run(self):
        address = ('localhost', 6000)
        authkey = b'secret password'
        listener = Listener(address,authkey=authkey)

        try:
            while True:
                connection = listener.accept()
                print('IPC connection accepted from %s' % str(listener.last_accepted))
                connectionQueue.put(connection)

        except KeyboardInterrupt:
            exit()
            

class CanbusReceiveThread(threading.Thread):
    def run(self):
        socket = CanSocket()

        print("start listening to CAN")

        try:
            while True:
                frame = socket.receive()
                strData = ''.join(format(x, '02x') for x in frame.data)

                #msg = 'Received: can_id=%x, can_dlc=%x, data=%s' % (frame.can_id, frame.can_dlc, frame.hexDataString())
                #print(msg)

                if not 0x123 == frame.can_id:
                    messageQueue.put(frame)

        except KeyboardInterrupt:
            exit()

class Main(object):
    def __init__(self):
        self.operator_initials = 'xxx'
        self.config = None
        self.active_config_filename = 'active_config.ini'
        self.config_filename = 'config.ini'
        self._state = ControllerState.INITIALIZING
        self.sdcard = None
        self.polling_list = []

    @property
    def state(self):
        return self._state
    @state.setter
    def state(self, value):
        self._state = value
        if self.config is not None:
            self.config.state = value
        
        print(self._state)

    @property
    def schedule_paused(self):
        if self.state == ControllerState.CHANGING_CARTRIDGES:
            return True
        return False
    @schedule_paused.setter
    def schedule_paused(self, value):
        pass

    def load_configuration(self):
        self.state = ControllerState.INITIALIZING_EBOXES
        old_state = None
        
        config = configparser.ConfigParser(default_section='ModuleDefaults')
        config.read(self.config_filename)

        self.active_config = configparser.ConfigParser()
        # by default configparser saves all properties to lower case,
        #  setting optionxform overrides the default and preserves case.
        #  (Don't need to set it for the main section because it is never written back to disk)
        self.active_config.optionxform = str
        self.active_config.read(self.active_config_filename)

        # clear the modules list
        global modules
        modules = {}
        
        for section_name in config.sections():
            section = config[section_name]
            # check if section exists is the active configuration file, if not create it
            if not section_name in self.active_config:
                self.active_config[section_name] = {}
            active_section = self.active_config[section_name]
            if section['Type'] == 'Module':
                module_config = eboxconfig.EboxConfig(section, active_section)
                modules[module_config.address] = ebox.Ebox(module_config)
            elif section['Type'] == 'Controller':
                self.config = controllerconfig.ControllerConfig(section, active_section)
                old_state = self.config.state

        if old_state is None:
            self.state = ControllerState.INACTIVE_NORMAL
        else:
            self.state = old_state
        print(self.config.status_str)

        # needs to be set before trying to log anything
        self.sdcard = sdcard.Sdcard(self.config)

        # can't write to the log file until we know where it is from the config file
        self.log_action("Controller started")
        self.log_action(self.get_version())

    def get_version(self):
        # use the commit count as a easy to read approx version number
        cmd = 'git rev-list HEAD --count'
        value = subprocess.check_output(cmd, shell=True)
        commit_count = value.decode(encoding='UTF-8')
        # get the actual git version number (commit hash)
        cmd = 'git show --pretty="%ad (%H)" -s'
        value = subprocess.check_output(cmd, shell=True)
        version = value.decode(encoding='UTF-8')
        # create a version number string that can be written to the log file
        version_string = 'version %s : %s' % (commit_count.strip(), version.strip())
        return version_string

    def save_active_configuration(self):
        filename = self.active_config_filename
        with open(filename, "w") as config_file:
            self.active_config.write(config_file)

    def run(self):
        self.loop()

    def loop(self):
        self.socket = CanSocket()
        self.connection_list = []
        
        try:
            for moduleId in modules:
                self.setModuleTime(moduleId)

            i = 0
            loop_interval = self.config.main_loop_interval
            next_loop_time = datetime.datetime.now()
            log_interval = self.config.log_file_interval
            next_log_time = datetime.datetime.now() + log_interval
            print(log_interval)
            self.polling_list.append(messageQueue._reader)
            self.polling_list.append(connectionQueue._reader)
            
            while True:
                now = datetime.datetime.now()
                # calculate the remaining time for the select
                remaining_time = next_loop_time - now
                if next_loop_time < now:
                    remaining_time = now - now
                remaining_seconds = remaining_time.seconds + (remaining_time.microseconds / 1000000)
                #print(remaining_seconds)
                    
                # use select so that the main loop can sleep when nothing is happening (frees up cpu for GUI or other tasks)
                #  watches for CAN and IPC messages
                (select_list, [], []) = select.select(self.polling_list, [], [], remaining_seconds)
                if not messageQueue.empty():
                    frame = messageQueue.get()
                    self.processMessage(frame)
                    messageQueue.task_done()

                if not connectionQueue.empty():
                    conn = connectionQueue.get()
                    self.add_ipc_connection(conn)
                    connectionQueue.task_done()

                self.check_ipc()
                        
                now = datetime.datetime.now()
                if next_loop_time < now:
                    # Add a blank line and then print the current time
                    print("") 
                    print(now)
                    self.config.status_str = str(now)

                    for moduleId in modules:
                        module = modules[moduleId]

                        print(' module %x' % module.can_id, end='')
                        if not module.quiet_mode:
                            print(':')
                        
                        if module.online:
                            for cmd in module.generate_status_cmds():
                                self.sendCmd(module, cmd)
                            print(': ' + module.one_line_summary())
                        else:
                            print(' - Offline')

                        # increment the elapsed time if sampling is happening
                        if module.online and module.state == ebox.EboxState.SAMPLING_ACTIVE:
                            module.add_elapsed_time(loop_interval)

                        # want to do this even if the module is offline to keep times in sync.
                        if module.ebox_schedule.next_event_time < now:
                            self.handle_ebox_schedule_event(module)

                    self.read_temperature_probe()

                    # bring any modules online that are currently offline
                    frame = CanFrame()
                    frame.can_id = 0x123
                    frame.initCmdFrame(0x01, CanFrame.CAN_COMMAND_REPEAT)
                    self.socket.send(frame)

                    # show how long the last processing took
                    print(datetime.datetime.now() - now)

                    # save elapsed time or other values
                    self.save_active_configuration()

                    # increment the log time if it has passed
                    if next_log_time < now:
                        self.save_module_data()
                        next_log_time = next_log_time + log_interval
                        # check if next time is still behind now, if so skip forward to now
                        if next_log_time < now:
                            next_log_time = now

                    # important not to update now before here or it could mess up the log file timing
                    now = datetime.datetime.now()
                    
                    next_loop_time = next_loop_time + loop_interval
                    # if we are behind by more than one loop_interval, fix it by skipping forward to now.
                    if next_loop_time < now:
                        next_loop_time = now

        except KeyboardInterrupt:
            print('main loop interrupted')
            raise

    def save_module_data(self):
        # don't need to save data if schedule is not running
        #  (also makes it easier when changing SD card since don't need to worry about missing data)
        if self.schedule_paused:
            return
        
        now = datetime.datetime.now()
        
        for moduleId in modules:
            module = modules[moduleId]
            position = module.ebox_config.module_position
            filename = 'module%02d-%04d-%02d-%02d.log' % (position, now.year, now.month, now.day)
            file_data = module.get_module_data()
            self.write_file_data(file_data, filename)

    def read_temperature_probe(self):
        device_path = "/sys/bus/i2c/devices/2-0048/"
        adc_mv_file = "in4_input"
        offset_mv_file = "in5_input"
        # use default values that will give obviously bad temperature
        adc_mv = 0
        offset_mv = 12000

        try:
            if not os.path.exists(device_path):
                self.log_action("temperature adc not found!")
            else:
                with open(device_path + adc_mv_file, 'r') as f:
                    str_value = f.read().strip()
                    adc_mv = int(str_value)

                with open(device_path + offset_mv_file, 'r') as f:
                    str_value = f.read().strip()
                    offset_mv = int(str_value)
        except Exception as ex:
            self.log_action("problem reading adc temperature sensor")

        temperature = (adc_mv - offset_mv) / 10

        for module_id in modules:
            module = modules[module_id]
            module.set_temperature_probe_data(temperature, adc_mv)
            
        

    def processMessage(self, frame):
        if frame.can_id in modules:
            modules[frame.can_id].receive_frame(frame)

    def checkMessage(self, cmdFrame, responseFrame):
        if cmdFrame.data[0] != responseFrame.can_id:
            return False
        if cmdFrame.data[1] != responseFrame.data[0]:
            return False
        return True

    def sendCmd(self, module, cmd):
        frame = cmd #this may change
        self.socket.send(frame)
        timeout = datetime.timedelta(seconds=0.05)
        
        try:
            waitingForResponse = True
            endTime = datetime.datetime.now() + timeout
            
            while(True):
                remainingTime = endTime - datetime.datetime.now()
                # block and wait for response.  If no response within the timeout
                #  the queue will throw a queue.Empty exception and exit the loop.
                response = messageQueue.get(True, remainingTime.microseconds / 100000) 
                self.processMessage(response)
                messageQueue.task_done()
                # check that the message received was the response to the original cmd.
                if self.checkMessage(frame, response):
                    return True
            
        except queue.Empty:
            print('   **no response to %x : %s' % (frame.can_id, frame.hexDataString()))
            module.send_cmd_failed(cmd)
            return False

    def setModuleTime(self, moduleId):
        now = datetime.datetime.now()
        frame = modules[moduleId].generate_cmd_date(now.year, now.month, now.day, now.weekday())
        frame.can_id = 0x123
        self.socket.send(frame)
            
        frame = modules[moduleId].generate_cmd_time(now.hour, now.minute, now.second)
        frame.can_id = 0x123
        self.socket.send(frame)

    def handle_ebox_schedule_event(self, module):
        if module.ebox_schedule.next_event == eboxschedule.ScheduleEvent.start_sampling:
            
            print('Sampling started for module %x' % module.can_id, end='')
            print(' for position %d' % module.ebox_schedule.position_index, end='')
            if module.ebox_schedule.locked_until_cartridge_change:
                print(' **locked**', end='')
            print('') # end line

            module.ebox_schedule.sampling_active = True
            module.elapsed_time = datetime.timedelta(seconds=0)

            if module.online and not module.ebox_schedule.locked_until_cartridge_change and not self.schedule_paused:
                module.state = ebox.EboxState.SAMPLING_STARTING
                # open solenoid
                frames = module.generate_solenoid_cmds(module.ebox_schedule.position_index, True)
                for frame in frames:
                    frame.can_id = 0x123
                    self.socket.send(frame)

                # delay to let the solenoid open
                sleep(0.1)

                # check current?

                # start pump
                GPIO.turn_on_pump(module.module_position)
                # delay to make sure pumps don't all turn on at the same time
                #  if all pumps try to turn on, there will be a 5 seconds delay between the first and the last
                self.timed_delay(datetime.timedelta(seconds=1))

                module.state = ebox.EboxState.SAMPLING_ACTIVE

        elif module.ebox_schedule.next_event == eboxschedule.ScheduleEvent.stop_sampling:
            
            print('Sampling finished for %x' % module.can_id, end='')
            print('  for position %d' % module.ebox_schedule.position_index, end='')
            print('') # end line

            module.ebox_schedule.sampling_active = False
            module.state = ebox.EboxState.SAMPLING_STOPPING

            if module.online:
                if 'on' == GPIO.get_pump_state(module.module_position):
                    # turn off pump
                    GPIO.turn_off_pump(module.module_position)
                    # delay to let the pump de-pressurize
                    self.timed_delay(self.config.pump_shutdown_time)

                # turn off solenoid
                frames = module.generate_solenoid_cmds(module.ebox_schedule.position_index, False)
                for frame in frames:
                    frame.can_id = 0x123
                    self.socket.send(frame)

            module.state = ebox.EboxState.INACTIVE_NORMAL

        module.ebox_schedule.forward_to_next_event()

    def log_pressure(self, time_period, log_function_name, cassette_index=None, module_list=None):
        count = 0
        if module_list is None:
            # if no list was passed, create the list from all the modules
            module_list = [v for (k,v) in modules.items()]
        # build the pressure frame
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(0x01, CanFrame.CAN_COMMAND_REPORT_RAW_PRESSURE)
        now = datetime.datetime.now()
        finish_time = now + time_period
        while now < finish_time:
            for module in module_list:
                if not module.online:
                    # skip current item since it is offline
                    continue
                if not cassette_index is None and not cassette_index < module.ebox_logsheet.cassette_count:
                    # cassette_index is out of range for current module
                    continue
                print(".", end="")
                frame.data[0] = module.can_id
                if not self.sendCmd(module, frame):
                    # failed to get a response so nothing new to log
                    continue
                logfunction = getattr(module.ebox_logsheet, log_function_name)
                if cassette_index is not None:
                    logfunction(cassette_index, module.last_cyclone_pressure, module.last_orifice_pressure)
                else:
                    logfunction(module.last_cyclone_pressure, module.last_orifice_pressure)
            count += 1
            now = datetime.datetime.now()
            self.check_ipc(True)
        return count

    def open_solenoid(self, module, solenoid_number):
        # update the state of the solenoids on the ebox
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(module.can_id, CanFrame.CAN_COMMAND_REPORT_POSITIONS)
        self.sendCmd(module, frame)
        # send the commands to open the solenoid and close any others that are open
        frames = module.generate_solenoid_cmds(solenoid_number, True)
        for frame in frames:
            frame.can_id = 0x123
            self.socket.send(frame)

    def close_solenoids(self, module):
        # update the state of the solenoids on the ebox
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(module.can_id, CanFrame.CAN_COMMAND_REPORT_POSITIONS)
        self.sendCmd(module, frame)
        # send the commands to close any open solenoids
        frames = module.generate_solenoid_cmds(1, False)
        for frame in frames:
            frame.can_id = 0x123
            self.socket.send(frame)

    def timed_delay(self, timedelta):
        now = datetime.datetime.now()
        finish = now + timedelta
        while now < finish:
            self.check_ipc(True)
            sleep(0.1)
            now = datetime.datetime.now()

    def leak_check_final(self):
        self.state = ControllerState.CHANGING_CARTRIDGES
        self.log_action("Started final/exposed/equipment test.")
        cassette_count = 0
        for (module_address, module) in modules.items():
            if module.online:
                # open a solenoid so the pump isn't just creating a vacuum
                self.open_solenoid(module, 1)
                GPIO.turn_on_pump(module.module_position)
                module.state = ebox.EboxState.EXPOSED_TEST_RUNNING
                module.ebox_logsheet.clear_raw_data_final()
                module.ebox_logsheet.status_text = "initializing pumps"
                # get the maximum cassette count
                if cassette_count < module.ebox_logsheet.cassette_count:
                    cassette_count = module.ebox_logsheet.cassette_count
                # delay for the pump to initialize:
                self.timed_delay(self.config.pump_warmup_time)

        count = 0
        # cycle through positions
        for cassette_index in range(cassette_count):
            position = cassette_index + 1
            # open the cassette position
            for (module_address, module) in modules.items():
                if module.online:
                    self.open_solenoid(module, position)
                    msg = "checking position %d (%d)" % (position, count)
                    module.ebox_logsheet.status_text = msg
                    print(msg)
            # delay to make sure the solenoids opened
            sleep(0.1)
            
            count += self.log_pressure(self.config.leak_check_sample_time,
                                       'add_pressure_final', cassette_index)
            sleep(0.5)
            print(count)

        # turn off pump (leave the last solenoid open so that pump can depressurize
        for (module_address, module) in modules.items():
            if module.online:
                GPIO.turn_off_pump(module.module_position)
                module.ebox_logsheet.status_text = "turning off pumps"

        # delay for pump to depressurize
        self.timed_delay(self.config.pump_shutdown_time)

        # close last solenoid and calculate results
        for (module_address, module) in modules.items():
            if module.online:
                self.close_solenoids(module)
                module.state = ebox.EboxState.EXPOSED_TEST_FINISHED
                module.ebox_logsheet.status_text = "finished (%d)" % count
                module.ebox_logsheet.generate_summary_final()
                module.ebox_logsheet.temperature = 20
                module.ebox_logsheet.final_initials = self.operator_initials
                module.ebox_logsheet.temperature = module.last_temperature_probe_reading
                module_position = module.ebox_config.module_position
                filename = module.ebox_logsheet.generate_log_filename(module_position)
                file_data = module.ebox_logsheet.get_data_final()
                self.write_file_data(file_data, filename)
        self.log_action("finished final/exposed/equipment test.")

    def leak_check_initial(self):
        self.log_action("started initial/clean/install test.")
        count = 0
        cassette_count = 0
        for (module_address, module) in modules.items():
            if module.online:
                # open a solenoid so the pump isn't just creating a vacuum
                self.open_solenoid(module, 1)
                GPIO.turn_on_pump(module.module_position)
                module.state = ebox.EboxState.CLEAN_TEST_RUNNING
                module.ebox_logsheet.clear_raw_data_initial()
                module.ebox_logsheet.status_text = "running pump test"
                # get the maximum cassette count
                if cassette_count < module.ebox_logsheet.cassette_count:
                    cassette_count = module.ebox_logsheet.cassette_count
                # record the pressure curve for the pump's initialization:
                module_list = [module]
                count += self.log_pressure(self.config.pump_warmup_time, 'add_pressure_pump_startup', None, module_list)

        # cycle through positions
        for cassette_index in range(cassette_count):
            position = cassette_index + 1
            msg = "checking position %d (%d)" % (position, count)
            print(msg)
            # open the cassette position
            for (module_address, module) in modules.items():
                if module.online:
                    self.open_solenoid(module, position)
                    module.ebox_logsheet.status_text = msg
            # delay to make sure the solenoids opened
            sleep(0.1)
            
            count += self.log_pressure(self.config.leak_check_sample_time,
                                       'add_pressure_initial', cassette_index)
            sleep(0.5)
            print(count)

        # close all solenoids and do vacuum leak test for pump
        print("starting leak test (%d)" % count)
        for (module_address, module) in modules.items():
            if module.online:
                self.close_solenoids(module)
        count += self.log_pressure(self.config.leak_check_sample_time,
                                   'add_pressure_leak_check')

        # open a solenoid to depressurize pump and then turn off the pump
        for (modules_address, module) in modules.items():
            if module.online:
                self.open_solenoid(module, 1)
                GPIO.turn_off_pump(module.module_position)
                module.ebox_logsheet.status_text = "turning off pumps"


        # delay for pump to depressurize
        self.timed_delay(self.config.pump_shutdown_time)

        # close last solenoid and calculate results and save to log file
        for (module_address, module) in modules.items():
            if module.online:
                self.close_solenoids(module)
                module.state = ebox.EboxState.CLEAN_TEST_FINISHED
                module.ebox_logsheet.status_text = "finished (%d)" % count
                module.ebox_logsheet.generate_summary_initial()
                module.ebox_logsheet.initial_initials = self.operator_initials
                module.ebox_logsheet.temperature = module.last_temperature_probe_reading
                module_position = module.ebox_config.module_position
                filename = module.ebox_logsheet.generate_log_filename(module_position)
                file_data = module.ebox_logsheet.get_data_initial()
                self.write_file_data(file_data, filename)

        self.log_action("finished initial/clean/install test.")

    def leak_check_finished(self):
        self.state = ControllerState.INACTIVE_NORMAL
        # clear cartridge change flag and restart normal schedule
        for (module_address, module) in modules.items():
            if module.online:
                module.state = ebox.EboxState.INACTIVE_NORMAL
                module.ebox_schedule.set_next_event_to_now()
        self.log_action("finished filter change process.")

    def add_ipc_connection(self, connection):
        self.connection_list.append(connection)
        self.polling_list.append(connection)

    def remove_ipc_connection(self, connection):
        self.connection_list.remove(connection)
        self.polling_list.remove(connection)
            
    def check_ipc(self, allow_data_only_commands=False):
        for conn in wait(self.connection_list, 0):
            if conn.closed:
                self.remove_ipc_connection(conn)
                continue
            
            try:
                msg = conn.recv()
            except (EOFError, ConnectionResetError):
                self.remove_ipc_connection(conn)
                print('IPC Connection closed')
            except Exception as ex:
                # we don't want an ipc related exception taking down the whole program,
                #  so just eat the exception here.
                print('Unhandled exception:')
                (type_, value, traceback) = sys.exc_info()
                # prints the same message to stderr that would be printed if uncaught
                sys.excepthook(type_, value, traceback)
            else:
                # handle communication here
                self.process_ipc_message(conn, msg, allow_data_only_commands)

    def process_ipc_message(self, connection, msg, allow_data_only_commands=False):
        split = msg.split(' ')
        if 'list' == split[0] and 'modules' == split[1]:
            valid_filters = ["all", "online", "enabled"]
            filter_ = ""
            if 2 < len(split):
                filter_ = split[2].strip()
            if not (filter_ in valid_filters):
                filter_ = "enabled"
            
            list_ = []
            for module_id in modules:
                if filter_ == "all":
                    list_.append(modules[module_id].module_position)
                elif filter_ == "enabled" and modules[module_id].enabled:
                    list_.append(modules[module_id].module_position)
                elif filter_ == "online" and modules[module_id].online:
                    list_.append(modules[module_id].module_position)
                    
            connection.send(list_)
        elif 'pump' == split[0] and not allow_data_only_commands:
            pump_number = int(split[1])
            if 'off' == split[2]:
                GPIO.turn_off_pump(pump_number)
            else:
                GPIO.turn_on_pump(pump_number)
        elif 'controller' == split[0]:
            if 'datetime' == split[1] and not allow_data_only_commands:
                datetime_str = ' '.join(split[2:])
                self.set_datetime(datetime_str)
            elif 'configuration' == split[1] and not allow_data_only_commands:
                message = ' '.join(split[2:])
                restart_message = ' '.join(split[2:4])
                print(restart_message)
                restart_needed = False
                if restart_message == 'restart needed':
                    restart_needed = True
                    message = ' '.join(split[4:])
                self.update_configuration(restart_needed, message)
            elif 'leak' == split[1] and not allow_data_only_commands:
                if 'final' == split[2]:
                    self.leak_check_final()
                elif 'initial' == split[2]:
                    self.leak_check_initial()
                elif 'finished' == split[2]:
                    self.leak_check_finished()
            elif 'flashcard' == split[1]:
                if 'change' == split[2] and not allow_data_only_commands:
                    self.sdcard_change_start()
                elif 'check' == split[2] and not allow_data_only_commands:
                    self.sdcard_check()
                elif 'status' == split[2]:
                    connection.send({'status': self.sdcard.status,
                                     'state': self.sdcard.state})
            elif 'status' == split[1]:
                # return controller status here
                pass
        elif 'set' == split[0] and not allow_data_only_commands:
            if 'operator' == split[1]:
                initials = ' '.join(split[2:])
                self.update_initials(initials)
            elif 'datetime' == split[1]:
                datetime_str = ' '.join(split[2:])
                self.set_datetime(datetime_str)
                
        elif 'module' == split[0]:
            module_number = int(split[1])
            for (module_id, module) in modules.items():
                if module.module_position == module_number:
                    # found it
                    break
                elif module_number == -1 and module.online:
                    # if index is -1 return first online module
                    break
            else:
                # module not found
                print('module "%s" not found' % str(module_number))
                return
            
            if 'list' == split[2]:
                dict_ = module.to_dictionary()
                connection.send(dict_)
                return
            elif 'solenoid' == split[2] and not allow_data_only_commands:
                solenoid_number = int(split[3])
                action = split[4] # should be 'on'/'off'
                exclusive = True
                if 5 < len(split) and split[5] == 'inclusive':
                    exclusive = False
                print('solenoid %d %s' % (solenoid_number, action))
                if 'on' == action.strip():
                    action = True
                else: # off
                    action = False
                frames = module.generate_solenoid_cmds(solenoid_number, action, exclusive)
                for frame in frames:
                    frame.can_id = 0x123
                    self.socket.send(frame)
            elif 'change' == split[2] and not allow_data_only_commands:
                msg = 'module %d cartridge changed' % module_number
                self.log_action(msg)
                module.change_cartridge()
            else:
                print('message not understood "%s"' % str(msg))
                return
        elif 'exit' == split[0]:
            if 'shutdown' == split[1]:
                self.log_action('Shutdown now')
                # ping watchdog so it doesn't cause reboot in the middle of the shutdown
                #watchdog here
                cmd = 'shutdown -h now'
                print(subprocess.getoutput(cmd))
                quit()
            elif 'to' == split[1] and 'desktop' == split[2]:
                self.log_action('Exit to desktop')
                #stop watchdog here
                #script to load desktop here
                quit()
            elif 'program' == split[1]:
                self.log_action('Exit program')
                #stop watchdog here
                quit()
        else:
            print('message not understood "%s"' % str(msg))
            return

    def update_configuration(self, need_restart, message):
        msg = "updated configuration"
        if need_restart:
            msg += " (with restart)"
        msg += ": %s" % message
        self.log_action(msg)
        if need_restart:
            self.restart_eboxes()

    def update_initials(self, initials):
        initials = initials.strip()
        self.operator_initials = initials
        self.log_action("updated operator initials to '%s'." % initials)

    def set_datetime(self, datetime_str):
        now = datetime.datetime.now()
        print("set time to '%s'" % datetime_str)
        cmd = 'date +"%x %X" -s "' + datetime_str + '"'
        print(cmd)
        os.system(cmd)
        os.system('hwclock -w')
        self.update_configuration(True, "Date changed from '%s' to '%s'." % (str(now), datetime_str))

    def restart_eboxes(self):
        print("restarting eboxes")
        self.log_action("restarting ebox objects")
        self.load_configuration()

    def log_action(self, action):
        now = datetime.datetime.now()
        msg = '%s: %s\n' % (str(now), action)
        print(msg)

        now = datetime.datetime.now()
        filename = 'controller-%04d-%02d.log' % (now.year, now.month)
        
        self.write_file_data(msg, filename)


    def write_file_data(self, file_data, filename):
        local_folder = self.config.output_folder
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)
        local_filename = local_folder + filename
        
        with open(local_filename, 'a', newline='') as f:
            f.write(file_data)

        if self.sdcard.available():
            sdcard_folder = self.config.sdcard_mount_directory
            sdcard_filename = sdcard_folder + '/' + filename
            with open(sdcard_filename, 'a', newline='') as f:
                f.write(file_data)



    def sdcard_change_start(self):
        self.log_action('Unmounting SD card.')
        self.sdcard.unmount()

    def sdcard_check(self):
        if self.sdcard.available():
            # copy files here
            pass
        else:
            pass

    


if __name__ == "__main__":
    
    GPIO.init_pumps()

    canbusReceiveThread = CanbusReceiveThread(daemon=True)
    canbusReceiveThread.start()

    ipcReceiveThread = IpcThread(daemon=True)
    ipcReceiveThread.start()

    # initialize watchdog here

    try:
        main = Main()
        main.load_configuration()
        main.loop()
    except KeyboardInterrupt:
        print(' keyboard interrupt ')
        
        # turn off watchdog here

    quit()
