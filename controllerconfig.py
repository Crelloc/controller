import configparser
import datetime
import pickle

class ControllerConfig(object):
    def __init__(self, controller_config, controller_active_config):
        self.config = controller_config
        self.active_config = controller_active_config

    @property
    def site_name(self):
        return self.config.get('SiteName', '')

    @property
    def uc_code(self):
        return self.config.get('UcCode', '0000')

    @property
    def pump_warmup_time(self):
        time_str = self.config.get('PumpWarmupTime', '00:00:10')
        return self._parse_timedelta(time_str)

    @property
    def pump_shutdown_time(self):
        time_str = self.config.get('PumpShutdownTime', '00:00:11')
        return self._parse_timedelta(time_str)

    @property
    def leak_check_sample_time(self):
        time_str = self.config.get('LeakCheckSampleTime', '00:00:12')
        return self._parse_timedelta(time_str)

    @property
    def main_loop_interval(self):
        time_str = self.config.get('MainLoopInterval', '00:00:02')
        return self._parse_timedelta(time_str)

    @property
    def log_file_interval(self):
        time_str = self.config.get('LogFileInterval', '00:15:00')
        return self._parse_timedelta(time_str)

    @property
    def output_folder(self):
        return self.config.get('OutputFolder', '../output/')

    @property
    def sdcard_hardware_bus(self):
        return self.config.get('SDCardHardwareBus', 'mmc2')

    @property
    def sdcard_mount_directory(self):
        return self.config.get('SDCardMountDirectory', '/mnt/p1')

    @property
    def state(self):
        value_str = self.active_config.get('State', None)
        if value_str is None:
            return None
        # convert from string to binary string for pickle
        value_bstr = eval(value_str)
        # convert back to objects and return
        value = pickle.loads(value_bstr)
        return value
    @state.setter
    def state(self, value):
        # convert from objects to binary string
        value_bstr = pickle.dumps(value)
        # convert to standard string
        value_str = str(value_bstr)
        self.active_config['State'] = value_str

    @property
    def status_str(self):
        return self.active_config.get('StatusString', '')
    @status_str.setter
    def status_str(self, value):
        self.active_config['StatusString'] = value

    def _parse_timedelta(self, timedelta_string):
        # expects format to be in HH:MM:SS, can have more than 24 hours (it will convert automatically to days)
        splitstring = timedelta_string.split(':')
        timespan = datetime.timedelta(hours = int(splitstring[0]),
                                      minutes = int(splitstring[1]),
                                      seconds = int(splitstring[2]))
        return timespan

        
