#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
 
#include <linux/watchdog.h>
 
#define WATCHDOGDEV "/dev/watchdog"



int main(void){

	FILE *pipein_fp;
        char readbuf[80];
	char* cmd = "python3 controller_main.py";
	int fd = open(WATCHDOGDEV, O_RDWR);
	if (fd < 0) {
                perror("open");
                return 1;
        }

        int timeout = 60;
        ioctl(fd, WDIOC_SETTIMEOUT, &timeout);
        //printf("The timeout was set to %d seconds\n", timeout);

	while(1){

		 /* Create one way pipe line with call to popen() */
		pipein_fp = popen("ps -C python3 -o cmd", "r");
		if(pipein_fp == NULL){
			 perror("popen");
        	        exit(1);
		}
		while(fgets(readbuf, 80, pipein_fp)){
			if(strstr(readbuf, cmd) != NULL){
 				printf("%s", readbuf);
				ioctl(fd, WDIOC_KEEPALIVE, 0);
		                sleep(2);
			}
			else{

				goto end;
			}
			pclose(pipein_fp);
		}
	}
end:
	pclose(pipein_fp);
	return 0;
}
