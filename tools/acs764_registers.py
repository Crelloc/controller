import i2c

values = {}

bus_master = i2c.I2CMaster()

value = bus_master.read(0x60, 0x02, 3)
values["avg_points"] = value[2]

# set the gain to 15.66 mA/LSB
bus_master.write(0x60, 0x04, 0x00, 0x00, 0x02)

value = bus_master.read(0x60, 0x04, 3)
values["gain_range"] = value[2]

value = bus_master.read(0x60, 0x06, 3)
values["fault_level"] = value[2]

gain = {0b10: 15.66,
        0b11: 31.31,
        0b00: 62.62,
        0b01: 125.24}
values["gain"] = gain[values["gain_range"]]

value = bus_master.read(0x60, 0x00, 3)
status_value = value[1]
values["raw_status"] = value[1]

SYNC_BIT = 0x2
LATCHED_FAULT_BIT = 0x4
NONLATCHED_FAULT_BIT = 0x6

values["sync"] = (status_value & SYNC_BIT) == SYNC_BIT
values["latched_fault"] = (status_value & LATCHED_FAULT_BIT) == LATCHED_FAULT_BIT
values["non_latched_fault"] = (status_value & NONLATCHED_FAULT_BIT) == NONLATCHED_FAULT_BIT

values["raw_current"] = value[2]
values["current"] = values["raw_current"] * values["gain"]

bus_master.close()

for key,value in values.items():
    print('%s: %s' % (key, str(value)))
