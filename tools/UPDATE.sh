#!/bin/bash
#Get the latest controller files:

#cd /usr/local/controller && git pull && git checkout -f master

#set the SD card to automount:
echo "setting up the SD card to automount."
cd /usr/local/controller/tools
cat fstab >> /etc/fstab

p1='/mnt/p1'
p2='/mnt/p2'

if [ -d "$p1" ]; then
    echo "mountpoint '/mnt/p1' exits"
else
    echo "mountpoint '/mnt/p1' doesnt exits"
    echo "making directory -------: /mnt/p1"
    mkdir /mnt/p1
fi

if [ -d "$p2" ]; then
    echo "mountpoint '/mnt/p2' exits"
else
    echo "mountpoint '/mnt/p2' doesnt exits"
    echo "making directory -------: /mnt/p2"
    mkdir /mnt/p2
fi

#Autostart for gui
echo "setting up Autostart for gui."

cd /usr/share/xsessions/
ln -s /usr/local/controller/tools/debian_user/CONTROLLER.desktop .

cd /usr/share/pixmaps/
ln -s /usr/local/controller/tools/debian_user/REBOOT.png .

cd /etc/lightdm/ 
rm lightdm.conf lightdm-gtk-greeter.conf || true
ln -s /usr/local/controller/tools/debian_user/lightdm.conf .
ln -s /usr/local/controller/tools/debian_user/lightdm-gtk-greeter.conf .

#Get latest device tree settings:
echo "getting the latest device tree settings"
cd /usr/local/dtb-rebuilder && git pull && git checkout 4.1 && make clean && make install
echo "done"

sleep 1

echo "You should reboot now."
