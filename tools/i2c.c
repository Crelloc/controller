#include<stdio.h>
#include<unistd.h>
#include<linux/i2c-dev.h>

#include<sys/ioctl.h>
#include<fcntl.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>

void do_read(int, unsigned char,int);
void do_write(int, unsigned char, unsigned char);

void printbits(int num_of_bytes, unsigned char rxBuffer[], unsigned char startaddr);

int main(int argc, char **argv){
	int		tenBitAddress = 0;
	int		c;
	int		read = 0;
	int		write = 0;
	int		i2cHandle;
	char		filename[40];
	unsigned char	reg;
	unsigned int	val;
	int 		totbytes = 1;
	unsigned char 	addr;
	int		bus;


	while((c = getopt(argc, argv, "hb:d:r:w:t:")) != EOF){

		switch(c){
		case 'r':
			read = 1;
			reg = (unsigned char)(strtol(optarg, NULL, 16));
			continue;						
		case 'w':
			write = 1;
			val = (unsigned int)(strtol(optarg, NULL, 16));
			continue;			
		case 'b':
			bus = atoi(optarg); 
			continue;
		case 'd': 
			addr = strtol(optarg, NULL, 16);
			continue;
		case 't':
			totbytes = atoi(optarg);
			continue;
		case 'h':
		case '?':
usage:
			fprintf(stderr, "usage: %s [-h] [-b B] [-d DevAddr] [-r reg] [-w val] [-t bytes]\n", argv[0]);
			return 1;

		}
	}	
	
	if(argc == 1) goto usage;

         //Create a file descriptor for the I2C bus
	snprintf(filename, 40, "/dev/i2c-%d", bus);
        i2cHandle = open(filename, O_RDWR);
        if(i2cHandle < 0){
                 printf("error: %s [%s]\n", strerror(errno), filename);
                return 1;

        }

  //Tell the I2C peripheral that the device address is (or isn't) a 10-bit value

        if (ioctl(i2cHandle, I2C_TENBIT, tenBitAddress) < 0){
                printf("ioctl error: %s\n", strerror(errno));
                return 1;
        }

         //Tell the I2C peripheral what the address of the device is

        if ( ioctl(i2cHandle, I2C_SLAVE_FORCE, addr) < 0) {
                printf("ioctl error: %s\n", strerror(errno));
                return 1;
        }


	if(read && write > 0){
		printf("write val %02x to reg: %02x\n", val, reg);
		do_write(i2cHandle, reg, val);
		
	}
	else if(read){
	
		do_read(i2cHandle, reg, totbytes);
	}
	else if(write){
		goto usage;		
	}
	else{
		printf("missing -r argument\n");
		goto usage;
		return 1;
	}	

	close(i2cHandle);

	return 0;
}


void do_read(int i2cHandle, unsigned char reg, int totbytes){
	unsigned char 	rxBuffer[32];
	unsigned char 	txBuffer[32];


	 //clear out buffers

        memset(rxBuffer, 0, sizeof(rxBuffer));
        memset(txBuffer, 0, sizeof(txBuffer));

	txBuffer[0] = (unsigned char)reg;                     //This is the address we want to read from

 	if (write(i2cHandle, txBuffer, 1) != 1) {/* ERROR HANDLING: i2c transaction failed */
		printf("i2c write transaction failed\n");
		return;
  	}

	if (read(i2cHandle, rxBuffer, totbytes) != totbytes) {   /* ERROR HANDLING: i2c transaction failed */
		printf("i2c read transaction failed\n");
		return;
  	} 


	int p;
        for(p=0; p< totbytes; p++){
                printf("%02x\n", rxBuffer[p]);
        }
//	printbits(totbytes, rxBuffer, reg);

}
void do_write(int i2cHandle, unsigned char reg, unsigned char val){

	unsigned char   rxBuffer[32];
        unsigned char   txBuffer[32];
	
	memset(rxBuffer, 0, sizeof(rxBuffer));
        memset(txBuffer, 0, sizeof(txBuffer));

        txBuffer[0] = reg;                     //This is the address we want to read from
	txBuffer[1] = val;			//value to write

        if (write(i2cHandle, txBuffer, 2) != 2) {/* ERROR HANDLING: i2c transaction failed */
                printf("i2c write transaction failed\n");
                return;
        }

       /* if (read(i2cHandle, rxBuffer, totbytes) != totbytes) {  ERROR HANDLING: i2c transaction failed 
                printf("i2c read transaction failed\n");
                return;
        }*/
	

}
void printbits(int num_of_bytes, unsigned char rxBuffer[], unsigned char startaddr){

        unsigned int temp;
        unsigned int mask = 0x80;
        int i; 

        for(i=0; i<num_of_bytes; i++){
		temp = rxBuffer[i];
		printf("%02x : ",startaddr++);
		printf("0x%02x : ", temp);
                while(mask > 0){
                        if((temp & mask) == mask){
                                printf("%d", 1);
                        }
                        else{   
                                printf("%d", 0);
                        }
                        mask >>= 1;
                }
                printf("\n");
                mask = 0x80;
        }
        printf("\n");


}



