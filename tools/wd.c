#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <linux/watchdog.h>
#include <unistd.h>
#define WATCHDOGDEV "/dev/watchdog"


int main(int argc, char **argv){
	int bootstatus;
	int fd = open(WATCHDOGDEV, O_RDWR);
        if (fd < 0) {
                perror("open");
                return 1;
        }

	if (ioctl(fd, WDIOC_GETBOOTSTATUS, &bootstatus) == 0) {
      		fprintf(stdout, "Last boot is caused by : %s\n",
         	(bootstatus != 0) ? "Watchdog" : "Power-On-Reset");
  	 }
	else {
      		fprintf(stderr, "Error: Cannot read watchdog status\n");
		exit(EXIT_FAILURE);
   	}

	write(fd, "V", 1);
   	/* Closing the watchdog device will deactivate the watchdog. */
   	close(fd);
	return 0;
}
