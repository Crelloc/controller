import subprocess

for address in range(0, 0x1f):
    cmd = 'i2cget -y -f 0 0x24 0x%x' % address
    value = subprocess.check_output(cmd, shell=True)
    value = value.decode(encoding='UTF-8').strip()
    i = int(value, 0)
    b = '{0:8b}'.format(i)
    print('%2x : %s : %s' % (address, value, b))
