import subprocess
import string

offset = 500 #in mV
cmd = 'cd /sys/bus/iio/devices/iio:device0 && cat in_voltage0*'

raw_output = subprocess.check_output(cmd, shell=True)
output = raw_output.decode('utf-8')
output = output.strip()
output = output.split('\n')

#print(output[0])
raw = int(output[0])
#print(raw)
mV = raw*(1.8/4095)*1000
C = (mV - offset)/10 
print('raw: %d, mV: %0.2f, Celsius: %0.2f' % (raw, mV, C))
