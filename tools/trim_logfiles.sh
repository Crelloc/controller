#!/bin/bash

##-mtime n:
##	File's  data  was last modified n*24 hours ago.

##+n	for greater than n,
##-n    for less than n,
##n     for exactly n.

sudo find /usr/local/output -mtime +90 -type f -delete

