import os
import time
import subprocess

for address in range(0, 0x0a):
    cmd = './i2c -b 2 -d 0x20 -r 0x%x  -t 1' % address
    raw_output = subprocess.check_output(cmd, shell=True)
    output = raw_output.decode('utf-8')
    output = output.strip()
    number = int(output, 16)
    b = '{0:08b}'.format(number)
    print('%2xh : %sh : %s' % (address, output, b))
