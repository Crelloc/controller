import os
import time
import subprocess


def Humidity(Humcount):
	Hum = (Humcount*100) / (16384 - 1)
	print('Humidity: %.3f%%' % Hum )

def Temperature(Temcount):
	Tem = ((Temcount*165) / (16384 - 1)) - 40
	print('Temperature: %.3f(C)' % Tem )
	 

cmd = './i2c -b 1 -d 0x27 -r 0x00 -t 4'
print(cmd) 
raw_output = subprocess.check_output(cmd, shell=True)
output = raw_output.decode('utf-8')
output = output.strip()
outstring = output
output = output.split('\n')

bytes = []


if(output[0][0].isdigit() == 0):
	print(outstring)

else:
	for index in range(len(output)):
		number = int(output[index], 16)
		bytes.append(number)
		print(bytes[index])

	status = (bytes[0] >> 6) & 0x03
	print("status: " + str(status))

	Humcount = bytes[0] & 0x3f
	Humcount = (Humcount << 8) | bytes[1]
	Humidity(Humcount)

	if(len(bytes) > 3): 	
		Temcount = (bytes[2] << 8) | bytes[3]
		Temcount >>= 2
		Temperature(Temcount)
