import subprocess
import string

cmd = 'cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pins'

raw_output = subprocess.check_output(cmd, shell=True)
output = raw_output.decode('utf-8')
output = output.strip()
output = output.split('\n')

for index in range (1,len(output)):
	str = output[index].split(' ')
	pin_number = str[1]
	str1 = str[2]
	str1 = str1[1:-3]
	hexnum = int(str1, 16)
	offset = hexnum - 0x44e10800
	hexnum2 = int(str[3], 16)
	binstr = "{0:08b}".format(hexnum2)
	slewctrl = int(binstr[1])
	rxactive = int(binstr[2])
	pu_type_sel = int(binstr[3])
	pu_d_en = int(binstr[4])
	mmode = int(binstr[5:], 2)	
	print('pin %s : DT Offset 0x%03x : 0x%02x : mode %d : %s, %s, %s, %s' % 
		(pin_number, offset, hexnum2, mmode, 
		'Slew Rate: Slow' if slewctrl else 'Slew Rate: fast', 
		'Enable Receiver' if rxactive else 'Disable Receiver', 
		'Pullup type' if pu_type_sel else 'Pulldown type', 
		'Disabled' if pu_d_en else 'Enabled'))
