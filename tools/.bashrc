# ~/.bashrc: executed by bash(1) for non-login shells.

# Note: PS1 and umask are already set in /etc/profile. You should not
# need this unless you want different defaults for root.
# PS1='${debian_chroot:+($debian_chroot)}\h:\w\$ '
# umask 022

# You may uncomment the following lines if you want `ls' to be colorized:
export LS_OPTIONS='--color=auto'
eval "`dircolors`"
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'

# Some more alias to avoid making mistakes:
# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'

#put in /root directory
alias tools='cd /usr/local/controller/tools'
complete -F _minimal tools
alias cape='nano /boot/uEnv.txt'
alias lspi='l /dev/spi*'
alias li2c='ll /sys/bus/i2c/devices/i2c-*'
alias overlays='cd /usr/local/bb.org-overlays'
complete -F _minimal overlays
alias dtree='cd /usr/local/dtb-rebuilder'
complete -F _minimal dtree
alias local='cd /usr/local'
complete -F _minimal local
alias showcan0='ip -statistics -details link show can0'
alias agi='sudo apt-get install'
alias update='sudo apt-get update'
alias hg='history | grep'
alias arm='/usr/local/dtb-rebuilder/src/arm'
alias mcmi='make clean && make install'
