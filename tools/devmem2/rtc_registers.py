import subprocess

# rtc starting address for memory registers
base_address = 0x44e3e000

for relative_address in range(0, 0xa0, 0x4):
    address = base_address + relative_address
    cmd = './devmem2.exe 0x%x' % address
    value = subprocess.check_output(cmd, shell=True)
    value = value.decode(encoding='UTF-8')
    #print(value)
    value = value.split('\n')
    value = value[2].split(': ')
    value = int(value[1], 0)
    b = '{0:32b}'.format(value)
    b = " ".join(b[i:i+8] for i in range(0, len(b), 8))
    x = '{0:8x}'.format(value)
    x = " ".join(x[i:i+2] for i in range(0, len(x), 2))
    print('%2x : %s : %s ' % (relative_address, x, b))
