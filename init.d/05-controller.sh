#! /bin/sh
### BEGIN INIT INFO
# Provides:          controller
# Required-Start:    $all
# Required-Stop:     can-bus
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Starts the controller backend
### END INIT INFO


PATH=/sbin:/usr/sbin:/bin:/usr/bin

. /lib/init/vars.sh
. /lib/lsb/init-functions

cd /usr/local/controller

case "$1" in
    start)
      screen -d -m ./controller_main.py

      su -s xterm -c startx -- :1 -dpi 100 debian &
      ;;
    restart|reload|force-reload)
      echo "Error: argument '$1' not supported" >&2
      exit 3
      ;;
    stop|status)
      # No-op
      exit 0
      ;;
    *)
      echo "Usage: $0 start" >&2
      exit 3
      ;;
esac

