#!/bin/bash
# -*-ksh-*-
### BEGIN INIT INFO
# Provides:          can-bus
# Required-Start:    capemgr.sh
# Required-Stop:     
# Default-Start:     2
# Default-Stop:
# Short-Description: Initialize the CAN bus on the BBB
### END INIT INFO

PATH="/sbin:/bin"

. /lib/lsb/init-functions

case "$1" in
  status)
    ip -details -stats link show can0
    exit 0
    ;;

  start) 
#    if ! grep -q BB-DCAN /sys/devices/bone_capemgr.*/slots; then
#        echo "Loading BB-DCAN firmware."
#        echo BB-DCAN > /sys/devices/bone_capemgr.*/slots
#    fi

    modprobe can
    modprobe can-dev
    modprobe can-raw
    
    ip link set can0 up type can bitrate 500000 restart-ms 100
    ifconfig can0 up

    exit 0
    ;;
  stop)
    ifconfig can0 down
    sleep 2
    ip link set dev can0 down
    sleep 2

    #modprobe --remove c_can_platform
    #modprobe --remove can-raw can-dev can
    #modprobe --remove can-dev
    #modprobe --remove can

    exit 1
    ;;
  *)
    echo "Usage: $0 {start|stop|status}" >&2
    exit 1
    ;;
esac

