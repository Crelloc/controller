#!/bin/bash
#set system clock to external rtc

# make sure bit 7: Oscillator Stop Flag (OSF) is zero.
/usr/sbin/i2cset -y -f 2 0x68 0x0f 0x04 

echo ds3232 0x68 > /sys/class/i2c-adapter/i2c-2/new_device

/sbin/hwclock -s -f /dev/rtc1
/sbin/hwclock -w

#Enable 1hz square wave.
#sleep 2  
#i2cset -y -f 1 0x68 0x0e 0x00
#cd /usr/local/controller/tools
#./i2c -b 1 -d 0x68 -r 0x0e -w 0x00
