import configparser
import datetime
import pickle

class EboxConfig(object):
    def __init__(self, module_config, module_active_config):
        self.config = module_config
        self.active_config = module_active_config
        
        # need to pull these values ahead of time otherwise the
        #  forward_to_now function in the EboxSchedule runs too slow
        self._sampling_cycle_time_on = self._parse_timedelta(module_config['SamplingCycleTimeOn'])        
        self._sampling_cycle_time_off = self._parse_timedelta(module_config['SamplingCycleTimeOff'])     
        self._filter_cartridge_schedule = self._parse_filter_cartridge_schedule(module_config['FilterCartridgeSchedule'])
        
        self.print_info()

    @property
    def address(self):
        # BusAddress is in hex, so let int parse it with the 0 option
        return int(self.config.get('BusAddress', '0x40'), 0)

    @property
    def module_enabled(self):
        return self.config.getboolean('ModuleEnabled', False)

    @property
    def sampling_start(self):
        date_str = self.config.get('SamplingStart', '2015/05/23 00:00:00')
        return datetime.datetime.strptime(date_str, '%Y/%m/%d %X')

    @property
    def sampling_cycle_offset_count(self):
        return int(self.config.get('SamplingCycleOffsetCount', '0'))

    @property
    def sampling_cycle_time_on(self):
        #time_str = self.config.get('SamplingCycleTimeOn', '00:02:02')
        #return self._parse_timedelta(time_str)
        return self._sampling_cycle_time_on

    @property
    def sampling_cycle_time_off(self):
        #time_str = self.config.get('SamplingCycleTimeOff', '00:01:01')
        #return self._parse_timedelta(time_str)
        return self._sampling_cycle_time_off

    @property
    def filter_cartridge_schedule(self):
        #schedule_str = self.config.get('FilterCartridgeSchedule', '[4]')
        #return self._parse_filter_cartridge_schedule(schedule_str)
        return self._filter_cartridge_schedule

    def print_info(self):
        print('Loading config for %x' % self.address)
        print('  Start: ', end='')
        print(self.sampling_start, end='')
        print('  Offset: ', end='')
        print(self.sampling_cycle_offset_count, end='')
        print('  TimeOn: ', end='')
        print(self.sampling_cycle_time_on, end='')
        print('  TimeOff: ', end='')
        print(self.sampling_cycle_time_off)
        print('   Schedule: ', end='')
        print(self.filter_cartridge_schedule, end='')
        print('  Lock: ', end='')
        print(self.prevent_sampling_until_cartridge_change)

    @property
    def prevent_sampling_until_cartridge_change(self):
        return self.config.getboolean('PreventSamplingUntilCartridgeChange')

    @property
    def module_position(self):
        return int(self.config.get('ModulePosition', 0))

    @property
    def cyclone_sensor_version(self):
        return int(self.config.get('CycloneSensorVersion', 1))

    @property
    def cyclone_sensor_pressure_max(self):
        return float(self.config.get('CycloneSensorPressureMax', 1))

    @property
    def cyclone_sensor_pressure_min(self):
        return float(self.config.get('CycloneSensorPressureMin', -1))

    @property
    def orifice_sensor_version(self):
        return int(self.config.get('OrificeSensorVersion', 1))

    @property
    def orifice_sensor_pressure_max(self):
        return float(self.config.get('OrificeSensorPressureMax', 15))

    @property
    def orifice_sensor_pressure_min(self):
        return float(self.config.get('OrificeSensorPressureMin', 0))
    

    @property
    def print_debug_output(self):
        return self.config.getboolean('PrintDebugOutput')

    @property
    def locked_until_cartridge_change(self):
        return self.active_config.getboolean('LockedUntilCartridgeChange', False)
    @locked_until_cartridge_change.setter
    def locked_until_cartridge_change(self, value):
        self.active_config['LockedUntilCartridgeChange'] = str(value)

    @property
    def cartridge_was_changed(self):
        return self.active_config.getboolean('CartridgeWasChanged', False)
    @cartridge_was_changed.setter
    def cartridge_was_changed(self, value):
        self.active_config['CartridgeWasChanged'] = str(value)

    @property
    def current_cartridge_dates(self):
        value_str = self.active_config.get('CurrentCartridgeDates', None)
        if value_str is None:
            return []
        # convert from string to binary string for pickle
        value_bstr = eval(value_str)
        # convert back to objects and return
        value = pickle.loads(value_bstr)
        return value
    @current_cartridge_dates.setter
    def current_cartridge_dates(self, value):
        # convert from objects to binary string
        value_bstr = pickle.dumps(value)
        # convert to standard string
        value_str = str(value_bstr)
        self.active_config['CurrentCartridgeDates'] = value_str

    @property
    def elapsed_times(self):
        value_str = self.active_config.get('ElapsedTimes', None)
        if value_str is None:
            return {}
        # convert from string to binary string for pickle
        value_bstr = eval(value_str)
        # convert back to objects and return
        value = pickle.loads(value_bstr)
        return value
    @elapsed_times.setter
    def elapsed_times(self, value):
        # convert from objects to binary string
        value_bstr = pickle.dumps(value)
        # convert to standard string
        value_str = str(value_bstr)
        self.active_config['ElapsedTimes'] = value_str

    @property
    def current_schedule_index(self):
        return int(self.active_config.get('CurrentScheduleIndex', -1))
    @current_schedule_index.setter
    def current_schedule_index(self, value):
        self.active_config['CurrentScheduleIndex'] = str(value)
        
        
    def _parse_timedelta(self, timedelta_string):
        # expects format to be in HH:MM:SS, can have more than 24 hours (it will convert automatically to days)
        splitstring = timedelta_string.split(':')
        timespan = datetime.timedelta(hours = int(splitstring[0]),
                                      minutes = int(splitstring[1]),
                                      seconds = int(splitstring[2]))
        return timespan

    def _parse_filter_cartridge_schedule(self, scheduleString):
        splitstring = scheduleString.split('-')
        schedule = []
        for item in splitstring:
            schedule.append(int(item))
        return schedule

