from canbus import CanFrame
from enum import Enum
import datetime
import os
import csv
import io
from eboxschedule import EboxSchedule
from eboxconfig import EboxConfig
from eboxlogsheet import EboxLogsheetData, EboxLogsheetCassetteData
from gpio import GPIO

class SolenoidPosition(Enum):
    Closed = 0
    Open = 1
    Unknown = 2

class EboxState(Enum):
    INACTIVE_NORMAL = 0
    SAMPLING_ACTIVE = 10
    SAMPLING_STARTING = 11
    SAMPLING_STOPPING = 12
    EXPOSED_TEST_RUNNING = 20
    EXPOSED_TEST_FINISHED = 21
    CARTRIDGES_CHANGED = 22
    CLEAN_TEST_RUNNING = 23
    CLEAN_TEST_FINISHED = 24

class Ebox(object):
    def __init__(self, ebox_config):
        self.ebox_config = ebox_config
        self.can_id = ebox_config.address
        self.module_position = ebox_config.module_position
        self.enabled = ebox_config.module_enabled
        
        self.quiet_mode = not ebox_config.print_debug_output

        self._init_local_variables()
        
        print('      Init completed for %x' % self.can_id)

    def _init_local_variables(self):
        self.online = False
        self.state = EboxState.INACTIVE_NORMAL
        
        self.last_pressure_time = datetime.datetime.min
        self.failed_cmd_count = 0

        self.version_major = 0
        self.version_minor = 0
        self.version_date_string = ''
        
        self.last_cyclone_pressure = 0.0
        self.last_orifice_pressure = 0.0
        self.last_cyclone_temperature = 0.0
        self.last_orifice_temperature = 0.0

        self.average_cyclone_pressure = 0.0
        self.average_orifice_pressure = 0.0
        self.pressure_averaging_length = 200.0

        self.last_current_reading = 0.0

        self.last_temperature_probe_reading = 0.0
        self.last_temperature_probe_reading_mv = 0.0

        self.solenoids = [SolenoidPosition.Unknown, SolenoidPosition.Unknown, SolenoidPosition.Unknown, SolenoidPosition.Unknown]
        self.motor_going_up = False
        self.motor_going_down = False

        self.last_main_loop_count = 0
        self.last_cyclone_error_count = 0
        self.last_orifice_error_count = 0

        self.ebox_schedule = EboxSchedule(self.ebox_config)

        if len(self.ebox_config.current_cartridge_dates) == 0:
            self.ebox_config.current_cartridge_dates = self.ebox_schedule.calculated_cartridge_dates
            self.ebox_config.current_schedule_index = self.ebox_schedule.cartridge_schedule_index
        elif self.ebox_config.current_cartridge_dates[0] < self.ebox_schedule.calculated_cartridge_dates[0]:
            # if the "current cartridge" is behind the calculated cartridge, lock sampling
            self.ebox_schedule.locked_until_cartridge_change = True

        self.ebox_logsheet = EboxLogsheetData(self.ebox_config.current_schedule_index,
                                              self.ebox_config.current_cartridge_dates)
        if 0 < len(self.ebox_config.elapsed_times):
            pass

        self.ebox_config.elapsed_times = self.ebox_logsheet.elapsed_times

    def to_dictionary(self):
        properties = ['average_cyclone_pressure', 'average_orifice_pressure', 'can_id',
                      'failed_cmd_count', 'last_current_reading', 'last_cyclone_error_count',
                      'last_cyclone_pressure', 'last_cyclone_temperature', 'last_main_loop_count',
                      'last_orifice_error_count', 'last_orifice_pressure',
                      'last_temperature_probe_reading', 'last_temperature_probe_reading_mv',
                      'last_orifice_temperature', 'module_position', 'motor_going_down',
                      'motor_going_up', 'online', 'pressure_averaging_length', 'quiet_mode',
                      'solenoids', 'version_date_string', 'version_major', 'version_minor',
                      'last_pressure_time', 'state', 'sampling_movable_cassette',
                      'next_cartridge'
                      ]
        dict_ = {}
        for prop in properties:
            value = getattr(self, prop)
            dict_[prop] = value
        dict_['ebox_schedule'] = self.ebox_schedule.to_dictionary()
        dict_['pump_state'] = GPIO.get_pump_state(self.module_position)
        dict_['ebox_logsheet'] = self.ebox_logsheet.to_dictionary()
        return dict_

    def one_line_summary(self):
        result = 'Cyclone: %0.4f  ' % self.last_cyclone_pressure
        result += 'Orifice: %0.4f  ' % self.last_orifice_pressure
        result += 'Current: %0.2f mA  ' % self.last_current_reading
        result += 'loop: %d  ' % self.last_main_loop_count
        if 0 < self.last_cyclone_error_count:
            result += 'c_error: %d  ' % self.last_cyclone_error_count
        if 0 < self.last_orifice_error_count:
            result += 'o_error: %d  ' % self.last_orifice_error_count

        for solenoid_id in range(0, 4):
            if self.solenoids[solenoid_id] == SolenoidPosition.Open:
                result += 'Solenoid %d is Open  ' % (solenoid_id + 1)

        if self.motor_going_up:
            result += 'Motor up  '
        if self.motor_going_down:
            result += 'Motor down  '

        return result


    def set_temperature_probe_data(self, temperature, temperature_mv):
        self.last_temperature_probe_reading = temperature
        self.last_temperature_probe_reading_mv = temperature_mv
        

    def _receive_cmd_version(self, frame):
        self.version_major = frame.data[1]
        self.version_minor = frame.data[2]

        year = (frame.data[3] << 8) + frame.data[4]
        month = frame.data[5]
        day = frame.data[6]

        self.version_date_string = '%04d-%02d-%02d' % (year, month, day)

        msg = '  Version: \t %d.%d \t' % (self.version_major, self.version_minor)
        msg += self.version_date_string
        if not self.quiet_mode:
            print(msg)

    def _receive_cmd_current(self, frame):
        stale = False if frame.data[1] == 0 else True
        status = frame.data[2]
        raw_current = (frame.data[3] << 8) + frame.data[4]
        average_points = frame.data[5]
        gain_setting = frame.data[6]
        fault_setting = frame.data[7]

        # mA/LSB
        gain = { 0b10: 15.66, 0b11: 31.31, 0b00: 62.62, 0b01: 125.24 }

        self.last_current_reading = raw_current * gain[gain_setting]

        msg = '  Current: \t'
        msg += 'status: %x \t' % status
        msg += 'current: %0.2f (%x)\t' % (raw_current * gain[gain_setting], raw_current)
        msg += 'avgPts: %x, gain: %x, fault: %x' % (average_points, gain_setting, fault_setting)

        if not self.quiet_mode:
            print(msg)
    
    def _receive_cmd_positions(self, frame):
        for index in range(0, 4):
            self.solenoids[index] = SolenoidPosition.Closed if frame.data[index+1] == 0 else SolenoidPosition.Open

        self.motor_going_up = bool(frame.data[5])
        self.motor_going_down = bool(frame.data[6])

        msg = '  Positions: \t'
        msg += 'S 1: %s \t' % self.solenoids[0].name
        msg += 'S 2: %s \t' % self.solenoids[1].name
        msg += 'S 3: %s \t' % self.solenoids[2].name
        msg += 'S 4: %s \t' % self.solenoids[3].name

        if self.motor_going_up:
            msg += 'Motor up \t'
        if self.motor_going_down:
            msg += 'Motor down \t'

        if not self.quiet_mode:
            print(msg)

    def bcd_to_number(self, bcd):
        number = bcd & 0x0F
        number += ((bcd >> 4) & 0x0F) * 10

        return number

    def _receive_cmd_time(self, frame):
        #if not self.quietMode:
        #    print('  Time %s' % frame.hexDataString())
            
        status = frame.data[1]
        hours = self.bcd_to_number(frame.data[2])
        minutes = self.bcd_to_number(frame.data[3])
        seconds = self.bcd_to_number(frame.data[4])

        msg = '  Time: \t'
        msg += '%02d:%02d:%02d' % (hours, minutes, seconds)
        msg += '\t status: %x' % status
        
        if not self.quiet_mode:
            print(msg)

    def _receive_cmd_date(self, frame):
        #if not self.quietMode:
        #    print('  Date %s' % frame.hexDataString())

        status = frame.data[1]
        year = self.bcd_to_number(frame.data[2])
        month = self.bcd_to_number(frame.data[3])
        mday = self.bcd_to_number(frame.data[4])
        wday = frame.data[5]

        msg = '  Date: \t'
        msg += '20%02d / %02d / %02d \t' % (year, month, mday)
        msg += 'wday: %d \t' % wday
        #msg += '%x / %x / %x \t' % (mday, month, year)
        msg += ' status: %x' % status

        if not self.quiet_mode:
            print(msg)

    def _calculate_pressure_v1(self, raw_pressure, pressure_max, pressure_min):
        output_max = 0x3999
        output_min = 0x0666
        output = raw_pressure

        pressure = (output - output_min)
        pressure *= (pressure_max - pressure_min)
        pressure /= (output_max - output_min)
        pressure += pressure_min

        return pressure

    def _receive_cmd_raw_pressure(self, frame):
        raw_cyclone_reading = (frame.data[1] << 8) + frame.data[2]
        raw_orifice_reading = (frame.data[3] << 8) + frame.data[4]

        cyc_max = self.ebox_config.cyclone_sensor_pressure_max;
        cyc_min = self.ebox_config.cyclone_sensor_pressure_min;
        cyc_version = self.ebox_config.cyclone_sensor_version;

        self.last_cyclone_pressure = self._calculate_pressure_v1(raw_cyclone_reading, cyc_max, cyc_min)

        ori_max = self.ebox_config.orifice_sensor_pressure_max;
        ori_min = self.ebox_config.orifice_sensor_pressure_min;
        ori_version = self.ebox_config.orifice_sensor_version;
        
        self.last_orifice_pressure = self._calculate_pressure_v1(raw_orifice_reading, ori_max, ori_min)

        msg = '  Pressure: \t Cyclone: %f (%x)' % (self.last_cyclone_pressure, raw_cyclone_reading)
        msg += ',\t Orifice: %f (%x)' % (self.last_orifice_pressure, raw_orifice_reading)
        if not self.quiet_mode:
            print(msg)

        self.average_cyclone_pressure = self.average_cyclone_pressure * (self.pressure_averaging_length - 1) / self.pressure_averaging_length
        self.average_cyclone_pressure += self.last_cyclone_pressure / self.pressure_averaging_length

        self.average_orifice_pressure = self.average_orifice_pressure * (self.pressure_averaging_length - 1) / self.pressure_averaging_length
        self.average_orifice_pressure = self.last_orifice_pressure / self.pressure_averaging_length

        self.last_pressure_time = datetime.datetime.now()

    def _receive_cmd_raw_temperature(self, frame):
        raw_cyclone_reading = (frame.data[1] << 8) + frame.data[2]
        raw_orifice_reading = (frame.data[3] << 8) + frame.data[4]

        self.last_cyclone_temperature = ((raw_cyclone_reading / 2047.0) * 200.0) - 50.0
        self.last_orifice_temperature = ((raw_orifice_reading / 2047.0) * 200.0) - 50.0

        msg = '  Temperature: \t Cyclone: %f (%x)' % (self.last_cyclone_temperature, raw_cyclone_reading)
        msg += ',\t Orifice: %f (%x)' % (self.last_orifice_temperature, raw_orifice_reading)
        if not self.quiet_mode:
            print(msg)

    def _receive_cmd_i2c_stats(self, frame):

        dataType = (frame.data[1] >> 4) & 0x0F
        stale = frame.data[1] & 0x0F

        number1 = (frame.data[2] << 8) + frame.data[3]
        number2 = (frame.data[4] << 8) + frame.data[5]
        number3 = (frame.data[6] << 8) + frame.data[7]

        sensorType = 'Cyclone'
        sensorType = 'Orifice' if frame.data[0] == CanFrame.CAN_COMMAND_REPORT_I2C_STATS_ORIFICE else sensorType
        sensorType = 'Current' if frame.data[0] == CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CURRENT else sensorType

        
        if dataType == 0:
            self.last_main_loop_count = number2
        if dataType == 2 and sensorType == 'Cyclone':
            self.last_cyclone_error_count = number3
        elif dataType == 2 and sensorType == 'Orifice':
            self.last_orifice_error_count = number3

        msg = '  %s I2C stats %d: \t n1: %d, \t n2: %d, \t n3: %d \t stale: %d' % (sensorType, dataType, number1, number2, number3, stale)
        
        if not self.quiet_mode:
            #print('  %s' % frame.hexDataString())
            print(msg)

    def _receive_cmd_pressure_stats(self, frame):
        stale = frame.data[1]
        successCount = (frame.data[2] << 8) + frame.data[3]
        staleCount = (frame.data[4] << 8) + frame.data[5]
        failCount = (frame.data[6] << 8) + frame.data[7]

        msg = '  %x sensor stats: \t' % frame.data[0]
        msg += 'Success: %d, \t' % successCount
        msg += 'Stale: %d, \t' % staleCount
        msg += 'Fail: %d ' % failCount
        if not self.quiet_mode:
            print(msg)

    def receive_frame(self, frame):
        """take the CAN frame and pass it off to the appropriate function"""
        command = frame.data[0]

        if command == CanFrame.CAN_COMMAND_REPEAT:
            # don't need to do anything, ebox responded to the online ping
            pass
        elif command == CanFrame.CAN_COMMAND_REPORT_VERSION:
            self._receive_cmd_version(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_CURRENT:
            self._receive_cmd_current(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_POSITIONS:
            self._receive_cmd_positions(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_TIME:
            self._receive_cmd_time(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_DATE:
            self._receive_cmd_date(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_PRESSURE:
            pass
        elif command == CanFrame.CAN_COMMAND_REPORT_TEMPERATURE:
            pass
        elif command == CanFrame.CAN_COMMAND_REPORT_RAW_TEMPERATURE:
            self._receive_cmd_raw_temperature(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_RAW_PRESSURE:
            self._receive_cmd_raw_pressure(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CYCLONE:
            self._receive_cmd_i2c_stats(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_I2C_STATS_ORIFICE:
            self._receive_cmd_i2c_stats(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CURRENT:
            self._receive_cmd_i2c_stats(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_PRESSURESENSOR_STATS_CYCLONE:
            self._receive_cmd_pressure_stats(frame)
        elif command == CanFrame.CAN_COMMAND_REPORT_PRESSURESENSOR_STATS_ORIFICE:
            self._receive_cmd_pressure_stats(frame)
        else:
            print('command %x not recognized' % command)
            print('%x received: %s' % (self.can_id, frame.hexDataString()))

        if not self.online:
            self.online = True
            # need to make sure the ebox is configured for where it is in the schedule
            self.ebox_schedule.set_next_event_to_now()

        self.failed_cmd_count = 0

    def send_cmd_failed(self, cmd):
        self.failed_cmd_count += 1
        if 10 <= self.failed_cmd_count:
            self.online = False
    
    def generate_solenoid_cmds(self, solenoid, isActive, exclusive=True):
        """Generate the cmds to turn off any Open solenoids and also Open/Close the specified one.  Solenoid is a number [1-4]."""
        cmds = []
        # iterate through the solenoids besides the specified one and close any that are open (since only one should be open at a time).
        for solenoid_index in range(0, 4):
            if self.solenoids[solenoid_index] == SolenoidPosition.Open and solenoid != (solenoid_index + 1):
                frame = CanFrame()
                frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_SET_SOLENOID)
                frame.data[2] = solenoid_index + 1
                frame.data[3] = False
                frame.can_dlc = 4
                # exclusive means that we only want one solenoid open at a time
                if exclusive:
                    cmds.append(frame)
                
        # Open/Close the specified position
        frame = CanFrame()
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_SET_SOLENOID)
        frame.data[2] = solenoid
        frame.data[3] = isActive
        frame.can_dlc = 4
        cmds.append(frame)
        return cmds

    def number_to_bcd(self, number): # assumes only 2 digit number
        # get the first digit
        bcd = int(number % 10) & 0x0F
        number /= 10
        # get the second digit
        bcd += (int(number % 10) & 0x0F) << 4
        return bcd

    def generate_cmd_time(self, hours, minutes, seconds):
        frame = CanFrame()
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_SET_TIME)
        frame.data[2] = self.number_to_bcd(hours)
        frame.data[3] = self.number_to_bcd(minutes)
        frame.data[4] = self.number_to_bcd(seconds)
        frame.can_dlc = 5
        return frame;

    def generate_cmd_date(self, year, month, day, weekDay):
        frame = CanFrame()
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_SET_DATE)
        frame.data[2] = self.number_to_bcd(year)
        frame.data[3] = self.number_to_bcd(month)
        frame.data[4] = self.number_to_bcd(day)
        frame.data[5] = weekDay & 0x0F
        frame.can_dlc = 6
        return frame

        
    def generate_status_cmds(self):
        cmds = []
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_VERSION)
        cmds.append(frame)
        
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_DATE)
        cmds.append(frame)
        
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_TIME)
        cmds.append(frame)
        
        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_RAW_PRESSURE)
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_RAW_TEMPERATURE)
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_POSITIONS)
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_CURRENT)
        cmds.append(frame) 

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CYCLONE)
        frame.data[2] = 0
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CYCLONE)
        frame.data[2] = 1
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CYCLONE)
        frame.data[2] = 2
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_I2C_STATS_ORIFICE)
        frame.data[2] = 2
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_I2C_STATS_CURRENT)
        frame.data[2] = 2
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_PRESSURESENSOR_STATS_CYCLONE)
        cmds.append(frame)

        frame = CanFrame()
        frame.can_id = 0x123
        frame.initCmdFrame(self.can_id, CanFrame.CAN_COMMAND_REPORT_PRESSURESENSOR_STATS_ORIFICE)
        cmds.append(frame)

        return cmds

    def add_elapsed_time(self, timedelta):
        if not self.online:
            return
        if not self.state == EboxState.SAMPLING_ACTIVE:
            return
        cassette_index = self.ebox_schedule.position_index - 1
        self.ebox_logsheet.add_elapsed_time(cassette_index, timedelta)

    @property
    def sampling_movable_cassette(self):
        current_cassette_count = len(self.ebox_schedule.calculated_cartridge_dates)
        next_cassette_count = len(self.ebox_schedule.calculated_next_cartridge_dates)
        # check if next cartridge has less cassettes than current one
        if current_cassette_count <= next_cassette_count:
            return False
        # check if we are sampling on the last position (position index counts from one instead of from zero)
        if current_cassette_count != self.ebox_schedule.position_index:
            return False
        # check if sampling is actually running on the above position
        if not self.ebox_schedule.sampling_active:
            return False
        # most importantly, check that the actual current cartridge matches the calculated current cartridge
        last_date = self.ebox_config.current_cartridge_dates[-1:1]
        if last_date != self.ebox_schedule.calculated_cartridge_dates[-1:1]:
            return False
        
        # All conditions matched
        return True

    @property
    def next_cartridge(self):
        next_ = {}
        next_['dates'] = []
        next_['schedule_index'] = 0

        first_date = self.ebox_config.current_cartridge_dates[0]
        
        # check to see if we are behind or caught up on changing the cartridge
        if first_date in self.ebox_schedule.calculated_cartridge_dates:
            next_['dates'] = self.ebox_schedule.calculated_next_cartridge_dates
            next_['schedule_index'] = self.ebox_schedule.next_cartridge_schedule_index
            
        else:
            next_['dates'] = self.ebox_schedule.calculated_cartridge_dates
            next_['schedule_index'] = self.ebox_schedule.cartridge_schedule_index

        return next_
        

    def change_cartridge(self):
        # reset the schedule object cartridge changed flag
        self.ebox_schedule.change_cartridge()

        # update the current cartridge
        next_ = self.next_cartridge
        self.ebox_config.current_cartridge_dates = next_['dates']
        self.ebox_config.current_schedule_index = next_['schedule_index']

        # create the next logsheet object
        next_logsheet = EboxLogsheetData(next_['schedule_index'], next_['dates'])       
            
        # need to deal with movable cassette here
        if self.sampling_movable_cassette:
            current_cassette = self.ebox_logsheet.cassette_data[self.ebox_schedule.position_index - 1]
            # we want to "clone" the logsheet cassette and then only copy the elapsed time property
            moved_cassette = EboxLogsheetCassetteData(current_cassette.sample_date)
            moved_cassette.moved_from_previous_cartridge = True
            moved_cassette.elapsed_sampling_time = current_cassette.elapsed_sampling_time
            next_logsheet.cassette_data.append(moved_cassette)
            

        self.ebox_logsheet = next_logsheet
        self.state = EboxState.CARTRIDGES_CHANGED

    def _format_solenoid(self, solenoid_index):
        value = self.solenoids[solenoid_index]
        if value == SolenoidPosition.Closed:
            return 'closed'
        elif value == SolenoidPosition.Open:
            return 'open'
        else:
            return 'unknown'

    def get_module_data_column_titles(self):
        data = ['time',
                'position',
                'module online'
                'cyclone pressure',
                'orifice pressure',
                'cyclone temperature',
                'orifice temperature',
                'average cyc',
                'average ori',
                'amperage sensor (mV)',
                'solenoid 1',
                'solenoid 2',
                'solenoid 3',
                'solenoid 4',
                'pump on/off',
                'motor going up',
                'motor going down',
                'ebox internal loop count',
                'i2c cyclone error count',
                'i2c orifice error count',
                'temperature probe (C)',
                'temperature probe (mV)'
                ]

    def get_module_data(self):
        now = datetime.datetime.now()
        position = self.ebox_config.module_position
        f6 = '{:.6f}' # floating point 6 decimal places
        f2 = '{:.2f}' # floating point 2 decimal places
        data = [str(now),
                'module%02d' % position,
                'online' if self.online else 'offline',
                f6.format(self.last_cyclone_pressure),
                f6.format(self.last_orifice_pressure),
                f2.format(self.last_cyclone_temperature),
                f2.format(self.last_orifice_temperature),
                f6.format(self.average_cyclone_pressure),
                f6.format(self.average_orifice_pressure),
                f2.format(self.last_current_reading),
                self._format_solenoid(0),
                self._format_solenoid(1),
                self._format_solenoid(2),
                self._format_solenoid(3),
                GPIO.get_pump_state(position),
                self.motor_going_up,
                self.motor_going_down,
                self.last_main_loop_count,
                self.last_cyclone_error_count,
                self.last_orifice_error_count,
                f2.format(self.last_temperature_probe_reading),
                f2.format(self.last_temperature_probe_reading_mv)
                ]
        
        output = io.StringIO()
        # we want to save the output in csv format
        writer = csv.writer(output)
        writer.writerow(data)
        
        file_data = output.getvalue()
            
        output.close()
        return file_data
